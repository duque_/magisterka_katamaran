#########################################################################
#   Objects for custom makefile for STM32L552ZE-G NUCLEO board                                                                   #
#########################################################################

INCS:=\
	-I./Drivers/CMSIS/Include\
	-I./Inc\
	-I./Drivers/STM32F4xx_HAL_Driver/Inc\
	-I./Drivers/CMSIS/Device/ST/STM32F4xx/Include\
	-I./Drivers/STM32F4xx_HAL_Driver/Inc/Legacy\
	-I./Middlewares/Third_Party/FreeRTOS/Source/include/\
	-I./Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F\
	-I./Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/\
	-I./Middlewares/ST/STM32_USB_Host_Library/Core/Inc\
	-I./Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc

C_SRCS += \
	$(filter-out Src/syscalls.c, $(wildcard Src/*.c))\
	$(wildcard Drivers/STM32F4xx_HAL_Driver/Src/*.c)\
	$(wildcard Middlewares/Third_Party/FreeRTOS/Source/*.c)\
	$(wildcard Middlewares/Third_Party/FreeRTOS/Source/*/*.c)\
	$(wildcard Middlewares/Third_Party/FreeRTOS/Source/*/*/*.c)\
	$(wildcard Middlewares/Third_Party/FreeRTOS/Source/*/*/*/*.c)\
	Startup/startup_stm32f429zitx.s\


OBJS:=$(C_SRCS:.c=.o)
OBJS:=$(OBJS:.s=.o)
OBJS:= $(addprefix ./Build/obj/,$(OBJS))


# objects make rules #
$(BUILD_DIR)/obj/Startup/%.o: ./Startup/%.s
	arm-none-eabi-gcc -mcpu=cortex-m4 -g3 -c -x assembler-with-cpp -o "$@" "$<"
$(BUILD_DIR)/obj/%.o: %.c
	arm-none-eabi-gcc "$<" $(CFLAGS) $(INCS) -MF"$(BUILD_DIR)/obj/$*.d" -MT"$@" -o "$@"
