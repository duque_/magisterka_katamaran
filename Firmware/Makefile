################################################################################
# Custom makefile for STM32F429xx board
################################################################################

RM:= rm -rf
MKDIR := mkdir -p

OUTPUT_NAME:=katamaran
BUILD_DIR:=Build

OUTPUT:=$(BUILD_DIR)/$(OUTPUT_NAME)

CFLAGS+= 	 -mcpu=cortex-m4 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DUSE_FULL_LL_DRIVER\
		 	 -DSTM32F429xx -DDEBUG -c -O0 -ffunction-sections -fdata-sections -Wall\
		 	 -fstack-usage -MMD -MP --specs=nano.specs -mfpu=fpv5-sp-d16\
		 	 -mfloat-abi=hard -mthumb

CFLAGS_ELF+= -mcpu=cortex-m4 -T"./STM32F429ZITX_FLASH.ld" --specs=nosys.specs\
			 -Wl,-Map="$(OUTPUT).map" -Wl,--gc-sections -static --specs=nano.specs\
			 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -u _printf_float -u _scanf_float\
			 -Wl,--start-group -lc -lm -Wl,--end-group

# All of the sources participating in the build are defined here
-include ./objects.mk

.DEFAULT_GOAL := all

all: make_dir $(OBJS) 
	arm-none-eabi-gcc -o "$(OUTPUT).elf" $(OBJS)  $(USER_OBJS) $(LIBS) $(CFLAGS_ELF)
	arm-none-eabi-size  $(OUTPUT).elf
	arm-none-eabi-objdump -h -S $(OUTPUT).elf > "$(OUTPUT).list"
	arm-none-eabi-objcopy  -O binary $(OUTPUT).elf "$(OUTPUT).bin"
	@echo 'Finished building: $@'

make_dir:
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Src;
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Startup;
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Drivers/STM32F4xx_HAL_Driver/Src;
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS;
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang;
	@-$(MKDIR) ./$(BUILD_DIR)/obj/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F;

# Other Targets
clean:
	@-$(RM) ./$(BUILD_DIR)
	-@echo 'Cleaning project...'

flash: all
	openocd -f stm32f429-disco.cfg -c "program $(BUILD_DIR)/$(OUTPUT_NAME).elf verify reset exit"
	-@echo 'Flashing project...'

debugserver:
	openocd -f stm32f429-disco.cfg

flash_debug: all
	(echo "monitor reset halt"; cat) | (echo "target remote localhost:3333"; cat) | arm-none-eabi-gdb ./Build/katamaran.elf

.PHONY: all clean makedir