
#include "modbus.h"


//#define RS485_DIRECTION_CHANGE_DELAY	20  // tyle ms trzeba czekać by znowu nadawać na rs485 po odebraniu ramki

#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

extern char XBee_input[64];

extern uint8_t UART_Buffer_6[UART_BUFFER_6_SIZE];
extern uint8_t UART_Buffer_5[UART_BUFFER_5_SIZE];

extern UART_HandleTypeDef huart6;
extern UART_HandleTypeDef huart5;

extern uint16_t UART_6_entry;
extern uint16_t UART_5_entry;

uint16_t COIL_REG = 0;  //inicjalizacja rejestru statusu cewek
uint16_t ANALOG_REG[HOLDING_REG_LENGTH] = {0}; //inicjalizacja wartosci w rejestrze

uint64_t lastRequestTime = 0;  //chwila relizacji ostatniego zapytania - kontrola komunikacji

#define modbusASCII_frame_lenght_MAX 100
unsigned char modbus_received_frame_HEX[modbusASCII_frame_lenght_MAX / 2] = {};
unsigned char modbus_received_frame_ASCII[modbusASCII_frame_lenght_MAX] = {};
unsigned char modbus_received_frame_RS485[modbusASCII_frame_lenght_MAX] = {};
unsigned char modbus_response_hex[modbusASCII_frame_lenght_MAX / 2] = {};  // sama odpowiedz w HEX (kazdy bajt trzeba zamienic na dwa ASCII) bez : CR LF
unsigned char modbusFrame_ASCII[modbusASCII_frame_lenght_MAX] = {};

uint8_t serialXbeeReads = 0;	//ile bajtow danych odebrano (z XBee) - dlugosc ramki
uint8_t serial485Reads = 0;		//ile bajtow danych odebrano (z RS485) - dlugosc ramki

int responseIndex = 0;
uint32_t counter = 0;

//TODO:
//przy konwersji może pojawić się liczba większa niż 128
const unsigned char char_tab[128] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0,
									  0, 0, 0, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0a, 0x0b, 0x0c,
									  0x0d, 0x0e, 0x0f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0
};

void handleCommunication(void *pvParameters)
{
	/* Set RS485 communication direction to receiving */
	HAL_GPIO_WritePin(GPIOC, RS485_EN_Pin, GPIO_PIN_SET);   // albo reset, TODO

	for (;;)
	{
		if (readXbee())
		{
			uint8_t modbus_idx_end = Ascii2Hex(modbus_received_frame_ASCII, modbus_received_frame_HEX, serialXbeeReads);

#ifdef INFO_MODBUS
			counter++;
			printf("MODBUS POOL START\n");
			printf("counter: %lu\n", counter);
			printf("Received frame: %s, %d\n", (char*)modbus_received_frame_ASCII, serialXbeeReads);
			printf("Length: %u Calculated LRC: %x Received LRC: %x\n", modbus_idx_end, lrc(modbus_received_frame_HEX, modbus_idx_end), modbus_received_frame_HEX[modbus_idx_end]);
			printf("MODBUS POOL END\n");
#endif // INFO_MODBUS

			if (lrc(modbus_received_frame_HEX, modbus_idx_end) == modbus_received_frame_HEX[modbus_idx_end])
			{
				// dolozyc sprawdzenie czy nadajnik 485 zdazyl sie przelaczyc w nadawanie i dopiero wtedy sprawdzac rozkaz
				if (pollReceived_internal())  //w przypadku otrzymania calej ramki (od ':' do CRLF) przetworzenie otrzymanego rozkazu, lub kopiuje ramkę do rs485, zwraca true, gdy wewnetrzny rozkaz (lub błędny) i od razu można przejść do wysyłania odpowiedzi Xbee
				{
					lastRequestTime = xTaskGetTickCount();
				}
				else
				{
#ifdef INFO_MODBUS
					printf("%lu. MD: %s\n", counter, (char*)modbus_received_frame_ASCII);
#endif // INFO_MODBUS

					char data[128];// Tablica przechowujaca wysylana wiadomosc.
					uint16_t size = 0; // Rozmiar wysylanej wiadomosci

					// Wysyłanie wiadomości na magistralę Modbus RS485
					size = sprintf(data, (char *)":");
					HAL_UART_Transmit(&huart6, (uint8_t *)data, size ,20);
					HAL_UART_Transmit(&huart6, (uint8_t *)modbus_received_frame_ASCII, serialXbeeReads,20);

					HAL_GPIO_WritePin(GPIOC, RS485_EN_Pin, GPIO_PIN_RESET);   // set RS485 to receiving

					uint16_t rs485_response_timestamp = xTaskGetTickCount();  // timestamp do 485_response_timeOutu
					uint16_t gg = 0;

					while ((gg = (xTaskGetTickCount() - rs485_response_timestamp)) < pdMS_TO_TICKS(RS485_RESPONSE_TIMEOUT))
					{
						if (readSlave())
						{
							HAL_UART_Transmit(&huart5, (uint8_t *)modbus_received_frame_RS485, serial485Reads, 20);
							break;
						}
					}

					if (serial485Reads == 0)
					{
						ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_GATEWAY_TARGET_DEV_RESPOND);
					}
					serial485Reads = 0;
				}
				// Wysyłanie wiadomości na magistralę Modbus RS485

				HAL_GPIO_WritePin(GPIOC, RS485_EN_Pin, GPIO_PIN_SET);   // set RS485 to sending
			}
			else
			{
				serialXbeeReads = 0;
			}
		}
		osDelay(10);
	}

}


//Odczytuje ramkę z bufora Xbee do bufora modbus_received_frame_ASCII[] (bez ':', ale z CR i LF; indeks ostatniego elementu to (serialXbeeReads-1))
//Zwraca: True, jeśli odczytano pełną ramkę
bool readXbee()
{
	bool ret = false;
	int i = 0;
	int colon_detected = false;

	for (int n = 0; n < 10; n++)
	{
		if (0x3A == UART_Buffer_5[n])
		{  // 0x3A = ':'
			colon_detected = true;
		}

		while(colon_detected)
		{
			if (CR_CHAR == UART_Buffer_5[i++])
			{
				if ((i % 2) != 0)
				{
					serialXbeeReads = 0;
				}
				else
				{
					serialXbeeReads = i;

					if (serialXbeeReads <= modbusASCII_frame_lenght_MAX)
					{
						memcpy(modbus_received_frame_ASCII, &UART_Buffer_5[n + 1], serialXbeeReads);
						memset(&UART_Buffer_5[0], 0, serialXbeeReads);
						ret = true;
					}
					else
					{
						ret = false; // ERROR HANDLER
					}

					colon_detected = false;
					i = 0;
				}
			}
		}
	}

	return ret;
}

//Odczytuje ramkę z urządzenia SLAVE do bufora modubs_master_buffer[]
//Zwraca: True jeśli została odczytana pełna ramka lub jeżeli ramka odpowiedzi była za długa
bool readSlave()
{
	bool ret = false;
	int i = 0;
	int colon_detected = false;

	for (int n = 0; n < 10; n++)
	{
		if (0x3A == UART_Buffer_6[n])
		{  // 0x3A = ':'
			colon_detected = true;
		}

		while(colon_detected)
		{
			if (CR_CHAR == UART_Buffer_6[i++])
			{
				if ((i % 2) != 0)
				{
					serial485Reads = 0;
				}
				else
				{
					serial485Reads = i + 1;

					if (serial485Reads <= modbusASCII_frame_lenght_MAX)
					{
						memcpy(modbus_received_frame_RS485, &UART_Buffer_6[0], serial485Reads + 2);
						memset(&UART_Buffer_6[n], 0, serial485Reads);
						ret = true;
					}
					else
					{
						ret = false; // ERROR HANDLER
					}

					colon_detected = false;
					i = 0;
				}
			}
		}
	}

	return ret;
}

//Przetwarza odebrany rozkaz
//Zwraca: True jeśli rozkaz był wewnętrzny
bool pollReceived_internal()
{
	if (modbus_received_frame_HEX[0] == MODBUS_ADDRESS)
	{ //czy  ramka była skierowana do modułu głównego
		switch (modbus_received_frame_HEX[1])
		{
		case 0x01:
			break;
		case 0x03:
			Function_03(modbus_received_frame_HEX);	// Read Holding Registers (czyta ANALOG_REG)
			break;
		case 0x04:
			break;
		case 0x05:
			Function_05(modbus_received_frame_HEX);	// Force single coil (ustawia jedno wyjscie binarne)
			break;
		case 0x06:
			break;
		case 0x0F:
			Function_0F(modbus_received_frame_HEX);
			break;
		case 0x10:
			Function_10(modbus_received_frame_HEX);
			break;
		case 0x11:
			Function_11(modbus_received_frame_HEX); // Report Slave ID
			break;
		case 0x99: // rozkaz testowy poza standardem
			break;
		default:
			ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_ILLEGAL_FUNCTION);
			break;
		}
		return true;
	}
	return false;
}

//Zamienia ramke w formacie ASCII na format BINARNY
//Zwraca: indeks na ostatni element tablicy dest[]
uint8_t Ascii2Hex(unsigned char *src, unsigned char *dest, uint8_t len) {
	uint8_t i = len/2;

	while (i--) {
		dest[i] = char_tab[src[i*2]] * 16 + char_tab[src[i*2+1]];
	}

	return (len/2 - 2);
}

//Zamienia ramke w formacie BINARNYM na format ASCII
//Zwraca: wskaźnik na ostatni bajt
char * Hex2Ascii(unsigned char *data, char *AsciiBuf, int length) {
	while (length--) {
		*AsciiBuf = (*data >> 4) + '0';
		if (*AsciiBuf > '9')
			*AsciiBuf += 7;
		AsciiBuf++;
		*AsciiBuf = (*data & 0x0F) + '0';
		if (*AsciiBuf > '9')
			*AsciiBuf += 7;
		AsciiBuf++;
		data++;
	}
	*AsciiBuf = '\0';
	return AsciiBuf;
}

//Sprawdza sume kontrolną LRC
//Zwraca: True jeśli suma kontrolna się zgadza
uint8_t lrc(unsigned char *tab, uint8_t length) {   //Funkcja wyliczajaca wartosc LRC
	uint8_t checksum = 0;

	for (int i = 0; i < length; i++) {
		checksum -= tab[i];
	}
	return checksum;
}


void Function_03(unsigned char *request) {
	//ramka :-ID-FCN-ADRHi-ADRLo-QuanHi-QuanLo-LRC-CR-LF
	//if(!MatchSlaveID(request)) return;					// Gdy adres jest zly - pomiac przetwarzanie
#ifdef INFO_MODBUS
	printf("FUN 03\n");
#endif // INFO_MODBUS

	int  address = 256 * request[2] + request[3];		// Adres (poczatek)
	int  quantity = 256 * request[4] + request[5];		// Ilosc rejestrow do odczytu
	int bytesToRead = quantity * 2;						// Ilosc bajtow do odczytu (8 inputs/byte)
	// Tworzenie ramki z odpowiedzia:
	if (HOLDING_REG_LENGTH >= address + quantity) {
		// response: :-ID-FCN-B1-B2-BN-LRC-CR-LF
		responseIndex = 0;
		modbus_response_hex[responseIndex] = MODBUS_ADDRESS; responseIndex++;						// SS
		modbus_response_hex[responseIndex] = 0x03; responseIndex++;								// FF
		modbus_response_hex[responseIndex] = bytesToRead; responseIndex++;							// TT

		for (int i = 0; i < bytesToRead; i += 2) {
			modbus_response_hex[i + responseIndex] = ANALOG_REG[address + i / 2] >> 8; // Data hi (u nas zawsze 0)
			modbus_response_hex[i + responseIndex + 1] = ANALOG_REG[address + i / 2]; // Data lo
		}

		responseIndex += bytesToRead;
		modbus_response_hex[responseIndex] = lrc(modbus_response_hex, responseIndex); responseIndex++;

		serialXbeeReads = CreateModbusFrame();

		HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);
	}
	else {
		ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_ILLEGAL_DATA_ADDRESS);
	}
}

//Force single coil
void Function_05(unsigned char *request) {
	// :-ID-FCN-FCN-ADR-ADR-VAL-VAL-LRC-LRC-CR-LF
#ifdef INFO_MODBUS
	printf("FUN 05\n");
#endif // INFO_MODBUS

	int  address = 256 * (unsigned int)request[2] + (unsigned int)request[3];			// Adres (poczatek)
	int value = (request[4] == 0xFF && request[5] == 0x00);						// 0xFF00 - ON | 0x0000 - OFF

	if (address < COIL_REG_LENGTH) {
		// Tworzenie ramki z odpowiedzia:
		// response <- request
		responseIndex = 0;
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// SS
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// FF
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (hi)
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (lo)

		bitWrite(COIL_REG, address, value);

		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// VV (hi)
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// VV (lo)

		modbus_response_hex[responseIndex] = lrc(modbus_response_hex, responseIndex);
		responseIndex++;

		serialXbeeReads = CreateModbusFrame();
		HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);
	}
	else {
		ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_ILLEGAL_DATA_ADDRESS);
	}
}

// Preset Multiple Coils
void Function_0F(unsigned char *request) {
	//11 0F 0013 000A 02 CD01     BF0B
	//SS FF AAAA TTTT NN VVVV ... LLLL
	//0  1  2 3  4 5  6  7 8  ... M N   (M=N-1)

#ifdef INFO_MODBUS
	printf("FUN 0F\n");
#endif // INFO_MODBUS

	int  address = 256 * (unsigned int)request[2] + (unsigned int)request[3];	// Adres (poczatek)
	int  quantity = 256 * (unsigned int)request[4] + (unsigned int)request[5];	// Ilosc wyjsc bitowych do zapisu
	char bytesToFollow = request[6];												// Ilosc bajtow do zapisu (8 inputs/byte)

	if (address + quantity <= COIL_REG_LENGTH) {
		// Tworzenie ramki z odpowiedzia:
		//SS FF AAAA TTTT LLLL
		//0  1  2 3  4 5  6 7
		responseIndex = 0;
		modbus_response_hex[responseIndex] = MODBUS_ADDRESS; responseIndex++;						// SS
		modbus_response_hex[responseIndex] = 0x0F; responseIndex++;								// FF
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (hi)
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (lo)


		// Zapis wyjsc bitowych:
		int coil, dataIndex, value;
		unsigned char mask = 0x00;
		const int requestIndexOffset = 7;

		for (coil = 0; coil < bytesToFollow * 8; coil++) {
			if (coil < quantity) {
				dataIndex = coil / 8 + requestIndexOffset;
				mask = ipow(2, coil % 8); // 2^(i mod 8)
				value = ((request[dataIndex] & mask) != 0);

				bitWrite(COIL_REG, address + coil, value);
			}
			else {
				break;
			}
		}

		modbus_response_hex[responseIndex] = (coil) / 256;	responseIndex++;						// TT (hi)
		modbus_response_hex[responseIndex] = (coil) % 256;	responseIndex++;						// TT (lo)
		modbus_response_hex[responseIndex] = lrc(modbus_response_hex, responseIndex); responseIndex++;		// LL

		serialXbeeReads = CreateModbusFrame();
		//_SerialXBee.write(modbusFrame_ASCII, serialXbeeReads);
		HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);

	}
	else {
		ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_ILLEGAL_DATA_ADDRESS);
	}
}

// Preset multiple Registers
void Function_10(unsigned char *request) {
#ifdef INFO_MODBUS
	printf("FUN 10\n");
#endif // INFO_MODBUS

	int  address = 256 * (unsigned int)request[2] + (unsigned int)request[3];	// Adres (poczatek)
	int  quantity = 256 * (unsigned int)request[4] + (unsigned int)request[5];	// Ilosc rejestrow do odczytu AK: nieuzywane, to zakomenowalem   LR: kontrola czy nie zapisujemy poza rejestrem, odkomentowalem
	int bytesToFollow = request[6];												// Ilosc bajtow do zapisu
	if (HOLDING_REG_LENGTH >= address + quantity) {
		//Zapis rejestrow:
		int r;
		for (r = 0; r < bytesToFollow; r += 2) {
			ANALOG_REG[address + r / 2] = (request[7 + r] << 8) | request[8 + r];
		}

		// Tworzenie ramki z odpowiedzia:
		//SS FF AAAA TTTT LLLL
		//0  1  2 3  4 5  6 7
		responseIndex = 0;
		modbus_response_hex[responseIndex] = MODBUS_ADDRESS; responseIndex++;						// SS
		modbus_response_hex[responseIndex] = 0x10; responseIndex++;								// FF
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (hi)
		modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// AA (lo)
		modbus_response_hex[responseIndex] = (r / 2) / 256; responseIndex++;							// TT (hi)
		modbus_response_hex[responseIndex] = (r / 2) % 256; responseIndex++;							// TT (lo)
		modbus_response_hex[responseIndex] = lrc(modbus_response_hex, responseIndex); responseIndex++; 		// LL

		serialXbeeReads = CreateModbusFrame();
		//_SerialXBee.write(modbusFrame_ASCII, serialXbeeReads);
		HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);
	}
	else {
		ExceptionResponse(modbus_received_frame_HEX, MODBUS_EXC_ILLEGAL_DATA_ADDRESS);
	}
}

// Report Slave ID
void Function_11(unsigned char *request) {
#ifdef INFO_MODBUS
	printf("FUN 11\n");
#endif // INFO_MODBUS

	// Tworzenie ramki z odpowiedzia:
	responseIndex = 0;
	modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// SS (any)
	modbus_response_hex[responseIndex] = request[responseIndex]; responseIndex++;				// FF
	modbus_response_hex[responseIndex] = MODBUS_ADDRESS; responseIndex++;						// SS (real)
	modbus_response_hex[responseIndex] = lrc(modbus_response_hex, responseIndex); responseIndex++; 		// LL

	serialXbeeReads = CreateModbusFrame();
	//_SerialXBee.write(modbusFrame_ASCII, serialXbeeReads);
	HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);
}

// Tworzy ramke Modbus na podstawie globalnej zmiennej modbusFrame i zwraca dlugosc ramki (ilosc elementow modbusFrame)
unsigned int CreateModbusFrame(void) {
	modbusFrame_ASCII[0] = ':';

	Hex2Ascii(modbus_response_hex, (char*)(modbusFrame_ASCII + 1), responseIndex); // HEX na 2 znaki ASCII

	modbusFrame_ASCII[responseIndex * 2 + 1] = CR_CHAR;
	modbusFrame_ASCII[responseIndex * 2 + 2] = LF_CHAR;

	return responseIndex * 2 + 3; // Zwroc dlugosc ramki
}

//Odpowiedz z kodem bledu
void ExceptionResponse(volatile unsigned char *request, unsigned char exceptionCode) {
	unsigned char slaveID = request[0];
	unsigned char function = request[1] + 0x80;

	responseIndex = 0;
	modbus_response_hex[responseIndex] = slaveID; responseIndex++;								// SS
	modbus_response_hex[responseIndex] = function; responseIndex++;							// FF
	modbus_response_hex[responseIndex] = exceptionCode; responseIndex++;						// EE
	modbus_response_hex[responseIndex] = lrc(*&modbus_response_hex, responseIndex);	responseIndex++;					// LL
	serialXbeeReads = CreateModbusFrame();
#ifdef INFO_MODBUS
	printf("Exception: %s\n", (char*)modbusFrame_ASCII);
#endif // INFO_MODBUS
	//_SerialXBee.write(modbusFrame_ASCII, serialXbeeReads);
	HAL_UART_Transmit(&huart5, modbusFrame_ASCII, serialXbeeReads, 30);
}

int ipow(int base, int exp) { // 2^(i mod 8) // dla base=2  i exp= 3 i  zwraca 8, dla 4 zwraca 16, exp=0 1, 1 zwraca 2
	int result = 1;
	while (exp) {
		if (exp & 1)       // jesli najmlodszy bit exp=1
			result *= base;    //    to result= result*base
		exp >>= 1;         // przesuwa exp w prawo
		base *= base;      // podnosi baze do kwadratu - tu zawsze base=2
	}
	return result;
}







