
#ifndef GPS_C_
#define GPS_C_
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "GPS.h"
#include "DeviceInfo.h"
#include "cmsis_os.h"
#include "task.h"

#include <math.h>

extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart2;

extern uint8_t  UART_Buffer_3[UART_BUFFER_3_SIZE];
extern uint16_t UART_3_entry;

char gps_buf[GPS_BUF_SIZE]; 					// Bufor przechowujący ramkę GPS
bool GPS_dollar_detected = false; 				// Status wykrycią początku ramki (znaku: '$')
int  buf_len = 0; 									// Długość bufora gps_buf


float gps_latitude = 0;							// Zmienna przechowujaca polozenie w stopniach
float gps_longitude = 0;						// Zmienna przechowujaca polozenie w stopniach
float velocity_GPS_float_raw = 9999;  			// Prędkość poruszania się z gps przeliczona na m/s ale bez kontroli poprawnosci - wykorzystywane do sprawdzenia przy liczeniu korekcji z H z GPS

short gps_time[2];  							// Zmienna przechowujaca czas z GPS gps_time[0] - HHMM; gps_time[1] - SS
short GPS_fix = 0;								// Zmienna przechowujaca odczytana z GPS wartosc jakosci sygnalu
												// 0 = Invalid, 1 = GPS fix, 2 = DGPS fix, starsza czesc to dokladnosc horyzontalna w metrach+ HDOPx10 przesuniety p 3 bity

float gps_altitude = 0;							// Wysokość nad pooziomem morza

uint16_t heading_GPSx100 = heading_GPS_error;
bool heading_GPSx100_flag = false; 				// Flaga sygnalizujaca, ze nowa dana z GPS przyszla


//**************************************


//Funkcja inizjalizująca moduł GPS
uint8_t InitGPS()
{
	char data[50];			// Tablica przechowujaca wysylana wiadomosc.1`1`2	33333333333
	uint16_t size = 0; 		// Rozmiar wysylanej wiadomosci
	HAL_StatusTypeDef ret = HAL_OK;
	uint8_t err = 0;


	size = sprintf(data, (char *)"$PUBX,40,GLL,0,0,0,0,0,0*5c\r\n"); // Wylacza komunikaty GLL
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -1;

	size = sprintf(data, (char *)"$PUBX,40,GSV,0,0,0,0,0,0*59\r\n"); // satelites in viev
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -2;

	size = sprintf(data, (char *)"$PUBX,40,VTG,0,1,0,0,0,0*5f\r\n");  // course over ground and ground speed !! - do wykorzystania
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -3;

	size = sprintf(data, (char *)"$PUBX,40,GSA,0,0,0,0,0,0*4e\r\n"); // Wylacza komunikaty o aktywnych satelitach
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -4;

	size = sprintf(data, (char *)"$PUBX,40,RMC,0,0,0,0,0,0*47\r\n"); // recommended minimum data
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -5;

	size = sprintf(data, (char *)"$PUBX,40,GGA,0,1,0,0,0,0*5b\r\n"); // WLACZA WYSYlANIE GGA CO KAZDE ROZWIAZANIE
	ret = HAL_UART_Transmit(&huart3, (uint8_t *)data, size , 100);
	if (ret != 0) err = -6;

	return err;
}


//Funkcja przetwarzająca długość geograficzną w formacie "DDDMMSS.SSSSS" (D-stopnie, M-minuty, S-sekundy) na stopnie float
//Zwraca: watość w stopniach
float calc_longitude(char *buff) {
	char deg[4] = {buff[0], buff[1], buff[2], '\0' };

	int iDeg = atoi(deg);
	float fMinutes = atof(&buff[3]);

	float longitude = iDeg + (fMinutes / 60.f);
	if (longitude < 0 || longitude > 360)
		longitude = LonError + 1; // sprawdzenie

	return longitude;
}


//Funkcja przetwarzająca szerokość geograficzną w formacie "DDMMSS.SSSSS" (D-stopnie, M-minuty, S-sekundy) na stopnie float
//Zwraca: watość w stopniach
float calc_latitude(char *buff) {
	char deg[3] = { buff[0] , buff[1], '\0' }; //  pierwsze dwie cyfry to stopnie

	int iDeg = atoi(deg);
	float iMinutes = atof(&buff[2]);

	float latitude = iDeg + (iMinutes / 60.f);
	if (latitude < 0 || latitude > 9000)
		latitude = LatError + 1; // sprawdzenie

	return latitude;
}


//Funkcja czytająca buffor od danego miejsca(offset) i zwracająca za pomocą referencji(dest) ciąg znaków do terminatora (np. ',', '*')
//Zwraca: Długość ciągu znaku (dest)
uint8_t readField(char *src, char *dest, uint8_t *offset, uint8_t max_len) {
	uint8_t i = 0;

	while (src[*offset] != ',' && src[*offset] != '*') {
		if ((*offset - 1) >= max_len)
			break;
		dest[i++] = src[(*offset)++];
	}

	dest[i] = '\0';
	(*offset)++;

	//zwraca dlugosc
	return i;
}


//Funckja przetwarzająca ramkę GPS z buffora gps_buf[]
void gps_process_buff() {
	char temp[10];
	uint8_t offset = 0;

	//odczyt ID z bufora
	readField(gps_buf, temp, &offset, buf_len);

	if (strcmp(temp, "GPVTG") == 0)
	{
		//$GPVTG,,,,,,,,,N*30
		//$GPVTG,,T,,M,0.834,N,1.545,K,A*29

		//Heading np 240.5
		if (readField(gps_buf, temp, &offset, buf_len)) {
			heading_GPSx100 = (uint16_t)(atof(temp) * 100);
			heading_GPSx100_flag = true; // flaga sygnalizująca, że z GPS przyszedł nowy, dobry heading
			regHGPSx10 = heading_GPSx100/10;
		}
		else {
			regHGPSx10 = heading_GPS_error;
		}

		// np 'T' indicates that track made good is relative to true north
		if (readField(gps_buf, temp, &offset, buf_len)) {}

		//Heading magnetyczny
		if (readField(gps_buf, temp, &offset, buf_len)) {}

		//np 'M' czyli Magnetic
		if (readField(gps_buf, temp, &offset, buf_len)) {}

		//Prędkość w węzłach
		if (readField(gps_buf, temp, &offset, buf_len)) {}

		//Jednostka prędkości w węzłach, zawsze 'N'
		if (readField(gps_buf, temp, &offset, buf_len)) {}

		//Prędkość w km/h
		if (readField(gps_buf, temp, &offset, buf_len)) {
			velocity_GPS_float_raw = fmin(atof(temp)*0.277777777f, 10.f); // wczytanie jako float w km/h i przeliczone na m/s;
			regVelocityx100 = (unsigned int)(velocity_GPS_float_raw * 100);
		}
		//Jednostka prędkości, zawsze 'K'
		//if (readField(gps_buf, temp, &offset, gps_buf_iter)) {}

	}
	else if (strcmp(temp, "GPGGA") == 0)
	{
		//$GPGGA,075813.00,,,,,0,00,99.99,,,,,,*6E
		//$GPGGA,080736.00,5017.27932,N,01840.61440,E,1,04,2.57,217.4,M,40.5,M,,*57

		//Czas
		if (readField(gps_buf, temp, &offset, buf_len)) {
			char hhmm[5] = { temp[0], temp[1], temp[2], temp[3], '\0' };
			gps_time[0] = atoi(hhmm);
			gps_time[1] = atoi(&temp[4]);
		}

		//Szerokość geograficzna
		if (readField(gps_buf, temp, &offset, buf_len)) {
			gps_latitude = calc_latitude(temp);
		}
		else {
			gps_latitude = LonError;
		}

		//Kierunek szerokości geograficznej
		if (readField(gps_buf, temp, &offset, buf_len)) {
			if (temp[0] == 'S') {
				gps_latitude = -gps_latitude;
			}
		}

		//Długość geograficzna
		if (readField(gps_buf, temp, &offset, buf_len)) {
			gps_longitude = calc_longitude(temp);
		}
		else {
			gps_longitude = LatError;
		}

		//Kierunek długości geograficznej
		if (readField(gps_buf, temp, &offset, buf_len)) {
			if (temp[0] == 'W') {
				gps_longitude = -gps_longitude;
			}
		}

		//Gps fix status 0=no fix, 1=gps fix, 2=gps dif fix
		if (readField(gps_buf, temp, &offset, buf_len)) {
			GPS_fix = atoi(temp);
		}

		//Ilość złapanych satelitów XX np. 05
		if (readField(gps_buf, temp, &offset, buf_len)) {

		}

		//Horyzontalna dokładność pozycji x.x = horizontal dilution of precision np. '99.9' lub '2.3'
		if (readField(gps_buf, temp, &offset, buf_len)) {
			float fhdop = atof(temp);
			int16_t hdop = fhdop * 10;
			GPS_fix |= (hdop << 4);
		}

		//Wysokość w metrach nad poziom morza np. '232.2'
		if (readField(gps_buf, temp, &offset, buf_len)) {
			gps_altitude = atof(temp);
		}
		//Jednostka wysokosći w metrach nad poziomem morza np. 'M'
		//if (readField(gps_buf, temp, &offset, gps_buf_iter)) {
		//}
		//wysokość geoidy (powyżej elipsoidy WGS84) np. '23.2'
		//if (readField(gps_buf, temp, &i, gps_buf_iter)) {
		//}
		//Znak wysokości geoidy (powyżej elipsoidy WGS84) np. 'M'
		//if (readField(gps_buf, temp, &i, gps_buf_iter)) {
		//}
		//x.x = Age of Differential GPS data (seconds)
		//if (readField(gps_buf, temp, &i, gps_buf_iter)) {
		//}
		//xxxx = Differential reference station ID
		//if (readField(gps_buf, temp, &i, gps_buf_iter)) {
		//}

		//Przeliczanie wartości szerokości i długości geograficznej, czasu i jakości pomiaru gpsu do rejestrów
		regLatHigh = (short)(gps_latitude * 100);
		regLatLow = (short)(gps_latitude * 1000000 - regLatHigh * 10000);
		regLonHigh = (short)(gps_longitude * 100);
		regLonLow = (short)(gps_longitude * 1000000 - regLonHigh * 10000);
		regTimeHM = gps_time[0];
		regTimeS = gps_time[1];
		regGPSfixQuality = GPS_fix;


#ifdef INFO_GPS
	printf("--- GPS INFO ---:\n");
	printf("LatHigh: %d, LatLow: %d\n", regLatHigh, regLatLow);
	printf("LonHigh: %d, LonLow: %d\n", regLonHigh, regLonLow);
	printf("TimeHM: %d, TimeS: %d\n", regTimeHM, regTimeS);
	printf("GPSfixQuality: %d, Heading: %d\n", regGPSfixQuality, regHGPSx10);
	printf("Velocity: %d\n\n", regVelocityx100);
#endif // INFO_GPS
	}
}


//Funkcja sprawdzająca ramkę w gps_buf[]
//Zwraca: true, jeżeli suma kontrolna ramki się zgadza (suma kontrolna znajduję się na końcu każdej ramki, między * a CR_CHAR)
bool gps_check_crc(char *buf, uint8_t len) {
	if (len < 10) //GPGGA... ..*FF0 - min dlugosc 9 znakow
		return false;

	uint8_t checksum = buf[0];

	for (int i = 1; i < (len - 3); i++)
		checksum ^= buf[i];

	char str_crc[3] = { buf[len - 2] , buf[len - 1], '\0'};
	uint8_t in_frame = (uint8_t)strtol(str_crc, NULL, 16);

#ifdef INFO_GPS_BUFFER
	printf("--- GPS CRC INFO ---:\n");
	printf("Buffer: %s\n", buf);
	printf("CRC Calculated: %x\n", checksum);
	printf("InFrame: %x\n\n", in_frame);
#endif // INFO_GPS

	return (checksum == in_frame);
}


//Funkcja odczytująca i przetwarzająca dane z modułu GPS
//Dla jak najmniejszych opóźnień funkcję powinno wykonywać się jak najczęściej
uint8_t readGPS()
{
	HAL_StatusTypeDef ret = HAL_OK;
		int frame_begin = 0;
		int i = 0;
		int len = 0;
		int index = 0;
		int tocopy = 0;
		int entry = UART_3_entry;  // just in case

		// Kopiowanie danych z bufora UART_Buffer_3 znajdujących się między znakami $ i CR
		// do bufora gps_buf, następnie po sprawdzeniu sumy kontrolnej przetworzenie danych
		GPS_dollar_detected = false;

		memset(gps_buf, 0, GPS_BUF_SIZE);

		for (i = 0; i < UART_BUFFER_3_SIZE; i++)
		{
			index = (i + entry) % UART_BUFFER_3_SIZE;
			if (GPS_dollar_detected)
			{
				if (CR_CHAR != UART_Buffer_3[index])
				{
					len++;
				}
				else
				{
					buf_len = len;
					/* Get number of bytes we can copy to the end of buffer */
					tocopy = UART_BUFFER_3_SIZE - frame_begin;

					/* Check how many bytes to copy */
					if (tocopy > len) tocopy = len;

					memcpy(gps_buf, &UART_Buffer_3[frame_begin], tocopy);
					memset(&UART_Buffer_3[frame_begin], 0, tocopy);

					len -= tocopy;
					if (len)
					{
						memcpy(gps_buf + tocopy, UART_Buffer_3, len);      /* Don't care if we override Read pointer now */
						memset(&UART_Buffer_3, 0, len);
					}

					if (gps_check_crc(gps_buf, tocopy + len))
					{
						gps_process_buff();
					}

					GPS_dollar_detected = false;
					len = 0;
				}
			}
			else
			{
				if ('$' == UART_Buffer_3[index])
				{
					UART_Buffer_3[index] = 0;
					frame_begin = index + 1;
					GPS_dollar_detected = true;
				}
			}
		}

		return ret;

}
