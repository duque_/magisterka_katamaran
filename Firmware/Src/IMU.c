#include <IMU.h>

extern I2C_HandleTypeDef        IMU_I2C;


/*
 * brief: Configure BNO sensor
 */
void IMU_Init(void)
{
	/* Select BNO055 config mode */
	uint8_t opr_config_mode[2] = {BNO055_OPR_MODE, CONFIGMODE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, opr_config_mode, sizeof(opr_config_mode), 10);
	HAL_Delay(10);

	/* Select page 1 to configure sensors */
	uint8_t conf_page1[2] = {BNO055_PAGE_ID, 0x01};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_page1, sizeof(conf_page1), 10);
	HAL_Delay(10);

 	/* Configure ACC (Page 1; 0x08) */
	uint8_t conf_acc[2] = {BNO055_ACC_CONFIG, ACCEL_POWER_MODE << 5 | ACCEL_BANDWIDTH << 2 | ACCEL_FULL_SCALE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_acc, sizeof(conf_acc), 10);
	HAL_Delay(10);

	/* Configure GYR */
	uint8_t conf_gyro[2] = {BNO055_GYRO_CONFIG_0, GYRO_BANDWIDTH << 3 | GYRO_FULL_SCALE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_gyro, sizeof(conf_gyro), 10);
	HAL_Delay(10);

	uint8_t conf_gyro_pwr[2] = {BNO055_GYRO_CONFIG_1, GYRO_POWER_MODE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_gyro_pwr, sizeof(conf_gyro_pwr), 10);
	HAL_Delay(10);

	/* Configure MAG */
	uint8_t conf_mag_pwr[4] = {REG_WRITE, BNO055_MAG_CONFIG, 0x01, MAG_POWER_MODE << 5 | MAG_PERFORMANCE_MODE << 3 | MAG_ODR};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_mag_pwr, sizeof(conf_mag_pwr), 10);
	HAL_Delay(10);

	/* Select BNO055 gyro temperature source  */
	//PutHexString(START_BYTE, BNO055_TEMP_SOURCE, 0x01 );
	
	/* Select page 0 */
	uint8_t conf_page0[2] = {BNO055_PAGE_ID, 0x00};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, conf_page0, sizeof(conf_page0), 10);
	HAL_Delay(10);

	/* Select BNO055 sensor units (Page 0; 0x3B, default value 0x80) */
	/*- ORIENTATION_MODE		 - Android					(default)
		- VECTOR_ACCELEROMETER - m/s^2  					(default)
		- VECTOR_MAGNETOMETER  - uT								(default)
		- VECTOR_GYROSCOPE     - rad/s        v		(must be configured)
		- VECTOR_EULER         - degrees					(default)
		- VECTOR_LINEARACCEL   - m/s^2        v		(default)
		- VECTOR_GRAVITY       - m/s^2						(default)
	*/
	//const char conf_units[4] = {REG_WRITE, BNO055_UNIT_SEL, 0x01, 0x82};
	//SendAccelData(USART1, (uint8_t*)conf_units);
	//HAL_Delay(50);

	/* Select BNO055 system power mode (Page 0; 0x3E) */
	uint8_t pwr_pwrmode[2] = {BNO055_PWR_MODE, BNO055_POWER_MODE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, pwr_pwrmode, sizeof(pwr_pwrmode), 10);
	HAL_Delay(10);

	/* Select BNO055 system operation mode (Page 0; 0x3D) */
	uint8_t opr_oprmode[2] = {BNO055_OPR_MODE, BNO055_OPERATION_MODE};
	HAL_I2C_Master_Transmit(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, opr_oprmode, sizeof(opr_oprmode), 10);
	HAL_Delay(10);
}



uint8_t GetAccelData(uint8_t* str)
{
	return HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, BNO055_ACC_DATA_X_LSB, I2C_MEMADD_SIZE_8BIT, str, 6, 10);
}
uint8_t GetEulerData(uint8_t* str)
{
	return HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1,  BNO055_EUL_HEADING_LSB, I2C_MEMADD_SIZE_8BIT, str, 6, 10);
}

uint8_t GetAccelChipId(uint8_t *chip_id)
{
	return HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, BNO055_CHIP_ID, I2C_MEMADD_SIZE_8BIT, chip_id, 1, 10);
}


/*
 * brief:
 */
uint8_t IMU_Get_Heading(uint16_t *heading)
{
	uint8_t ret = 0;
	uint8_t buf[2];

	ret = HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, BNO055_EUL_HEADING_LSB, I2C_MEMADD_SIZE_8BIT, buf, 2, 10);

	*heading = ((buf[1] << 8) | buf[0]) / 16;

	return ret;
}


/*
 * brief:
 */
uint8_t GetAccelTemp(void)
{
	uint8_t temp;
	HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, BNO055_TEMP, I2C_MEMADD_SIZE_8BIT, &temp, 1, 10);
	return temp;
}


/*
 *  brief: Get IMU calibration values in hex value: 0xWXYZ, where:
 *  W – magnetometer calibration
 *  X – accelerometer calibration
 *  Y – gyroscope calibration
 *  Z – system calibration
 *  returns: _SUCCESS or _ERROR
 */
uint8_t IMU_Get_Calibration(uint16_t *calibration)
{
	uint8_t ret = 0;
	uint8_t buf = 0;
	uint8_t err = HAL_I2C_Mem_Read(&IMU_I2C, BNO055_I2C_ADDR_LO<<1, BNO055_CALIB_STAT, I2C_MEMADD_SIZE_8BIT, &buf, 1, 10);

	if (err == HAL_OK)
	{
		uint8_t cal_system  = (buf >> 6) & 0x03;
		uint8_t cal_gyro 	= (buf >> 4) & 0x03;
		uint8_t cal_acc 	= (buf >> 2) & 0x03;
		uint8_t cal_mag 	= (buf) & 0x03;

		*calibration = 0;
		*calibration |= ((uint16_t)cal_system);
		*calibration |= ((uint16_t)cal_gyro << 4);
		*calibration |= ((uint16_t)cal_acc  << 8);
		*calibration |= ((uint16_t)cal_mag  << 12);

		ret = _SUCCESS;
	}
	else
	{
		ret = _ERROR;
	}

	return ret;
}
