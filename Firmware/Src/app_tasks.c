#include "app_tasks.h"

extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart7;
extern UART_HandleTypeDef huart8;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;
extern I2C_HandleTypeDef  hi2c1;

extern float gps_latitude;			// Zmienna przechowujaca polozenie w stopniach
extern float gps_longitude;

extern short GPS_fix;
extern uint64_t lastRequestTime;

static bool IMU_calibrated;
static uint16_t imu_heading = 0;

/*
 * brief:
 */
void taskGPS(void *argument)
{
	osDelay(3000);
	InitGPS();

    for(;;)
    {
    	readGPS();
    	osDelay(100);
    }
}


/*
 * brief: 
 */
void taskMotors(void *argument)
{
	uint16_t path_heading = 0;
	uint16_t radius = 0;
	uint32_t iter = 0;
	uint8_t  err = 0;
	uint8_t changed = 0;

	int16_t L_motor_speed = 0;
	int16_t R_motor_speed = 0;

	regStopDistance = TURN_OFF_MOTORS_DISTANCE;
	regInertionTime = INERTION_TIME_CONSTANT;
	regAutoSpeed = 100;
	init_motors();

	for (;;)
	{
		if (regAutoControl == 0)  /* Tryb sterowania ręcznego */
		{
			if (changed == 1)
			{
				changed = 0;
				regPwmA = 0;
				regPwmB = 0;
			}
			L_motor_speed = regPwmA;
			R_motor_speed = regPwmB;
		}
		else  /* Tryb sterowania autonomicznego */
		{
			if (changed == 0) changed = 1;

			if (calculate_path(&path_heading, &radius, iter++) != 0)
			{
				err = -1;
				printf("ERROR calculating the path heading - function %s\n", __FUNCTION__);
			}

#ifdef  INFO_AUTO_CONTROL
			printf("Distance to the destination point: %u\n", radius);
#endif

			if (!err)
			{
				auto_calculate_motors_speed(&L_motor_speed, &R_motor_speed, imu_heading, path_heading, radius);
				regPwmA = (uint16_t)L_motor_speed;
				regPwmB = (uint16_t)R_motor_speed;
			}
			else
			{
				stop_motors();
				err = 0;
			}
		}

		/* Jeżeli od ostatniej wiadomości minęło LOST_COMMUNICATION_TIMEOUT, wyłącz silniki */
		if ((xTaskGetTickCount() - lastRequestTime) > pdMS_TO_TICKS(LOST_COMMUNICATION_TIMEOUT))
		{
			L_motor_speed = 0;
			R_motor_speed = 0;
			regPwmA = 0;
			regPwmB = 0;
		}

		/* Ustaw prędkości silników (prędkość zmieniana jest z inercją,
		 * aby zapobiegać prądom wstecznym, lub gwałtownemu zatrzymywaniu silników) */
		set_motors_speed(L_motor_speed, R_motor_speed);
		handle_destination_coordinates(regAutoControl);
		osDelay(MOTORS_SAMPLING_PERIOD);
	}
}


/*
 * brief: Get IMU heading and calibration
 */
void taskIMU(void *argument)
{
	osDelay(1000);
	IMU_Init();

	IMU_calibrated = false;

	for(;;)
	{
		IMU_Get_Calibration(&regIMUCalibStatus);

		if (IMU_Get_Heading(&imu_heading))
		{
			printf("ERROR reading heading from IMU - function %s\n", __FUNCTION__);
			IMU_Init();
		}
		regHeadingIMUx100 = imu_heading * 100;


#ifdef INFO_IMU
		printf("\nCALIBRATION STATUS:\n");
		printf("%x\n", regIMUCalibStatus);
		printf("Heading: %d\n\n", imu_heading);
#endif

		osDelay(100);
	}
}
