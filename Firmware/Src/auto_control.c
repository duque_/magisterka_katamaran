#include "auto_control.h"

extern float gps_latitude;
extern float gps_longitude;
extern float gps_altitude;

/*
 * brief:
 */
void clear_dest_coordinations(void)
{
	regAutoLatHigh = NO_DEST_COORDINATES;
	regAutoLatLow  = NO_DEST_COORDINATES;
	regAutoLonHigh = NO_DEST_COORDINATES;
	regAutoLonLow  = NO_DEST_COORDINATES;
}


/*
 * brief:
 */
void handle_destination_coordinates(int16_t AutoControl_flag)
{
	static uint8_t dest_coordinations_cleared = 0;

	if (AutoControl_flag == 0)
	{
		if (dest_coordinations_cleared == 0)
		{
			clear_dest_coordinations();
			dest_coordinations_cleared = 1;
		}
	}
	else
	{
		if (dest_coordinations_cleared != 0)
		dest_coordinations_cleared = 0;
	}
}


/*
 *  brief: Function calculates destination coordinates as float numbers from
 *  	   integer Modbus coils. The values of the coils are being written to
 *  	   the last_regAuto* variables, to prevent float calculations every
 *  	   time the function is called.
 *  return:  0: success,
 *  		-1: failure
 */
int8_t get_destination(float *dest_latitude, float *dest_longitude)
{
	static uint16_t last_regAutoLatHigh = 0;
	static uint16_t last_regAutoLatLow  = 0;
	static uint16_t last_regAutoLonHigh = 0;
	static uint16_t last_regAutoLonLow  = 0;

	uint8_t ret = _SUCCESS;

	if ((last_regAutoLatHigh != regAutoLatHigh) || (last_regAutoLatLow != regAutoLatLow))
	{
		if ((regAutoLatHigh != NO_DEST_COORDINATES) && (regAutoLatLow != NO_DEST_COORDINATES))
		{
			printf("Destination latitude has been set\n");
			*dest_latitude = (float)regAutoLatHigh * 0.01 + (float)regAutoLatLow * 0.000001;
		}
		else
		{
			printf("ERROR: Destination latitude has not been set\n");
			ret = _ERROR;
		}
		last_regAutoLatHigh = regAutoLatHigh;
		last_regAutoLatLow = regAutoLatLow;
	}

	if ((last_regAutoLonHigh != regAutoLonHigh) || (last_regAutoLonLow != regAutoLonLow))
	{
		if ((regAutoLonHigh != NO_DEST_COORDINATES) &&(regAutoLonLow != NO_DEST_COORDINATES))
		{
			printf("Destination longitude has been set\n");
			*dest_longitude = (float)regAutoLonHigh * 0.01 + (float)regAutoLonLow * 0.000001;
		}
		else
		{
			printf("ERROR: Destination longitude has not been set\n");
			ret = _ERROR;
		}
		last_regAutoLonHigh = regAutoLonHigh;
		last_regAutoLonLow = regAutoLonLow;
	}

	if ((regAutoLatHigh == NO_DEST_COORDINATES) || (regAutoLatLow == NO_DEST_COORDINATES) ||
		(regAutoLonHigh == NO_DEST_COORDINATES) || (regAutoLonLow == NO_DEST_COORDINATES))
	{
		ret = _ERROR;
	}

	return ret;
}


/*
 *  brief: calculates distance from the object to the destination place
 *  return: radius value in meters
 */
uint16_t calculate_radius(float dest_latitude, float dest_longitude)
{
	static double earth_radius = 0;
	static double e_rad = 6378.1370;	/* Length of equatorial radius [km] */
	static double p_rad = 6356.7523;	/* Length of polar radius [km] */
	double radius = -1;

	/* Calculate earth radius only once */
	if (earth_radius == 0)
	{
		if ((gps_altitude != 0) && (gps_latitude != 0))
		{
			double lat = gps_latitude * (M_PI/180.0);
			double x = pow(e_rad*e_rad*cos(lat), 2) + pow(p_rad*p_rad*sin(lat), 2);
			double y = pow(e_rad*cos(lat), 2) + pow(p_rad*sin(lat), 2);
			earth_radius = sqrt(x/y) + (gps_altitude/1000.0);
		}
	}
	else if ((gps_latitude != 0) && (gps_longitude != 0))
	{
		/* Using haversine formula */
		double gps_lat = gps_latitude * (M_PI/180.0);
		double gps_lon = gps_longitude * (M_PI/180.0);
		double dest_lat = dest_latitude * (M_PI/180.0);
		double dest_lon = dest_longitude * (M_PI/180.0);
		double d_lat = (gps_lat - dest_lat)/2.0;
		double d_lon = (gps_lon - dest_lon)/2.0;
		double temp = sin(d_lat)*sin(d_lat) + cos(gps_lat)*cos(dest_lat)*sin(d_lon)*sin(d_lon);
		radius = 2000.0 * earth_radius * asin(sqrt(temp));
	}

	return (uint16_t)radius;
}


/*
 *  brief: Calculates path heading and radius
 *  return:  0: success,
 *  		-1: failure
 */
uint8_t calculate_path(uint16_t *path_heading, uint16_t *radius, uint32_t iter)
{
	uint8_t err = _SUCCESS;
	static float dest_latitude = 0;
	static float dest_longitude = 0;
	double heading = 0;

	err = get_destination(&dest_latitude, &dest_longitude);

	if (!err)
	{
		/* Heading is being calculated from angles in degrees, but the output value
		   is in radians. Input angles' units are negligible due to division.
		   Then, the value has to be transformed back to degrees. */
		heading = atan((dest_longitude - gps_longitude)/(dest_latitude - gps_latitude));
		heading = heading * (180.0/M_PI);

		if (dest_longitude < gps_longitude)
		{
			if (dest_latitude >= gps_latitude)
			{
				heading += 360;
			}
			else
			{
				heading += 180;
			}
		}
		else
		{
			if (dest_latitude < gps_latitude)
			{
				heading += 180;
			}
		}

		*path_heading = (uint16_t)round(heading);

		if (iter % 10 == 0)
		{
			*radius = calculate_radius(dest_latitude, dest_longitude);
		}
		if (*radius < 0)
		{
			err = _ERROR;
		}
	}
	else
	{
		*path_heading = -1;
	}

	return err;
}


/*
 * brief:
 */
uint8_t auto_calculate_motors_speed(int16_t *L_speed, int16_t *R_speed, uint16_t imu_heading, uint16_t path_heading, uint16_t radius)
{
	if (radius < regStopDistance)					/* Zatrzymaj silniki, jeżeli katamaran znajduje się blisko celu */
	{
#ifdef  INFO_AUTO_CONTROL
		if (radius > 0)
			printf("Achieved destination point\n");
		else
			printf("Radius has not been calculated\n");
#endif
		*L_speed = 0;
		*R_speed = 0;
	}
	else
	{
		int16_t phi = imu_heading - path_heading;  			/* φ – Kąt pomiędzy kierunkiem headingu, a kierunkiem do punktu docelowego
								 	 	 	 	 	 	 	   Jeżeli wartość jest ujemna, obiekt skierowany jest zbyt w lewo */
		if (phi > 180) phi -= 360;
		else if (phi < -180) phi += 360;

		if (phi < 0)										/* Dla kąta phi = -180° silniki obracają sie w przeciwnym kierunku, wraz ze  */
		{										   	   		/* zmniejszaniem się wartości absolutnej kąta zmniejsza się również różnica  */
			*R_speed = 255; 								/* prędkości silników. Przy phi = -90° lewy silnik kręci się z całą mocą,    */
			*L_speed = (int16_t)(2.8333 * phi + 255.0);		/* a prawy w ogóle. Dla kątów z zakresu (-90°, 0°) prawy silnik przyspiesza, */
		}										   	   		/* aż do zrównania prędkości silników przy phi = 0°. 						 */
		else
		{
			*R_speed = (int16_t)(-2.8333 * phi + 255.0);	/* Przypadek analogiczny, zmienia się jedynie znak współczynnika. */
			*L_speed = 255;
		}
	}

	if (regAutoSpeed > 100) regAutoSpeed = 100;
	else if (regAutoSpeed < 0) regAutoSpeed = 0;

	if (regAutoSpeed != 100)
	{
		float lspeed = *L_speed * ((float)regAutoSpeed)/100.0;
		float rspeed = *R_speed * ((float)regAutoSpeed)/100.0;

		*L_speed = (int16_t)lspeed;
		*R_speed = (int16_t)rspeed;

		printf("nastawy ll rr %d %d\n",*L_speed, *R_speed);
	}

	return 0;
}




