#include <motors_control.h>


extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim9;


/*
 *	brief: Initialise motors' PWMs
 */
void init_motors(void)
{
#if (MOTORS_PWM == 1)
	pwm_start(LEFT_MOTOR);
	pwm_start(RIGHT_MOTOR);
	stop_motors();

#elif (MOTORS_OTHER == 1)
	#error "This is just a dummy motor type for future development"
#endif
}


/*
 * brief: Set speed of the motors with inertion. To prevent motors
 * 		  from reverse current and to smoothen speed setting, the
 * 		  1-st grade inertion with time costant T has been implemented.
 */
void set_motors_speed(int16_t left_speed, int16_t right_speed)
{
#if (MOTORS_PWM == 1)
	static float L_speed = 0;
	static float R_speed = 0;
	static uint16_t Tp = MOTORS_SAMPLING_PERIOD; 	/* Okres próbkowania */
	float T = (float)regInertionTime;		/* Stała czasowa inercji nastawy silnika [ms] */

	L_speed = ((Tp/T) * (left_speed - L_speed) + L_speed);
	R_speed = ((Tp/T) * (right_speed - R_speed) + R_speed);

	pwm_set_motor_speed(LEFT_MOTOR, (int16_t)L_speed);
	pwm_set_motor_speed(RIGHT_MOTOR, (int16_t)R_speed);

#ifdef INFO_SPEED
	printf("\nregA: %d, regB: %d\n", regPwmA, regPwmB);
#endif

#elif (MOTORS_OTHER == 1)
	#error "This is just a dummy motor type for future development"
#endif
}


/*
 *	brief: Stops motors
 */
void stop_motors(void)
{
	regPwmA = 0;
	regPwmB = 0;
	set_motors_speed(0, 0);
}


#if (MOTORS_PWM == 1)
/*
 * brief: Initialise specified PWM timer
 */
void pwm_start(uint8_t pwm_id)
{
	switch (pwm_id)
	{
		case PWM1:
			HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1); // PWM 1
			break;
		case PWM2:
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1); // PWM 2
			break;
		case PWM3:
			HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1); // PWM 3
			break;
		case PWM4:
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2); // PWM 4
			break;
		default:
			printf("ERROR: wrong PWM ID! (%s)\n", __FUNCTION__);
			break;
	}
}


/*
 * brief: This function changes the 'speed' value from range (-255, 255) to values in range (510, 1020)
 * 		  This operation casts value -255 to the 1 ms of pulse width and 255 to the 2 ms
 * 		  The 1 ms width PWM signal corresponds with maximum speed backwards, whereas 2 ms sets
 * 		  the motor to the maximum speed forward. Values between sets motor speed with the lower speed accordingly,
 * 		  with stopping the motor at the 1.5 ms PWM width. The frequency of each PWM equals 50 Hz.
 * 		  The following settings have to be implemented in the timers:
 * 		  TIM_CLK = 72 MHz, Prescaler = 141, Counter Period = 10200
 * 		  Just do not touch TIM2, TIM3 and TIM9 in the CubeMX and everything will be OK.
 */
void pwm_set_motor_speed(uint8_t pwm_id, int16_t speed)
{
	speed = speed + 765;
	if (speed < 510) speed = 510;
	else if (speed > 1020) speed = 1020;

#ifdef INFO_SPEED
	printf("ID: %d, speed: %d\n", pwm_id, speed - 765);
#endif

	switch (pwm_id)
	{
		case PWM1:
			TIM9->CCR1 = speed; // PWM 1
			break;
		case PWM2:
			TIM2->CCR1 = speed; // PWM 2
			break;
		case PWM3:
			TIM3->CCR1 = speed; // PWM 3
			break;
		case PWM4:
			TIM2->CCR2 = speed; // PWM 4
			break;
		default:
			printf("ERROR: wrong PWM ID! (%s)\n", __FUNCTION__);
			break;
	}
}
#elif (MOTORS_OTHER == 1)
	/* dummy */
#endif








