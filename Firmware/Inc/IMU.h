#ifndef __IMU_H
#define __IMU_H

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "bno055.h"
#include "DeviceInfo.h"

/* IMU settings */
#define GYRO_POWER_MODE 		NormalG				// Select gyroscope power mode
#define GYRO_FULL_SCALE			GFS_2000DPS			// Select gyroscope full scale
#define	GYRO_BANDWIDTH			GBW_230Hz			// Select gyroscope bandwidth

#define	ACCEL_FULL_SCALE		AFS_16G				// Select accelerometer full scale
#define	ACCEL_POWER_MODE		NormalA				// Select accelerometer power mode
#define ACCEL_BANDWIDTH			ABW_250Hz			// Select accelerometer bandwidth, accel sample rate divided by ABW_divx

#define MAG_FULL_SCALE			MFS_4Gauss			// Select magnetometer full-scale resolution
#define MAG_PERFORMANCE_MODE	EnhancedRegular  	// Select magnetometer perfomance mode
#define MAG_POWER_MODE			Normal    			// Select magnetometer power mode
#define MAG_ODR 				MODR_30Hz    		// Select magnetometer ODR when in BNO055 bypass mode

#define BNO055_POWER_MODE		Normalpwr			// Select BNO055 power mode
#define	BNO055_OPERATION_MODE   NDOF				// specify operation mode for sensors [ACCONLY|MAGONLY|GYROONLY|ACCMAG|ACCGYRO|MAGGYRO|AMG|NDOF|NDOF_FMC_OFF]


void 	IMU_Init(void);
uint8_t IMU_Get_Calibration(uint16_t *calibration);
uint8_t IMU_Get_Heading(uint16_t *heading);



uint8_t GetAccelData(uint8_t* str);
uint8_t GetEulerData(uint8_t* str);
uint8_t GetAccelChipId(uint8_t *chip_id);
uint8_t GetAccelTemp(void);


#endif // __IMU_H
