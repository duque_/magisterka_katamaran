#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "DeviceInfo.h"
#include "IMU.h"
#include "motors_control.h"

#define NO_DEST_COORDINATES 	60000

int8_t get_destination(float *dest_latitude, float *dest_longitude);
void clear_dest_coordinations(void);
void handle_destination_coordinates(int16_t AutoControl_flag);
uint8_t calculate_path(uint16_t *path_heading, uint16_t *radius, uint32_t iter);
uint8_t auto_calculate_motors_speed(int16_t *L_speed, int16_t *R_speed, uint16_t imu_heading, uint16_t path_heading, uint16_t radius);
