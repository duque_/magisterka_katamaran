#pragma once

#ifndef DEVICEINFO_H_
#define DEVICEINFO_H_

#include <stdint.h>

/* Debug info */
#define DEBUG_INFO   1


#if (DEBUG_INFO == 1)
	//#define INFO_GENERAL
	//#define INFO_MODBUS
	//#define INFO_XBEE_BUFFER
	//#define INFO_GPS
	//#define INFO_GPS_BUFFER
	//#define INFO_IMU
	//#define INFO_AUTO_CONTROL
	//#define INFO_SPEED
#endif // (DEBUG_INFO == 1)


/* Communication interfaces */
#define IMU_I2C						hi2c1		 	/* IMU communication interface - I2C */


/* Motors */
#define CHANGE_DIRECTION_DELAY  	500 			/* Czas [ms] opóźnienie podczas zmiany kierunku płynięcia/obrotu silników */
#define LOST_COMMUNICATION_TIMEOUT  2500  			/* Czas w [ms] po jakim zostana wylaczone silniki gdy zerwana zostanie komunikacja */
#define TURN_OFF_MOTORS_DISTANCE	3				/* Odległość w metrach katamaranu od miejsca docelowego, przy której silniki zostają wyłączane */

/* Modbus */
#define MODBUS_ADDRESS				1				/* Adres w sieci Modbus */

#define RS485_RESPONSE_TIMEOUT		200 			//ms - czas oczekiwania na odpowiedź urządzenia SLAVE

#define HOLDING_REG_LENGTH			26				/* Dlugosc rejestru danych pomiarowych */
#define COIL_REG_LENGTH				16  			/* Dlugosc rejestru wyjsc binarnych */
#define STATUS_REG_LENGTH			21				/* Długość rejestru statusow */


extern uint16_t  ANALOG_REG[HOLDING_REG_LENGTH];    /* 16 bitowy rejestr analogowy */
extern uint16_t  COIL_REG;			/* Coil status */

#define regPwmA               ANALOG_REG[0]    		/* Wypelnienie kanalu A -255 - 255 */
#define regPwmB               ANALOG_REG[1]    		/* Wypelnienie kanalu B -255 - 255 */
#define regCurrentA           ANALOG_REG[2]    		/* Pobor pradu kanalu A w mA */
#define regCurrentB           ANALOG_REG[3]    		/* Pobor pradu kanalu B w mA */
#define regLatHigh            ANALOG_REG[4]    		/* Szerokosc geograficzna czesc 1    np dla wartosci w stopniach 50.284316: regLatHigh = 5028 */
#define regLatLow             ANALOG_REG[5]    		/* Szerokosc geograficzna czesc 2                                           regLatLow = 4316  */
#define regLonHigh            ANALOG_REG[6]    		/* Dlugosc geograficzna czesc 1      np dla wartosci w stopniach 18.701488: regLonHigh = 1870 */
#define regLonLow             ANALOG_REG[7]    		/* Dlugosc geograficzna czesc 2                                             regLonLow = 1488  */
#define regTimeHM             ANALOG_REG[8]    		/* Czas z GPS w formacie HHMM        np godzina 22:55:30 zostanie zapisana: regTimeHM = 2255 */
#define regTimeS              ANALOG_REG[9]    		/* Czas z GPS w formacie SS                                                 regTimeS = 30 */
#define regGPSfixQuality      ANALOG_REG[10]   		/* Status GPS */
#define regIMUCalibStatus     ANALOG_REG[11]   		/* Status kalibracji IMU 0-3 */
#define regHeadingIMUx100     ANALOG_REG[12]   		/* Orientacja z IMU w stopniach (kat pomiedzy osia Y shielda a polnoca) - od 0 do 360 liczone zgodnie z kierunkiem wskazowek zegara (pomnozone przez 100) */
#define regRotationDirection  ANALOG_REG[13]   		/* Predkosc obrotowa */
#define regVelocityx100       ANALOG_REG[14]   		/* Predkosc odczytana z GPS wyrazonej w m/s *100 */
#define regHGPSx10            ANALOG_REG[15]   		/* Heading  z GPS wyrazonej w deg *10 */
#define regError              ANALOG_REG[16]   		/* Rejestr stanu urzadzenia 0b0000 0000 0000 0000     0x0001 - bledna suma kontrolna w komunikacie GPS */
#define regVin				  ANALOG_REG[17]   		/* Napięcie akumulatora w mV (napięcie na magistrali modbus) */
#define regAutoControl  	  ANALOG_REG[18]   		/* Sterowanie autonomiczne: flaga przechowująca status sterowania. 0 – zwykle, 1 - autonomiczne. Tylko do odczytu. */
#define regAutoLatHigh        ANALOG_REG[19]   		/* sterowanie autonomiczne: szerokosc geograficzna celu czesc 1    np dla wartosci w stopniach 50.284316: regLatHigh = 5028 */
#define regAutoLatLow         ANALOG_REG[20]   		/* Sterowanie autonomiczne: szerokosc geograficzna celu czesc 2                                           regLatLow = 4316 */
#define regAutoLonHigh        ANALOG_REG[21]   		/* Sterowanie autonomiczne: dlugosc geograficzna celu czesc 1      np dla wartosci w stopniach 18.701488: regLonHigh = 1870 */
#define regAutoLonLow         ANALOG_REG[22]   		/* Sterowanie autonomiczne: dlugosc geograficzna celu czesc 2                                             regLonLow = 1488 */
#define regStopDistance       ANALOG_REG[23]		/* Sterowanie autonomiczne: odległość od celu, w jakiej silniki zostaną wyłączone [m] */
#define regInertionTime       ANALOG_REG[24]		/* Sterowanie autonomiczne: wartość stałej czasowej inercji nastaw silników [ms] */
#define regAutoSpeed          ANALOG_REG[25]		/* Sterowanie autonomiczne: prędkość katamaranu 0 - 100 */

/* Misc */
#define CR_CHAR 0x0D
#define LF_CHAR 0x0A

#define _SUCCESS              0
#define _ERROR               -1

#endif



