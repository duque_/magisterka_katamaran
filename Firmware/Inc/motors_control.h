#include <stdio.h>

#include "main.h"
#include "stm32f4xx_hal.h"
#include "retarget.h"
#include "DeviceInfo.h"

#define MOTORS_PWM 					1
#define MOTORS_OTHER				0

#if (MOTORS_PWM + MOTORS_OTHER > 1)
	#error "Only one motor type can be chosen"
#elif (MOTORS_PWM + MOTORS_OTHER == 0)
	#error "Motor type has not been chosen"
#endif

#define PWM1   						1
#define PWM2						2
#define PWM3						3
#define PWM4 						4

#define LEFT_MOTOR					PWM3
#define RIGHT_MOTOR 				PWM4

#define MOTORS_SAMPLING_PERIOD		100		/* Okres próbkowania */
#define INERTION_TIME_CONSTANT  	500.0  /* Stała czasowa inercji nastawy silnika [ms] */


void init_motors(void);
void set_motors_speed(int16_t L_speed, int16_t R_speed);
void stop_motors(void);

void pwm_start(uint8_t pwm_id);
void pwm_set_motor_speed(uint8_t pwm_id, int16_t speed);
