#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <string.h>

void USART_IrqHandler (UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma);

void DMA_IrqHandler (DMA_HandleTypeDef *hdma, uint8_t *DMA_RX_Buffer, size_t DMA_RX_Buffer_size, uint8_t *UART_Buffer, size_t UART_Buffer_size);
void DMA_IrqHandler_circular(DMA_HandleTypeDef *hdma, uint8_t *DMA_RX_Buffer, size_t DMA_RX_Buffer_size, uint8_t *UART_Buffer, size_t UART_Buffer_size, uint16_t *Write);

