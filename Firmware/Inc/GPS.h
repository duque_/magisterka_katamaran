#ifndef GPS_H_
#define GPS_H_

#include "main.h"
#include <stdbool.h>

#define GPS_BUF_SIZE 256 //Rozmiar bufora gps_buf[]

#define LatError 999 // te wartosci sa wpisywane gdy nie ma wlasciwej pozycji z GPS (a nie 0 0)
#define LonError 999
#define heading_GPS_error 9999

//*********GPS ZMIENNE **************************

extern uint16_t heading_GPSx100;
extern bool heading_GPSx100_flag;
extern float velocity_GPS_float_raw;  // dana odczytana z gps przeliczona na m/s ale bez kontroli poprawnosci - wykorzystywane do sprawdzenia przy liczeniu korekcji z H z GPS

//**************************************

uint8_t InitGPS();
uint8_t readGPS();  // odczytanie i dekodowanie linii NMEA z GPS  // xxx przerobil abu od razu do jakiegol bufora byla wczytywana cala linia, potem rozpoznawany rozkaz i poszczegllne pola z tego bufora

#endif
