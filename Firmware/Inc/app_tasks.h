#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

#include "DeviceInfo.h"

#include "GPS.h"
#include "IMU.h"
#include "motors_control.h"

#include "retarget.h"
#include "auto_control.h"

void taskGPS(void *argument);
void taskMotors(void *argument);
void taskIMU(void *argument);
