#ifndef MODBUS_H_
#define MODBUS_H_

#include "DeviceInfo.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "cmsis_os.h"
#include "task.h"
#include "cmsis_os.h"
#include "retarget.h"


#define MODBUS_EXC_WRONG_LRC_VALUE              0x00
#define MODBUS_EXC_ILLEGAL_FUNCTION             0x01
#define MODBUS_EXC_ILLEGAL_DATA_ADDRESS         0x02
#define MODBUS_EXC_ILLEGAL_DATA_VALUE           0x03
#define MODBUS_EXC_SLAVE_DEVICE_FAILURE         0x04
#define MODBUS_EXC_ACKNOWLEDGE                  0x05
#define MODBUS_EXC_SLAVE_DEVICE_BUSY            0x06
#define MODBUS_EXC_MEMORY_PARITY_ERROR          0x08
#define MODBUS_EXC_GATEWAY_PATH_UNAVAILABLE     0x0A
#define MODBUS_EXC_GATEWAY_TARGET_DEV_RESPOND   0x0B

extern uint64_t lastRequestTime;

void handleCommunication(void *pvParameters);
bool pollReceived_internal();
bool readXbee();
bool readSlave();
uint8_t Ascii2Hex(unsigned char *tab_new, unsigned char *tab_buff, uint8_t len);
char * Hex2Ascii(unsigned char *data, char *AsciiBuf, int length);
uint8_t lrc(unsigned char *tab, uint8_t length);
void Function_03(unsigned char *request);
void Function_05(unsigned char *request);
void Function_0F(unsigned char *request);
void Function_10(unsigned char *request);
void Function_11(unsigned char *request);
unsigned int CreateModbusFrame(void);
void ExceptionResponse(volatile unsigned char *request, unsigned char exceptionCode);
int ipow(int base, int exp);

#endif /* MODBUS_H_ */




