#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


void dec2hex(char *ptr);
void coord_dec2hex(char *ptr);
void stop(uint8_t *frame);
void coordinates(uint8_t *lattitude, uint8_t *longitude, uint8_t *frame);
void radius(uint8_t *str, uint8_t *frame);
void inertion(uint8_t *str, uint8_t *frame);
void speed(uint8_t *str, uint8_t *frame);
uint8_t lrc(unsigned char *tab, uint8_t length);
uint8_t Ascii2Hex(unsigned char *src, unsigned char *dest, uint8_t len);

unsigned char frame_HEX[256] = {};

const unsigned char char_tab[128] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0,
									  0, 0, 0, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0a, 0x0b, 0x0c,
									  0x0d, 0x0e, 0x0f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									  0, 0, 0, 0, 0, 0, 0 
};


int main( int argc, char *argv[] )  
{
    uint8_t err = 0;
    uint8_t frame[256] = {0};
    uint8_t output[260] = {0};

    if (argc == 2 && !strcmp(argv[1], "stop")) 
    {
        stop(frame);
    }
    else if (argc == 4 && !strcmp(argv[1], "coord"))
    {
        coordinates(argv[2], argv[3], frame);
    }
    else if (argc == 3 && !strcmp(argv[1], "radius"))
    {
        radius(argv[2], frame);
    }    
    else if (argc == 3 && !strcmp(argv[1], "inertion"))
    {
        inertion(argv[2], frame);
    }    
    else if (argc == 3 && !strcmp(argv[1], "speed"))
    {
        speed(argv[2], frame);
    }  
    else
    {
        err = -1;
        printf("Usage:\n");
        printf("1. stop\n");
        printf("2. coord <lattitude> <longitude>\n");
        printf("3. radius <stop_distance> [m]\n");
        printf("4. inertion <time_constant> [ms]\n");
        printf("5. speed <value> (0 - 100)\n");
    }

    if (!err)
    {
        uint8_t modbus_idx_end = Ascii2Hex(frame, frame_HEX, strlen(frame) + 4);

        sprintf(output, "%s%X", frame, lrc(frame_HEX, modbus_idx_end));
        printf("\n%s\n\n", output);
    }
}


void stop(uint8_t *frame)
{
    /* switch back to manual control */
    sprintf(frame, "0110001200050A00000000000000000000");
}


void coordinates(uint8_t *lattitude, uint8_t *longitude, uint8_t *frame)
{
    char lat[9] = {0};
    char lon[9] = {0};
    /* create modbus frame enabling auto control 
           with latitude and longitude */
    int lat_size = strlen(lattitude);
    int lon_size = strlen(longitude);

    /* 6.2457 -> 62457000 */
    int j = 0;
    for(int i = 0; i < 8; i++)
    {
        if (lattitude[j] == '.') j++;
        if (j < lat_size) 
        {
            lat[i] = lattitude[j++];
        }
        else
        {
            lat[i] = '0';
        } 
    }

    /* Same for longitude */
    j = 0;
    for(int i = 0; i < 8; i++)
    {
        if (longitude[j] == '.') j++;
        if (j < lon_size) 
        {
            lon[i] = longitude[j++];
        }
        else
        {
            lon[i] = '0';
        } 
    }

    /* converse from dec to hex */
    coord_dec2hex(lat);
    coord_dec2hex(lat + 4);
    coord_dec2hex(lon);
    coord_dec2hex(lon + 4);

    sprintf(frame, "0110001200050A0001%s%s", lat, lon);
}


void radius(uint8_t *str, uint8_t *frame)
{       
    dec2hex(str);
    sprintf(frame, "01100017000102%s", str);
}


void inertion(uint8_t *str, uint8_t *frame)
{
    dec2hex(str);
    sprintf(frame, "01100018000102%s", str);
}


void speed(uint8_t *str, uint8_t *frame)
{   
    dec2hex(str);
    sprintf(frame, "01100019000102%s", str);
}




uint8_t lrc(unsigned char *tab, uint8_t length) 
{
	uint8_t checksum = 0;

	for (int i = 0; i < length; i++) {
		checksum -= tab[i];
	}
	return checksum;
}


//Zamienia ramke w formacie ASCII na format BINARNY
//Zwraca: indeks na ostatni element tablicy dest[]
uint8_t Ascii2Hex(unsigned char *src, unsigned char *dest, uint8_t len) {
	uint8_t i = len/2;

	while (i--) {
		dest[i] = char_tab[src[i*2]] * 16 + char_tab[src[i*2+1]];
	}

	return (len/2 - 2);
}

/* change 4 decimal numbers to 4 hex */
void dec2hex(char *ptr)
{
    char temp[10] = {0};
    int temp_i = 0;

    strcpy(temp, ptr);
    temp_i = atoi(temp);
    sprintf(temp, "%04X", temp_i);
    memset(ptr, 0, 5);
    memcpy(ptr, temp, 4);
}

/* change 4 decimal numbers to 4 hex */
void coord_dec2hex(char *ptr)
{
    char temp[5] = {0};
    int temp_i = 0;

    memcpy(temp, ptr, 4);
    temp_i = atoi(temp);
    sprintf(temp, "%04X", temp_i);
    memcpy(ptr, temp, 4);
}
