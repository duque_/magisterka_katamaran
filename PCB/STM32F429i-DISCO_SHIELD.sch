<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="stm32f429i-disco">
<packages>
<package name="STM32F429I-DISCO">
<pad name="5V1" x="8.89" y="-34.29" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="5V" x="6.35" y="-34.29" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF5" x="8.89" y="-36.83" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF4" x="6.35" y="-36.83" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF3" x="8.89" y="-39.37" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF2" x="6.35" y="-39.37" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF1" x="8.89" y="-41.91" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF0" x="6.35" y="-41.91" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC15" x="8.89" y="-44.45" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC14" x="6.35" y="-44.45" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC13" x="8.89" y="-46.99" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE6" x="6.35" y="-46.99" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE5" x="8.89" y="-49.53" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE4" x="6.35" y="-49.53" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE3" x="8.89" y="-52.07" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE2" x="6.35" y="-52.07" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE1" x="8.89" y="-54.61" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE0" x="6.35" y="-54.61" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB9" x="8.89" y="-57.15" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB8" x="6.35" y="-57.15" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="VDD" x="8.89" y="-59.69" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="BOOT0" x="6.35" y="-59.69" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB7" x="8.89" y="-62.23" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB6" x="6.35" y="-62.23" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB5" x="8.89" y="-64.77" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB4" x="6.35" y="-64.77" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB3" x="8.89" y="-67.31" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG15" x="6.35" y="-67.31" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG14" x="8.89" y="-69.85" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG13" x="6.35" y="-69.85" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG12" x="8.89" y="-72.39" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG11" x="6.35" y="-72.39" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG10" x="8.89" y="-74.93" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG9" x="6.35" y="-74.93" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD6" x="8.89" y="-77.47" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD7" x="6.35" y="-77.47" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD4" x="8.89" y="-80.01" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD5" x="6.35" y="-80.01" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD2" x="8.89" y="-82.55" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD3" x="6.35" y="-82.55" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD0" x="8.89" y="-85.09" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD1" x="6.35" y="-85.09" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC11" x="8.89" y="-87.63" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC12" x="6.35" y="-87.63" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA15" x="8.89" y="-90.17" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC10" x="6.35" y="-90.17" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA13" x="8.89" y="-92.71" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA14" x="6.35" y="-92.71" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA11" x="8.89" y="-95.25" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA12" x="6.35" y="-95.25" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA9" x="8.89" y="-97.79" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA10" x="6.35" y="-97.79" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC9" x="8.89" y="-100.33" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA8" x="6.35" y="-100.33" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC7" x="8.89" y="-102.87" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC8" x="6.35" y="-102.87" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG8" x="8.89" y="-105.41" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC6" x="6.35" y="-105.41" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG6" x="8.89" y="-107.95" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG7" x="6.35" y="-107.95" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG4" x="8.89" y="-110.49" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG5" x="6.35" y="-110.49" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="GND5" x="8.89" y="-113.03" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="GND" x="6.35" y="-113.03" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="3V" x="59.69" y="-34.29" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="3V1" x="57.15" y="-34.29" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="NC" x="59.69" y="-36.83" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF6" x="57.15" y="-36.83" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF7" x="59.69" y="-39.37" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF8" x="57.15" y="-39.37" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PH0" x="59.69" y="-44.45" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PH1" x="57.15" y="-44.45" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="NRST" x="59.69" y="-46.99" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="GND2" x="57.15" y="-46.99" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC0" x="59.69" y="-49.53" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC1" x="57.15" y="-49.53" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC2" x="59.69" y="-52.07" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC3" x="57.15" y="-52.07" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA0" x="59.69" y="-54.61" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA1" x="57.15" y="-54.61" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA2" x="59.69" y="-57.15" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA3" x="57.15" y="-57.15" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA4" x="59.69" y="-59.69" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA5" x="57.15" y="-59.69" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PA6" x="59.69" y="-62.23" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PA7" x="57.15" y="-62.23" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PC4" x="59.69" y="-64.77" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PC5" x="57.15" y="-64.77" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB0" x="59.69" y="-67.31" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB1" x="57.15" y="-67.31" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB2" x="59.69" y="-69.85" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="GND3" x="57.15" y="-69.85" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF11" x="59.69" y="-72.39" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF12" x="57.15" y="-72.39" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF13" x="59.69" y="-74.93" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF14" x="57.15" y="-74.93" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF15" x="59.69" y="-77.47" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="G0" x="57.15" y="-77.47" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG1" x="59.69" y="-80.01" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE7" x="57.15" y="-80.01" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE8" x="59.69" y="-82.55" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE9" x="57.15" y="-82.55" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE10" x="59.69" y="-85.09" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE11" x="57.15" y="-85.09" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE12" x="59.69" y="-87.63" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE13" x="57.15" y="-87.63" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PE14" x="59.69" y="-90.17" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PE15" x="57.15" y="-90.17" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB10" x="59.69" y="-92.71" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB11" x="57.15" y="-92.71" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB12" x="59.69" y="-95.25" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB13" x="57.15" y="-95.25" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PB14" x="59.69" y="-97.79" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PB15" x="57.15" y="-97.79" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD8" x="59.69" y="-100.33" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD9" x="57.15" y="-100.33" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD10" x="59.69" y="-102.87" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD11" x="57.15" y="-102.87" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD12" x="59.69" y="-105.41" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD13" x="57.15" y="-105.41" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PD14" x="59.69" y="-107.95" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PD15" x="57.15" y="-107.95" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PG2" x="59.69" y="-110.49" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PG3" x="57.15" y="-110.49" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="GND1" x="59.69" y="-113.03" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="GND4" x="57.15" y="-113.03" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<pad name="PF9" x="59.69" y="-41.91" drill="0.9" diameter="1.4224" shape="offset"/>
<pad name="PF10" x="57.15" y="-41.91" drill="0.9" diameter="1.4224" shape="offset" rot="R180"/>
<wire x1="0" y1="-0.2286" x2="0" y2="-43.18" width="0.127" layer="21"/>
<wire x1="0" y1="-0.2159" x2="0" y2="-0.2032" width="0.127" layer="21"/>
<wire x1="0" y1="-0.2032" x2="66.04138125" y2="-0.2036625" width="0.127" layer="21"/>
<wire x1="0" y1="-43.2054" x2="0" y2="-119.761" width="0.127" layer="21"/>
<wire x1="0" y1="-119.761" x2="66.04" y2="-119.761" width="0.127" layer="21"/>
<wire x1="66.04" y1="-0.2032" x2="66.04" y2="-119.761" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="STM32F429I-DISCO">
<wire x1="0" y1="0" x2="0" y2="-83.82" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="-83.82" width="0.254" layer="94"/>
<wire x1="17.78" y1="-83.82" x2="0" y2="-83.82" width="0.254" layer="94"/>
<pin name="5V1" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="5V" x="-5.08" y="-2.54" length="middle"/>
<pin name="PF5" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PF4" x="-5.08" y="-5.08" length="middle"/>
<pin name="PF3" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PF2" x="-5.08" y="-7.62" length="middle"/>
<pin name="PF1" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PF0" x="-5.08" y="-10.16" length="middle"/>
<pin name="PC15" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="PC14" x="-5.08" y="-12.7" length="middle"/>
<pin name="PC13" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PE6" x="-5.08" y="-15.24" length="middle"/>
<pin name="PE5" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PE4" x="-5.08" y="-17.78" length="middle"/>
<pin name="PE3" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PE2" x="-5.08" y="-20.32" length="middle"/>
<pin name="PE1" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PE0" x="-5.08" y="-22.86" length="middle"/>
<pin name="PB9" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB8" x="-5.08" y="-25.4" length="middle"/>
<pin name="VDD" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="BOOT0" x="-5.08" y="-27.94" length="middle"/>
<pin name="PB7" x="22.86" y="-30.48" length="middle" rot="R180"/>
<pin name="PB6" x="-5.08" y="-30.48" length="middle"/>
<pin name="PB5" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="PB4" x="-5.08" y="-33.02" length="middle"/>
<pin name="PB3" x="22.86" y="-35.56" length="middle" rot="R180"/>
<pin name="PG15" x="-5.08" y="-35.56" length="middle"/>
<pin name="PG14" x="22.86" y="-38.1" length="middle" rot="R180"/>
<pin name="PG13" x="-5.08" y="-38.1" length="middle"/>
<pin name="PG12" x="22.86" y="-40.64" length="middle" rot="R180"/>
<pin name="PG11" x="-5.08" y="-40.64" length="middle"/>
<pin name="PG10" x="22.86" y="-43.18" length="middle" rot="R180"/>
<pin name="PG9" x="-5.08" y="-43.18" length="middle"/>
<pin name="PD6" x="22.86" y="-45.72" length="middle" rot="R180"/>
<pin name="PD7" x="-5.08" y="-45.72" length="middle"/>
<pin name="PD4" x="22.86" y="-48.26" length="middle" rot="R180"/>
<pin name="PD5" x="-5.08" y="-48.26" length="middle"/>
<pin name="PD2" x="22.86" y="-50.8" length="middle" rot="R180"/>
<pin name="PD3" x="-5.08" y="-50.8" length="middle"/>
<pin name="PD0" x="22.86" y="-53.34" length="middle" rot="R180"/>
<pin name="PD1" x="-5.08" y="-53.34" length="middle"/>
<pin name="PC11" x="22.86" y="-55.88" length="middle" rot="R180"/>
<pin name="PC12" x="-5.08" y="-55.88" length="middle"/>
<pin name="PA15" x="22.86" y="-58.42" length="middle" rot="R180"/>
<pin name="PC10" x="-5.08" y="-58.42" length="middle"/>
<pin name="PA13" x="22.86" y="-60.96" length="middle" rot="R180"/>
<pin name="PA14" x="-5.08" y="-60.96" length="middle"/>
<pin name="PA11" x="22.86" y="-63.5" length="middle" rot="R180"/>
<pin name="PA12" x="-5.08" y="-63.5" length="middle"/>
<pin name="PA9" x="22.86" y="-66.04" length="middle" rot="R180"/>
<pin name="PA10" x="-5.08" y="-66.04" length="middle"/>
<pin name="PC9" x="22.86" y="-68.58" length="middle" rot="R180"/>
<pin name="PA8" x="-5.08" y="-68.58" length="middle"/>
<pin name="PC7" x="22.86" y="-71.12" length="middle" rot="R180"/>
<pin name="PC8" x="-5.08" y="-71.12" length="middle"/>
<pin name="PG8" x="22.86" y="-73.66" length="middle" rot="R180"/>
<pin name="PC6" x="-5.08" y="-73.66" length="middle"/>
<pin name="PG6" x="22.86" y="-76.2" length="middle" rot="R180"/>
<pin name="PG7" x="-5.08" y="-76.2" length="middle"/>
<pin name="PG4" x="22.86" y="-78.74" length="middle" rot="R180"/>
<pin name="PG5" x="-5.08" y="-78.74" length="middle"/>
<pin name="GND5" x="22.86" y="-81.28" length="middle" rot="R180"/>
<pin name="GND" x="-5.08" y="-81.28" length="middle"/>
<wire x1="66.04" y1="0" x2="66.04" y2="-83.82" width="0.254" layer="94"/>
<wire x1="66.04" y1="0" x2="83.82" y2="0" width="0.254" layer="94"/>
<wire x1="83.82" y1="0" x2="83.82" y2="-83.82" width="0.254" layer="94"/>
<wire x1="83.82" y1="-83.82" x2="66.04" y2="-83.82" width="0.254" layer="94"/>
<pin name="3V" x="88.9" y="-2.54" length="middle" rot="R180"/>
<pin name="3V1" x="60.96" y="-2.54" length="middle"/>
<pin name="NC" x="88.9" y="-5.08" length="middle" rot="R180"/>
<pin name="PF6" x="60.96" y="-5.08" length="middle"/>
<pin name="PF7" x="88.9" y="-7.62" length="middle" rot="R180"/>
<pin name="PF8" x="60.96" y="-7.62" length="middle"/>
<pin name="PF9" x="88.9" y="-10.16" length="middle" rot="R180"/>
<pin name="PF10" x="60.96" y="-10.16" length="middle"/>
<pin name="PH0" x="88.9" y="-12.7" length="middle" rot="R180"/>
<pin name="PH1" x="60.96" y="-12.7" length="middle"/>
<pin name="NRST" x="88.9" y="-15.24" length="middle" rot="R180"/>
<pin name="GND2" x="60.96" y="-15.24" length="middle"/>
<pin name="PC0" x="88.9" y="-17.78" length="middle" rot="R180"/>
<pin name="PC1" x="60.96" y="-17.78" length="middle"/>
<pin name="PC2" x="88.9" y="-20.32" length="middle" rot="R180"/>
<pin name="PC3" x="60.96" y="-20.32" length="middle"/>
<pin name="PA0" x="88.9" y="-22.86" length="middle" rot="R180"/>
<pin name="PA1" x="60.96" y="-22.86" length="middle"/>
<pin name="PA2" x="88.9" y="-25.4" length="middle" rot="R180"/>
<pin name="PA3" x="60.96" y="-25.4" length="middle"/>
<pin name="PA4" x="88.9" y="-27.94" length="middle" rot="R180"/>
<pin name="PA5" x="60.96" y="-27.94" length="middle"/>
<pin name="PA6" x="88.9" y="-30.48" length="middle" rot="R180"/>
<pin name="PA7" x="60.96" y="-30.48" length="middle"/>
<pin name="PC4" x="88.9" y="-33.02" length="middle" rot="R180"/>
<pin name="PC5" x="60.96" y="-33.02" length="middle"/>
<pin name="PB0" x="88.9" y="-35.56" length="middle" rot="R180"/>
<pin name="PB1" x="60.96" y="-35.56" length="middle"/>
<pin name="PB2" x="88.9" y="-38.1" length="middle" rot="R180"/>
<pin name="GND3" x="60.96" y="-38.1" length="middle"/>
<pin name="PF11" x="88.9" y="-40.64" length="middle" rot="R180"/>
<pin name="PF12" x="60.96" y="-40.64" length="middle"/>
<pin name="PF13" x="88.9" y="-43.18" length="middle" rot="R180"/>
<pin name="PF14" x="60.96" y="-43.18" length="middle"/>
<pin name="PF15" x="88.9" y="-45.72" length="middle" rot="R180"/>
<pin name="G0" x="60.96" y="-45.72" length="middle"/>
<pin name="PG1" x="88.9" y="-48.26" length="middle" rot="R180"/>
<pin name="PE7" x="60.96" y="-48.26" length="middle"/>
<pin name="PE8" x="88.9" y="-50.8" length="middle" rot="R180"/>
<pin name="PE9" x="60.96" y="-50.8" length="middle"/>
<pin name="PE10" x="88.9" y="-53.34" length="middle" rot="R180"/>
<pin name="PE11" x="60.96" y="-53.34" length="middle"/>
<pin name="PE12" x="88.9" y="-55.88" length="middle" rot="R180"/>
<pin name="PE13" x="60.96" y="-55.88" length="middle"/>
<pin name="PE14" x="88.9" y="-58.42" length="middle" rot="R180"/>
<pin name="PE15" x="60.96" y="-58.42" length="middle"/>
<pin name="PB10" x="88.9" y="-60.96" length="middle" rot="R180"/>
<pin name="PB11" x="60.96" y="-60.96" length="middle"/>
<pin name="PB12" x="88.9" y="-63.5" length="middle" rot="R180"/>
<pin name="PB13" x="60.96" y="-63.5" length="middle"/>
<pin name="PB14" x="88.9" y="-66.04" length="middle" rot="R180"/>
<pin name="PB15" x="60.96" y="-66.04" length="middle"/>
<pin name="PD8" x="88.9" y="-68.58" length="middle" rot="R180"/>
<pin name="PD9" x="60.96" y="-68.58" length="middle"/>
<pin name="PD10" x="88.9" y="-71.12" length="middle" rot="R180"/>
<pin name="PD11" x="60.96" y="-71.12" length="middle"/>
<pin name="PD12" x="88.9" y="-73.66" length="middle" rot="R180"/>
<pin name="PD13" x="60.96" y="-73.66" length="middle"/>
<pin name="PD14" x="88.9" y="-76.2" length="middle" rot="R180"/>
<pin name="PD15" x="60.96" y="-76.2" length="middle"/>
<pin name="PG2" x="88.9" y="-78.74" length="middle" rot="R180"/>
<pin name="PG3" x="60.96" y="-78.74" length="middle"/>
<pin name="GND1" x="88.9" y="-81.28" length="middle" rot="R180"/>
<pin name="GND4" x="60.96" y="-81.28" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F429I-DISCO">
<gates>
<gate name="G$1" symbol="STM32F429I-DISCO" x="-88.9" y="35.56"/>
</gates>
<devices>
<device name="" package="STM32F429I-DISCO">
<connects>
<connect gate="G$1" pin="3V" pad="3V"/>
<connect gate="G$1" pin="3V1" pad="3V1"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="5V1" pad="5V1"/>
<connect gate="G$1" pin="BOOT0" pad="BOOT0"/>
<connect gate="G$1" pin="G0" pad="G0"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="GND3" pad="GND3"/>
<connect gate="G$1" pin="GND4" pad="GND4"/>
<connect gate="G$1" pin="GND5" pad="GND5"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NRST" pad="NRST"/>
<connect gate="G$1" pin="PA0" pad="PA0"/>
<connect gate="G$1" pin="PA1" pad="PA1"/>
<connect gate="G$1" pin="PA10" pad="PA10"/>
<connect gate="G$1" pin="PA11" pad="PA11"/>
<connect gate="G$1" pin="PA12" pad="PA12"/>
<connect gate="G$1" pin="PA13" pad="PA13"/>
<connect gate="G$1" pin="PA14" pad="PA14"/>
<connect gate="G$1" pin="PA15" pad="PA15"/>
<connect gate="G$1" pin="PA2" pad="PA2"/>
<connect gate="G$1" pin="PA3" pad="PA3"/>
<connect gate="G$1" pin="PA4" pad="PA4"/>
<connect gate="G$1" pin="PA5" pad="PA5"/>
<connect gate="G$1" pin="PA6" pad="PA6"/>
<connect gate="G$1" pin="PA7" pad="PA7"/>
<connect gate="G$1" pin="PA8" pad="PA8"/>
<connect gate="G$1" pin="PA9" pad="PA9"/>
<connect gate="G$1" pin="PB0" pad="PB0"/>
<connect gate="G$1" pin="PB1" pad="PB1"/>
<connect gate="G$1" pin="PB10" pad="PB10"/>
<connect gate="G$1" pin="PB11" pad="PB11"/>
<connect gate="G$1" pin="PB12" pad="PB12"/>
<connect gate="G$1" pin="PB13" pad="PB13"/>
<connect gate="G$1" pin="PB14" pad="PB14"/>
<connect gate="G$1" pin="PB15" pad="PB15"/>
<connect gate="G$1" pin="PB2" pad="PB2"/>
<connect gate="G$1" pin="PB3" pad="PB3"/>
<connect gate="G$1" pin="PB4" pad="PB4"/>
<connect gate="G$1" pin="PB5" pad="PB5"/>
<connect gate="G$1" pin="PB6" pad="PB6"/>
<connect gate="G$1" pin="PB7" pad="PB7"/>
<connect gate="G$1" pin="PB8" pad="PB8"/>
<connect gate="G$1" pin="PB9" pad="PB9"/>
<connect gate="G$1" pin="PC0" pad="PC0"/>
<connect gate="G$1" pin="PC1" pad="PC1"/>
<connect gate="G$1" pin="PC10" pad="PC10"/>
<connect gate="G$1" pin="PC11" pad="PC11"/>
<connect gate="G$1" pin="PC12" pad="PC12"/>
<connect gate="G$1" pin="PC13" pad="PC13"/>
<connect gate="G$1" pin="PC14" pad="PC14"/>
<connect gate="G$1" pin="PC15" pad="PC15"/>
<connect gate="G$1" pin="PC2" pad="PC2"/>
<connect gate="G$1" pin="PC3" pad="PC3"/>
<connect gate="G$1" pin="PC4" pad="PC4"/>
<connect gate="G$1" pin="PC5" pad="PC5"/>
<connect gate="G$1" pin="PC6" pad="PC6"/>
<connect gate="G$1" pin="PC7" pad="PC7"/>
<connect gate="G$1" pin="PC8" pad="PC8"/>
<connect gate="G$1" pin="PC9" pad="PC9"/>
<connect gate="G$1" pin="PD0" pad="PD0"/>
<connect gate="G$1" pin="PD1" pad="PD1"/>
<connect gate="G$1" pin="PD10" pad="PD10"/>
<connect gate="G$1" pin="PD11" pad="PD11"/>
<connect gate="G$1" pin="PD12" pad="PD12"/>
<connect gate="G$1" pin="PD13" pad="PD13"/>
<connect gate="G$1" pin="PD14" pad="PD14"/>
<connect gate="G$1" pin="PD15" pad="PD15"/>
<connect gate="G$1" pin="PD2" pad="PD2"/>
<connect gate="G$1" pin="PD3" pad="PD3"/>
<connect gate="G$1" pin="PD4" pad="PD4"/>
<connect gate="G$1" pin="PD5" pad="PD5"/>
<connect gate="G$1" pin="PD6" pad="PD6"/>
<connect gate="G$1" pin="PD7" pad="PD7"/>
<connect gate="G$1" pin="PD8" pad="PD8"/>
<connect gate="G$1" pin="PD9" pad="PD9"/>
<connect gate="G$1" pin="PE0" pad="PE0"/>
<connect gate="G$1" pin="PE1" pad="PE1"/>
<connect gate="G$1" pin="PE10" pad="PE10"/>
<connect gate="G$1" pin="PE11" pad="PE11"/>
<connect gate="G$1" pin="PE12" pad="PE12"/>
<connect gate="G$1" pin="PE13" pad="PE13"/>
<connect gate="G$1" pin="PE14" pad="PE14"/>
<connect gate="G$1" pin="PE15" pad="PE15"/>
<connect gate="G$1" pin="PE2" pad="PE2"/>
<connect gate="G$1" pin="PE3" pad="PE3"/>
<connect gate="G$1" pin="PE4" pad="PE4"/>
<connect gate="G$1" pin="PE5" pad="PE5"/>
<connect gate="G$1" pin="PE6" pad="PE6"/>
<connect gate="G$1" pin="PE7" pad="PE7"/>
<connect gate="G$1" pin="PE8" pad="PE8"/>
<connect gate="G$1" pin="PE9" pad="PE9"/>
<connect gate="G$1" pin="PF0" pad="PF0"/>
<connect gate="G$1" pin="PF1" pad="PF1"/>
<connect gate="G$1" pin="PF10" pad="PF10"/>
<connect gate="G$1" pin="PF11" pad="PF11"/>
<connect gate="G$1" pin="PF12" pad="PF12"/>
<connect gate="G$1" pin="PF13" pad="PF13"/>
<connect gate="G$1" pin="PF14" pad="PF14"/>
<connect gate="G$1" pin="PF15" pad="PF15"/>
<connect gate="G$1" pin="PF2" pad="PF2"/>
<connect gate="G$1" pin="PF3" pad="PF3"/>
<connect gate="G$1" pin="PF4" pad="PF4"/>
<connect gate="G$1" pin="PF5" pad="PF5"/>
<connect gate="G$1" pin="PF6" pad="PF6"/>
<connect gate="G$1" pin="PF7" pad="PF7"/>
<connect gate="G$1" pin="PF8" pad="PF8"/>
<connect gate="G$1" pin="PF9" pad="PF9"/>
<connect gate="G$1" pin="PG1" pad="PG1"/>
<connect gate="G$1" pin="PG10" pad="PG10"/>
<connect gate="G$1" pin="PG11" pad="PG11"/>
<connect gate="G$1" pin="PG12" pad="PG12"/>
<connect gate="G$1" pin="PG13" pad="PG13"/>
<connect gate="G$1" pin="PG14" pad="PG14"/>
<connect gate="G$1" pin="PG15" pad="PG15"/>
<connect gate="G$1" pin="PG2" pad="PG2"/>
<connect gate="G$1" pin="PG3" pad="PG3"/>
<connect gate="G$1" pin="PG4" pad="PG4"/>
<connect gate="G$1" pin="PG5" pad="PG5"/>
<connect gate="G$1" pin="PG6" pad="PG6"/>
<connect gate="G$1" pin="PG7" pad="PG7"/>
<connect gate="G$1" pin="PG8" pad="PG8"/>
<connect gate="G$1" pin="PG9" pad="PG9"/>
<connect gate="G$1" pin="PH0" pad="PH0"/>
<connect gate="G$1" pin="PH1" pad="PH1"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Rembrandt Electronics - JST XH Connectors v1-0">
<packages>
<package name="JST-XH-05-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="7.45" y1="-2.3575" x2="7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="7.45" y1="3.3925" x2="-7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-7.45" y1="3.3925" x2="-7.45" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-7.45" y1="-2.3575" x2="7.45" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-8.055" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.2025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.6675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-5.1" y1="-2.3" x2="-5.1" y2="-1.8" width="0.254" layer="21"/>
<wire x1="5.1" y1="-2.3" x2="5.1" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-05-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="7.45" y1="-2.3575" x2="7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="7.45" y1="3.3925" x2="-7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-7.45" y1="3.3925" x2="-7.45" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-7.45" y1="-2.3575" x2="7.45" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" rot="R90"/>
<text x="-8.055" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.2025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.6675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-5.1" y1="-2.3" x2="-5.1" y2="-1.8" width="0.254" layer="21"/>
<wire x1="5.1" y1="-2.3" x2="5.1" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-04-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;
Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="6.2" y1="-2.3575" x2="6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="6.2" y1="3.3925" x2="-6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-6.2" y1="3.3925" x2="-6.2" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-2.3575" x2="6.2" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.655" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.0025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.4675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="-1.8" width="0.254" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-04-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="6.2" y1="-2.3575" x2="6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="6.2" y1="3.3925" x2="-6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-6.2" y1="3.3925" x2="-6.2" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-2.3575" x2="6.2" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-3.81" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" rot="R90"/>
<text x="-6.655" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.0025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.4675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="-1.8" width="0.254" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-1.8" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST-XH-05-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="2.54" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-05-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-05-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-XH-04-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-04-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-04-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="DC-DC-STEP-DOWN-LM2596">
<description>&lt;b&gt;DC/DC Step-Down Regulator&lt;/b&gt; based on &lt;b&gt;LM2596-ADJ&lt;/b&gt; chip</description>
<wire x1="-21.59" y1="10.414" x2="21.59" y2="10.414" width="0.127" layer="21"/>
<wire x1="21.59" y1="10.414" x2="21.59" y2="-10.414" width="0.127" layer="21"/>
<wire x1="21.59" y1="-10.414" x2="-21.59" y2="-10.414" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-10.414" x2="-21.59" y2="10.414" width="0.127" layer="21"/>
<pad name="IN+" x="-19.812" y="8.636" drill="1.5" diameter="2.5" shape="square"/>
<pad name="IN-" x="-19.812" y="-8.636" drill="1.5" diameter="2.5" shape="square"/>
<pad name="OUT-" x="19.812" y="-8.636" drill="1.5" diameter="2.5" shape="square"/>
<pad name="OUT+" x="19.812" y="8.636" drill="1.5" diameter="2.5" shape="square"/>
<hole x="-14.478" y="7.62" drill="3"/>
<hole x="15.24" y="-7.62" drill="3"/>
<text x="0" y="11.43" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-11.43" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<text x="-19.685" y="0" size="1.778" layer="21" rot="R90" align="center">IN</text>
<text x="19.685" y="0" size="1.778" layer="21" rot="R90" align="center">OUT</text>
<wire x1="19.685" y1="6.985" x2="19.685" y2="5.715" width="0.254" layer="21"/>
<wire x1="19.05" y1="6.35" x2="20.32" y2="6.35" width="0.254" layer="21"/>
<wire x1="-19.685" y1="-6.985" x2="-19.685" y2="-5.715" width="0.254" layer="21"/>
<wire x1="19.685" y1="-6.985" x2="19.685" y2="-5.715" width="0.254" layer="21"/>
<wire x1="-19.685" y1="6.985" x2="-19.685" y2="5.715" width="0.254" layer="21"/>
<wire x1="-20.32" y1="6.35" x2="-19.05" y2="6.35" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DC-DC-STEP-DOWN-LM2596">
<description>&lt;b&gt;DC/DC Step-Down Regulator&lt;/b&gt; based on &lt;b&gt;LM2596-ADJ&lt;/b&gt; chip</description>
<pin name="IN+" x="-17.78" y="5.08" visible="pin" length="middle"/>
<pin name="IN-" x="-17.78" y="-5.08" visible="pin" length="middle"/>
<pin name="OUT+" x="17.78" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="OUT-" x="17.78" y="-5.08" visible="pin" length="middle" rot="R180"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DC-DC-STEP-DOWN-LM2596">
<description>&lt;b&gt;DC/DC Step-Down Regulator&lt;/b&gt; based on &lt;b&gt;LM2596-ADJ&lt;/b&gt; chip
&lt;p&gt;&lt;b&gt;LM2596&lt;/b&gt; datasheet available here:&lt;br /&gt;&lt;a href="http://www.ti.com/lit/ds/symlink/lm2596.pdf"&gt;http://www.ti.com/lit/ds/symlink/lm2596.pdf&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/lm2596+power+regulator"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=DC-DC-STEP-DOWN-LM2596"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DC-DC-STEP-DOWN-LM2596" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DC-DC-STEP-DOWN-LM2596">
<connects>
<connect gate="G$1" pin="IN+" pad="IN+"/>
<connect gate="G$1" pin="IN-" pad="IN-"/>
<connect gate="G$1" pin="OUT+" pad="OUT+"/>
<connect gate="G$1" pin="OUT-" pad="OUT-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MAX3078EASA_T">
<packages>
<package name="SOIC127P600X175-8N">
<circle x="-4.445" y="2.495" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.445" y="2.495" radius="0.1" width="0.2" layer="51"/>
<wire x1="-2" y1="2.5" x2="2" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2" y1="2.52" x2="2" y2="2.52" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.52" x2="2" y2="-2.52" width="0.127" layer="21"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2" y1="2.5" x2="2" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-3.71" y1="2.75" x2="3.71" y2="2.75" width="0.05" layer="39"/>
<wire x1="-3.71" y1="-2.75" x2="3.71" y2="-2.75" width="0.05" layer="39"/>
<wire x1="-3.71" y1="2.75" x2="-3.71" y2="-2.75" width="0.05" layer="39"/>
<wire x1="3.71" y1="2.75" x2="3.71" y2="-2.75" width="0.05" layer="39"/>
<text x="-3.97" y="-2.697" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-3.97" y="2.697" size="1.27" layer="25">&gt;NAME</text>
<smd name="1" x="-2.475" y="1.905" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="2" x="-2.475" y="0.635" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="3" x="-2.475" y="-0.635" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="4" x="-2.475" y="-1.905" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="5" x="2.475" y="-1.905" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="6" x="2.475" y="-0.635" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="7" x="2.475" y="0.635" dx="1.97" dy="0.59" layer="1" roundness="25"/>
<smd name="8" x="2.475" y="1.905" dx="1.97" dy="0.59" layer="1" roundness="25"/>
</package>
</packages>
<symbols>
<symbol name="MAX3078EASA+T">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.41" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-15.24" width="0.41" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.41" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="15.24" width="0.41" layer="94"/>
<text x="-12.7" y="16.24" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-12.7" y="-19.24" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="!RE" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="DE" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="DI" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="A" x="-17.78" y="-2.54" length="middle"/>
<pin name="B" x="-17.78" y="-7.62" length="middle"/>
<pin name="VCC" x="17.78" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="RO" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="17.78" y="-12.7" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAX3078EASA+T" prefix="U">
<gates>
<gate name="A" symbol="MAX3078EASA+T" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="A" pin="!RE" pad="2"/>
<connect gate="A" pin="A" pad="6"/>
<connect gate="A" pin="B" pad="7"/>
<connect gate="A" pin="DE" pad="3"/>
<connect gate="A" pin="DI" pad="4"/>
<connect gate="A" pin="GND" pad="5"/>
<connect gate="A" pin="RO" pad="1"/>
<connect gate="A" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" 1/1 Transceiver Half RS422, RS485 8-SOIC "/>
<attribute name="MF" value="Maxim Integrated"/>
<attribute name="MP" value="MAX3078EASA+"/>
<attribute name="PACKAGE" value="SOIC-8 Maxim"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-1" urn="urn:adsk.eagle:footprint:8285/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.334" y="-0.635" size="1.27" layer="21" ratio="10">4</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA04-1" urn="urn:adsk.eagle:package:8337/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA04-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA04-1" urn="urn:adsk.eagle:symbol:8284/1" library_version="1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-1" urn="urn:adsk.eagle:component:8375/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8337/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="suwak">
<packages>
<package name="SUWAK">
<wire x1="-5.15055" y1="2.15275" x2="5.14945" y2="2.15275" width="0.127" layer="21"/>
<wire x1="-5.15055" y1="2.15275" x2="-5.15055" y2="-2.14725" width="0.127" layer="21"/>
<wire x1="-5.15055" y1="-2.14725" x2="1.94945" y2="-2.14725" width="0.127" layer="21"/>
<wire x1="1.94945" y1="-2.14725" x2="5.14945" y2="-2.14725" width="0.127" layer="21"/>
<wire x1="5.14945" y1="-2.14725" x2="5.14945" y2="-0.00055" width="0.127" layer="21"/>
<wire x1="5.14945" y1="-0.00055" x2="5.15" y2="0" width="0.127" layer="21"/>
<wire x1="5.15" y1="0" x2="5.14945" y2="2.15275" width="0.127" layer="21"/>
<wire x1="0.44945" y1="-3.84725" x2="1.94945" y2="-3.84725" width="0.127" layer="21"/>
<wire x1="1.94945" y1="-3.84725" x2="1.94945" y2="-2.14725" width="0.127" layer="21"/>
<wire x1="0.44945" y1="-2.14725" x2="0.44945" y2="-3.84725" width="0.127" layer="21"/>
<wire x1="-1.57555" y1="-3.84725" x2="-1.20055" y2="-3.84725" width="0.127" layer="21"/>
<wire x1="-0.87555" y1="-3.84725" x2="-0.50055" y2="-3.84725" width="0.127" layer="21"/>
<wire x1="-0.30055" y1="-3.47225" x2="-0.30055" y2="-3.09725" width="0.127" layer="21"/>
<wire x1="-0.30055" y1="-2.34725" x2="-0.30055" y2="-2.72225" width="0.127" layer="21"/>
<wire x1="-1.80055" y1="-2.34725" x2="-1.80055" y2="-2.72225" width="0.127" layer="21"/>
<wire x1="-1.80055" y1="-3.47225" x2="-1.80055" y2="-3.09725" width="0.127" layer="21"/>
<pad name="OFF" x="-2" y="0" drill="1" diameter="1.6764"/>
<pad name="IN" x="0" y="0" drill="1" diameter="1.6764"/>
<pad name="ON" x="2" y="0" drill="1" diameter="1.6764"/>
<text x="2.6986125" y="-3.600525" size="1.27" layer="21">ON</text>
<text x="-5.51238125" y="-3.6454625" size="1.27" layer="21">OFF</text>
</package>
</packages>
<symbols>
<symbol name="SUWAK">
<wire x1="-17.78" y1="12.7" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="-17.78" y2="5.08" width="0.254" layer="94"/>
<wire x1="-17.78" y1="5.08" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<pin name="ON" x="-12.7" y="17.78" length="middle" rot="R270"/>
<pin name="IN" x="-7.62" y="17.78" length="middle" rot="R270"/>
<pin name="OFF" x="-2.54" y="17.78" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SUWAK">
<gates>
<gate name="G$1" symbol="SUWAK" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="SUWAK">
<connects>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OFF" pad="OFF"/>
<connect gate="G$1" pin="ON" pad="ON"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X03" urn="urn:adsk.eagle:footprint:22340/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="1X03/90" urn="urn:adsk.eagle:footprint:22341/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="1X03" urn="urn:adsk.eagle:package:22458/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X03"/>
</packageinstances>
</package3d>
<package3d name="1X03/90" urn="urn:adsk.eagle:package:22459/2" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X03/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD3" urn="urn:adsk.eagle:symbol:22339/1" library_version="3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X3" urn="urn:adsk.eagle:component:22524/3" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22458/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22459/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="2N7000">
<packages>
<package name="TO-92">
<description>&lt;b&gt;&lt;/b&gt;</description>
<wire x1="-2.1" y1="-1.6" x2="2.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="-1.6" x2="-1.4" y2="2.3" width="0.2032" layer="21" curve="-98.0572"/>
<wire x1="2.1" y1="-1.6" x2="1.4" y2="2.3" width="0.2032" layer="21" curve="99.7984"/>
<wire x1="-1.4" y1="2.3" x2="1.4" y2="2.3" width="0.2032" layer="51" curve="-63.7816"/>
<text x="3.175" y="0.635" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="3.175" y="-1.27" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
</package>
</packages>
<symbols>
<symbol name="MFNS">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="4.1275" y2="1.905" width="0.1524" layer="94"/>
<wire x1="4.1275" y1="1.905" x2="4.1275" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.1275" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.1275" y1="0.762" x2="3.5719" y2="-0.4763" width="0.1524" layer="94"/>
<wire x1="3.5719" y1="-0.4763" x2="4.1275" y2="-0.4763" width="0.1524" layer="94"/>
<wire x1="4.1275" y1="-0.4763" x2="4.6831" y2="-0.4763" width="0.1524" layer="94"/>
<wire x1="4.6831" y1="-0.4763" x2="4.1275" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.4925" y1="0.762" x2="4.1275" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.1275" y1="0.762" x2="4.7625" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.7625" y1="0.762" x2="5.0165" y2="1.016" width="0.1524" layer="94"/>
<wire x1="3.4925" y1="0.762" x2="3.2385" y2="0.508" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<wire x1="4.1275" y1="-0.4763" x2="4.1275" y2="-1.905" width="0.1524" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="6.35" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="6.35" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="2.54" size="1.016" layer="95">D</text>
<text x="1.27" y="-3.175" size="1.016" layer="95">S</text>
<text x="-2.54" y="-1.27" size="1.016" layer="95">G</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="4.1275" y="0.635"/>
<vertex x="3.6513" y="-0.3969"/>
<vertex x="4.6038" y="-0.3969"/>
</polygon>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2N7000" prefix="Q">
<description>&lt;b&gt;N-Channel MOSFET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MFNS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-92">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" N-Channel 60V 350mA _Tc_ 1W _Tc_ Through Hole TO-92-3 "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="2N7000"/>
<attribute name="PACKAGE" value="TO-92 ON Semiconductor"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="eagle-ltspice" urn="urn:adsk.eagle:library:217">
<description>Default symbols for import LTspice schematics&lt;p&gt;
2012-10-29 alf@cadsoft.de&lt;br&gt;</description>
<packages>
<package name="0204/7" urn="urn:adsk.eagle:footprint:13215/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:13216/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:13233/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0402" urn="urn:adsk.eagle:footprint:13234/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:13235/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:13236/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1005" urn="urn:adsk.eagle:footprint:13237/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:13238/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:13239/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:13240/1" library_version="1">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:13241/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:13242/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:13243/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:13244/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:13245/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1" library_version="1">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:13247/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:13248/1" library_version="1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:13274/1" type="box" library_version="1">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:13275/1" type="box" library_version="1">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:13294/1" type="box" library_version="1">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:13296/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:13302/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:13300/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R1005" urn="urn:adsk.eagle:package:13297/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1005"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:13301/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:13299/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:13303/1" type="box" library_version="1">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:13309/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:13306/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:13304/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:13305/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:13311/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/1" type="box" library_version="1">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:13308/1" type="box" library_version="1">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:13307/1" type="box" library_version="1">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R" urn="urn:adsk.eagle:symbol:13232/1" library_version="1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="5.08" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" urn="urn:adsk.eagle:component:13322/1" prefix="R" uservalue="yes" library_version="1">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13274/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13275/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13294/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13296/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13302/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13300/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13297/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13301/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13299/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13303/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13309/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13306/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13304/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13305/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13311/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13308/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13307/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="0855035001">
<packages>
<package name="MOLEX_0855035001">
<wire x1="-3.175" y1="3.95" x2="12.065" y2="3.95" width="0.127" layer="51"/>
<wire x1="12.065" y1="3.95" x2="12.065" y2="-14.15" width="0.127" layer="51"/>
<wire x1="12.065" y1="-14.15" x2="-3.175" y2="-14.15" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-14.15" x2="-3.175" y2="3.95" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-14.15" x2="12.065" y2="-14.15" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.95" x2="-3.175" y2="3.95" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.95" x2="-3.175" y2="-5.35" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.95" x2="12.065" y2="-5.35" width="0.127" layer="21"/>
<wire x1="12.065" y1="-14.15" x2="12.065" y2="-7.35" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-14.15" x2="-3.175" y2="-7.35" width="0.127" layer="21"/>
<wire x1="-3.425" y1="4.2" x2="12.315" y2="4.2" width="0.05" layer="39"/>
<wire x1="12.315" y1="4.2" x2="12.315" y2="-14.4" width="0.05" layer="39"/>
<wire x1="12.315" y1="-14.4" x2="-3.425" y2="-14.4" width="0.05" layer="39"/>
<wire x1="-3.425" y1="-14.4" x2="-3.425" y2="4.2" width="0.05" layer="39"/>
<text x="-3.555" y="4.65" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.555" y="-14.85" size="1.778" layer="27" align="top-left">&gt;VALUE</text>
<circle x="-3.8" y="0" radius="0.1" width="0.2" layer="51"/>
<circle x="-3.8" y="0" radius="0.1" width="0.2" layer="21"/>
<pad name="1" x="0" y="0" drill="0.9" shape="square"/>
<pad name="2" x="1.27" y="2.54" drill="0.9"/>
<pad name="3" x="2.54" y="0" drill="0.9"/>
<pad name="4" x="3.81" y="2.54" drill="0.9"/>
<pad name="5" x="5.08" y="0" drill="0.9"/>
<pad name="6" x="6.35" y="2.54" drill="0.9"/>
<pad name="7" x="7.62" y="0" drill="0.9"/>
<pad name="8" x="8.89" y="2.54" drill="0.9"/>
<hole x="-1.27" y="-6.35" drill="3.3"/>
<hole x="10.16" y="-6.35" drill="3.3"/>
</package>
</packages>
<symbols>
<symbol name="0855035001">
<wire x1="-5.08" y1="-12.7" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="-5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="-5.588" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-13.97" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<pin name="1" x="-10.16" y="7.62" length="middle" direction="pas"/>
<pin name="2" x="-10.16" y="5.08" length="middle" direction="pas"/>
<pin name="3" x="-10.16" y="2.54" length="middle" direction="pas"/>
<pin name="4" x="-10.16" y="0" length="middle" direction="pas"/>
<pin name="5" x="-10.16" y="-2.54" length="middle" direction="pas"/>
<pin name="6" x="-10.16" y="-5.08" length="middle" direction="pas"/>
<pin name="7" x="-10.16" y="-7.62" length="middle" direction="pas"/>
<pin name="8" x="-10.16" y="-10.16" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0855035001" prefix="J">
<description>Jack Modular Connector 8p8c (RJ45, Ethernet) 90° Angle (Right) Unshielded Cat5e </description>
<gates>
<gate name="G$1" symbol="0855035001" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOLEX_0855035001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Jack Modular Connector 8p8c (RJ45, Ethernet) 90° Angle (Right) Unshielded Cat5e "/>
<attribute name="MF" value="Molex"/>
<attribute name="MP" value="0855035001"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="stm32f429i-disco" deviceset="STM32F429I-DISCO" device=""/>
<part name="LIDAR" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-05-PIN" device="-LONG-PAD" value="LIDAR"/>
<part name="U$2" library="diy-modules" deviceset="DC-DC-STEP-DOWN-LM2596" device="" value="STEP_DOWN"/>
<part name="U1" library="MAX3078EASA_T" deviceset="MAX3078EASA+T" device=""/>
<part name="U2" library="MAX3078EASA_T" deviceset="MAX3078EASA+T" device=""/>
<part name="ECHOSOUNDER" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA04-1" device="" package3d_urn="urn:adsk.eagle:package:8337/1" value="ECHOSOUNDER"/>
<part name="GPS" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-05-PIN" device="-LONG-PAD" value="GPS"/>
<part name="IMU" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-05-PIN" device="-LONG-PAD" value="IMU"/>
<part name="XBEE" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="XBee"/>
<part name="U$3" library="suwak" deviceset="SUWAK" device=""/>
<part name="PWM1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X3" device="" package3d_urn="urn:adsk.eagle:package:22458/2"/>
<part name="Q1" library="2N7000" deviceset="2N7000" device=""/>
<part name="R1" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="R2" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="PWM2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X3" device="" package3d_urn="urn:adsk.eagle:package:22458/2"/>
<part name="Q2" library="2N7000" deviceset="2N7000" device=""/>
<part name="R3" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="R4" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="PWM3" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X3" device="" package3d_urn="urn:adsk.eagle:package:22458/2"/>
<part name="Q3" library="2N7000" deviceset="2N7000" device=""/>
<part name="R5" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="R6" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="PWM4" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X3" device="" package3d_urn="urn:adsk.eagle:package:22458/2"/>
<part name="Q4" library="2N7000" deviceset="2N7000" device=""/>
<part name="R7" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="R8" library="eagle-ltspice" library_urn="urn:adsk.eagle:library:217" deviceset="R" device="" package3d_urn="urn:adsk.eagle:package:13300/1" value="10k"/>
<part name="UART2" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="UART2"/>
<part name="UART4" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="UART4"/>
<part name="I2C2" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="I2C2"/>
<part name="SPI5" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-05-PIN" device="-LONG-PAD" value="SPI5"/>
<part name="I2C3" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="I2C3"/>
<part name="J2" library="0855035001" deviceset="0855035001" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="33.02" y="71.12" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<wire x1="27.94" y1="68.58" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<label x="17.78" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5V1"/>
<wire x1="55.88" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<label x="66.04" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="3V1"/>
<wire x1="93.98" y1="68.58" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<label x="81.28" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3V"/>
<wire x1="121.92" y1="68.58" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<label x="132.08" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PE5"/>
<wire x1="55.88" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<label x="63.5" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF0"/>
<wire x1="27.94" y1="60.96" x2="17.78" y2="60.96" width="0.1524" layer="91"/>
<label x="12.7" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF1"/>
<wire x1="55.88" y1="60.96" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<label x="60.96" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART7_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF6"/>
<wire x1="93.98" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<label x="78.74" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_SCK" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF7"/>
<wire x1="121.92" y1="63.5" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<label x="127" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_MISO" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF8"/>
<wire x1="93.98" y1="63.5" x2="78.74" y2="63.5" width="0.1524" layer="91"/>
<label x="76.2" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_MOSI" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF9"/>
<wire x1="121.92" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<label x="127" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA0"/>
<wire x1="121.92" y1="48.26" x2="134.62" y2="48.26" width="0.1524" layer="91"/>
<label x="127" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA2"/>
<wire x1="121.92" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<label x="127" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA3"/>
<wire x1="93.98" y1="45.72" x2="81.28" y2="45.72" width="0.1524" layer="91"/>
<label x="78.74" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA5"/>
<wire x1="93.98" y1="43.18" x2="81.28" y2="43.18" width="0.1524" layer="91"/>
<label x="81.28" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA6"/>
<wire x1="121.92" y1="40.64" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<label x="129.54" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART7_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PE8"/>
<wire x1="121.92" y1="20.32" x2="134.62" y2="20.32" width="0.1524" layer="91"/>
<label x="127" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PB10"/>
<wire x1="121.92" y1="10.16" x2="134.62" y2="10.16" width="0.1524" layer="91"/>
<label x="127" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PB11"/>
<wire x1="93.98" y1="10.16" x2="83.82" y2="10.16" width="0.1524" layer="91"/>
<label x="78.74" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA1"/>
<wire x1="93.98" y1="48.26" x2="81.28" y2="48.26" width="0.1524" layer="91"/>
<label x="78.74" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PC6"/>
<wire x1="27.94" y1="-2.54" x2="15.24" y2="-2.54" width="0.1524" layer="91"/>
<label x="12.7" y="-2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PC7"/>
<wire x1="55.88" y1="0" x2="68.58" y2="0" width="0.1524" layer="91"/>
<label x="60.96" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PA8"/>
<wire x1="27.94" y1="2.54" x2="15.24" y2="2.54" width="0.1524" layer="91"/>
<label x="12.7" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PC9"/>
<wire x1="55.88" y1="2.54" x2="68.58" y2="2.54" width="0.1524" layer="91"/>
<label x="60.96" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PC12"/>
<wire x1="27.94" y1="15.24" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<label x="12.7" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PD2"/>
<wire x1="55.88" y1="20.32" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
<label x="60.96" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PB3"/>
<wire x1="55.88" y1="35.56" x2="68.58" y2="35.56" width="0.1524" layer="91"/>
<label x="63.5" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PB7"/>
<wire x1="55.88" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<label x="60.96" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PB8"/>
<wire x1="27.94" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<label x="12.7" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART8_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PE0"/>
<wire x1="27.94" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<label x="12.7" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART8_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PE1"/>
<wire x1="55.88" y1="48.26" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<label x="60.96" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_EN" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PC3"/>
<wire x1="93.98" y1="50.8" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
<label x="78.74" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="ECHOSOUNDER_EN" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF3"/>
<wire x1="55.88" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<label x="50.8" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPS_PPS" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PE13"/>
<wire x1="93.98" y1="15.24" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<label x="78.74" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="27.94" y1="-10.16" x2="12.7" y2="-10.16" width="0.1524" layer="91"/>
<label x="12.7" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND5"/>
<wire x1="55.88" y1="-10.16" x2="68.58" y2="-10.16" width="0.1524" layer="91"/>
<label x="63.5" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND4"/>
<wire x1="93.98" y1="-10.16" x2="81.28" y2="-10.16" width="0.1524" layer="91"/>
<label x="81.28" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND1"/>
<wire x1="121.92" y1="-10.16" x2="139.7" y2="-10.16" width="0.1524" layer="91"/>
<label x="134.62" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND2"/>
<wire x1="93.98" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<label x="81.28" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOCTL" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="PF5"/>
<wire x1="55.88" y1="66.04" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
<label x="60.96" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="LIDAR" gate="-1" x="114.3" y="-30.48" smashed="yes">
<attribute name="NAME" x="116.84" y="-31.242" size="1.524" layer="95"/>
<attribute name="VALUE" x="113.538" y="-29.083" size="1.778" layer="96"/>
</instance>
<instance part="LIDAR" gate="-2" x="114.3" y="-33.02" smashed="yes">
<attribute name="NAME" x="116.84" y="-33.782" size="1.524" layer="95"/>
</instance>
<instance part="LIDAR" gate="-3" x="114.3" y="-35.56" smashed="yes">
<attribute name="NAME" x="116.84" y="-36.322" size="1.524" layer="95"/>
</instance>
<instance part="LIDAR" gate="-4" x="114.3" y="-38.1" smashed="yes">
<attribute name="NAME" x="116.84" y="-38.862" size="1.524" layer="95"/>
</instance>
<instance part="LIDAR" gate="-5" x="114.3" y="-40.64" smashed="yes">
<attribute name="NAME" x="116.84" y="-41.402" size="1.524" layer="95"/>
</instance>
<instance part="U$2" gate="G$1" x="33.02" y="-38.1" smashed="yes">
<attribute name="NAME" x="20.32" y="-22.86" size="1.778" layer="95"/>
<attribute name="VALUE" x="20.32" y="-25.4" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="A" x="109.22" y="66.04" smashed="yes">
<attribute name="NAME" x="96.52" y="82.28" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="96.52" y="46.8" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U2" gate="A" x="109.22" y="10.16" smashed="yes">
<attribute name="NAME" x="96.52" y="26.4" size="2.0828" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="96.52" y="-9.08" size="2.0828" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="ECHOSOUNDER" gate="1" x="33.02" y="10.16" smashed="yes" rot="R180">
<attribute name="VALUE" x="34.29" y="20.32" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="34.29" y="4.318" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GPS" gate="-1" x="114.3" y="-53.34" smashed="yes">
<attribute name="NAME" x="116.84" y="-54.102" size="1.524" layer="95"/>
<attribute name="VALUE" x="113.538" y="-51.943" size="1.778" layer="96"/>
</instance>
<instance part="GPS" gate="-2" x="114.3" y="-55.88" smashed="yes">
<attribute name="NAME" x="116.84" y="-56.642" size="1.524" layer="95"/>
</instance>
<instance part="GPS" gate="-3" x="114.3" y="-58.42" smashed="yes">
<attribute name="NAME" x="116.84" y="-59.182" size="1.524" layer="95"/>
</instance>
<instance part="GPS" gate="-4" x="114.3" y="-60.96" smashed="yes">
<attribute name="NAME" x="116.84" y="-61.722" size="1.524" layer="95"/>
</instance>
<instance part="GPS" gate="-5" x="114.3" y="-63.5" smashed="yes">
<attribute name="NAME" x="116.84" y="-64.262" size="1.524" layer="95"/>
</instance>
<instance part="IMU" gate="-1" x="114.3" y="-76.2" smashed="yes">
<attribute name="NAME" x="116.84" y="-76.962" size="1.524" layer="95"/>
<attribute name="VALUE" x="113.538" y="-74.803" size="1.778" layer="96"/>
</instance>
<instance part="IMU" gate="-2" x="114.3" y="-78.74" smashed="yes">
<attribute name="NAME" x="116.84" y="-79.502" size="1.524" layer="95"/>
</instance>
<instance part="IMU" gate="-3" x="114.3" y="-81.28" smashed="yes">
<attribute name="NAME" x="116.84" y="-82.042" size="1.524" layer="95"/>
</instance>
<instance part="IMU" gate="-4" x="114.3" y="-83.82" smashed="yes">
<attribute name="NAME" x="116.84" y="-84.582" size="1.524" layer="95"/>
</instance>
<instance part="IMU" gate="-5" x="114.3" y="-86.36" smashed="yes">
<attribute name="NAME" x="116.84" y="-87.122" size="1.524" layer="95"/>
</instance>
<instance part="XBEE" gate="-1" x="114.3" y="-101.6" smashed="yes">
<attribute name="NAME" x="116.84" y="-102.362" size="1.524" layer="95"/>
<attribute name="VALUE" x="113.538" y="-100.203" size="1.778" layer="96"/>
</instance>
<instance part="XBEE" gate="-2" x="114.3" y="-104.14" smashed="yes">
<attribute name="NAME" x="116.84" y="-104.902" size="1.524" layer="95"/>
</instance>
<instance part="XBEE" gate="-3" x="114.3" y="-106.68" smashed="yes">
<attribute name="NAME" x="116.84" y="-107.442" size="1.524" layer="95"/>
</instance>
<instance part="XBEE" gate="-4" x="114.3" y="-109.22" smashed="yes">
<attribute name="NAME" x="116.84" y="-109.982" size="1.524" layer="95"/>
</instance>
<instance part="U$3" gate="G$1" x="60.96" y="76.2" smashed="yes" rot="R90"/>
<instance part="PWM1" gate="A" x="53.34" y="-66.04" smashed="yes">
<attribute name="NAME" x="46.99" y="-60.325" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="-73.66" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="27.94" y="-76.2" smashed="yes">
<attribute name="NAME" x="34.29" y="-76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.29" y="-78.74" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="22.86" y="-68.58" smashed="yes">
<attribute name="NAME" x="19.05" y="-67.0814" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-71.882" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="20.32" y="-86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="-90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="-90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PWM2" gate="A" x="53.34" y="-101.6" smashed="yes">
<attribute name="NAME" x="46.99" y="-95.885" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="-109.22" size="1.778" layer="96"/>
</instance>
<instance part="Q2" gate="G$1" x="27.94" y="-111.76" smashed="yes">
<attribute name="NAME" x="34.29" y="-111.76" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.29" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="22.86" y="-104.14" smashed="yes">
<attribute name="NAME" x="19.05" y="-102.6414" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-107.442" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="20.32" y="-121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="-125.73" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="-125.73" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PWM3" gate="A" x="53.34" y="-134.62" smashed="yes">
<attribute name="NAME" x="46.99" y="-128.905" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="-142.24" size="1.778" layer="96"/>
</instance>
<instance part="Q3" gate="G$1" x="27.94" y="-144.78" smashed="yes">
<attribute name="NAME" x="34.29" y="-144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.29" y="-147.32" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="22.86" y="-137.16" smashed="yes">
<attribute name="NAME" x="19.05" y="-135.6614" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-140.462" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="20.32" y="-154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="-158.75" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="-158.75" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PWM4" gate="A" x="53.34" y="-170.18" smashed="yes">
<attribute name="NAME" x="46.99" y="-164.465" size="1.778" layer="95"/>
<attribute name="VALUE" x="46.99" y="-177.8" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="G$1" x="27.94" y="-180.34" smashed="yes">
<attribute name="NAME" x="34.29" y="-180.34" size="1.778" layer="95"/>
<attribute name="VALUE" x="34.29" y="-182.88" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="22.86" y="-172.72" smashed="yes">
<attribute name="NAME" x="19.05" y="-171.2214" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.05" y="-176.022" size="1.778" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="20.32" y="-190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="18.8214" y="-194.31" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.622" y="-194.31" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="UART2" gate="-1" x="-25.4" y="33.02" smashed="yes">
<attribute name="NAME" x="-22.86" y="32.258" size="1.524" layer="95"/>
<attribute name="VALUE" x="-26.162" y="34.417" size="1.778" layer="96"/>
</instance>
<instance part="UART2" gate="-2" x="-25.4" y="30.48" smashed="yes">
<attribute name="NAME" x="-22.86" y="29.718" size="1.524" layer="95"/>
</instance>
<instance part="UART2" gate="-3" x="-25.4" y="27.94" smashed="yes">
<attribute name="NAME" x="-22.86" y="27.178" size="1.524" layer="95"/>
</instance>
<instance part="UART2" gate="-4" x="-25.4" y="25.4" smashed="yes">
<attribute name="NAME" x="-22.86" y="24.638" size="1.524" layer="95"/>
</instance>
<instance part="UART4" gate="-1" x="-25.4" y="12.7" smashed="yes">
<attribute name="NAME" x="-22.86" y="11.938" size="1.524" layer="95"/>
<attribute name="VALUE" x="-26.162" y="14.097" size="1.778" layer="96"/>
</instance>
<instance part="UART4" gate="-2" x="-25.4" y="10.16" smashed="yes">
<attribute name="NAME" x="-22.86" y="9.398" size="1.524" layer="95"/>
</instance>
<instance part="UART4" gate="-3" x="-25.4" y="7.62" smashed="yes">
<attribute name="NAME" x="-22.86" y="6.858" size="1.524" layer="95"/>
</instance>
<instance part="UART4" gate="-4" x="-25.4" y="5.08" smashed="yes">
<attribute name="NAME" x="-22.86" y="4.318" size="1.524" layer="95"/>
</instance>
<instance part="I2C2" gate="-1" x="-25.4" y="-10.16" smashed="yes">
<attribute name="NAME" x="-22.86" y="-10.922" size="1.524" layer="95"/>
<attribute name="VALUE" x="-26.162" y="-8.763" size="1.778" layer="96"/>
</instance>
<instance part="I2C2" gate="-2" x="-25.4" y="-12.7" smashed="yes">
<attribute name="NAME" x="-22.86" y="-13.462" size="1.524" layer="95"/>
</instance>
<instance part="I2C2" gate="-3" x="-25.4" y="-15.24" smashed="yes">
<attribute name="NAME" x="-22.86" y="-16.002" size="1.524" layer="95"/>
</instance>
<instance part="I2C2" gate="-4" x="-25.4" y="-17.78" smashed="yes">
<attribute name="NAME" x="-22.86" y="-18.542" size="1.524" layer="95"/>
</instance>
<instance part="SPI5" gate="-1" x="-25.4" y="-50.8" smashed="yes">
<attribute name="NAME" x="-22.86" y="-51.562" size="1.524" layer="95"/>
<attribute name="VALUE" x="-26.162" y="-49.403" size="1.778" layer="96"/>
</instance>
<instance part="SPI5" gate="-2" x="-25.4" y="-53.34" smashed="yes">
<attribute name="NAME" x="-22.86" y="-54.102" size="1.524" layer="95"/>
</instance>
<instance part="SPI5" gate="-3" x="-25.4" y="-55.88" smashed="yes">
<attribute name="NAME" x="-22.86" y="-56.642" size="1.524" layer="95"/>
</instance>
<instance part="SPI5" gate="-4" x="-25.4" y="-58.42" smashed="yes">
<attribute name="NAME" x="-22.86" y="-59.182" size="1.524" layer="95"/>
</instance>
<instance part="SPI5" gate="-5" x="-25.4" y="-60.96" smashed="yes">
<attribute name="NAME" x="-22.86" y="-61.722" size="1.524" layer="95"/>
</instance>
<instance part="I2C3" gate="-1" x="-25.4" y="-30.48" smashed="yes">
<attribute name="NAME" x="-22.86" y="-31.242" size="1.524" layer="95"/>
<attribute name="VALUE" x="-26.162" y="-29.083" size="1.778" layer="96"/>
</instance>
<instance part="I2C3" gate="-2" x="-25.4" y="-33.02" smashed="yes">
<attribute name="NAME" x="-22.86" y="-33.782" size="1.524" layer="95"/>
</instance>
<instance part="I2C3" gate="-3" x="-25.4" y="-35.56" smashed="yes">
<attribute name="NAME" x="-22.86" y="-36.322" size="1.524" layer="95"/>
</instance>
<instance part="I2C3" gate="-4" x="-25.4" y="-38.1" smashed="yes">
<attribute name="NAME" x="-22.86" y="-38.862" size="1.524" layer="95"/>
</instance>
<instance part="J2" gate="G$1" x="-30.48" y="68.58" smashed="yes">
<attribute name="NAME" x="-36.068" y="80.01" size="1.778" layer="95"/>
<attribute name="VALUE" x="-35.56" y="54.61" size="1.778" layer="96" align="top-left"/>
</instance>
<instance part="GND1" gate="1" x="-53.34" y="53.34" smashed="yes">
<attribute name="VALUE" x="-55.88" y="50.8" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="OUT+"/>
<wire x1="50.8" y1="-33.02" x2="66.04" y2="-33.02" width="0.1524" layer="91"/>
<label x="63.5" y="-33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-68.58" x2="10.16" y2="-68.58" width="0.1524" layer="91"/>
<label x="10.16" y="-68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-104.14" x2="10.16" y2="-104.14" width="0.1524" layer="91"/>
<label x="10.16" y="-104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-137.16" x2="10.16" y2="-137.16" width="0.1524" layer="91"/>
<label x="10.16" y="-137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-172.72" x2="10.16" y2="-172.72" width="0.1524" layer="91"/>
<label x="10.16" y="-172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="LIDAR" gate="-1" pin="S"/>
<wire x1="111.76" y1="-30.48" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<label x="96.52" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="IN-"/>
<wire x1="15.24" y1="-43.18" x2="0" y2="-43.18" width="0.1524" layer="91"/>
<label x="0" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="OUT-"/>
<wire x1="50.8" y1="-43.18" x2="66.04" y2="-43.18" width="0.1524" layer="91"/>
<label x="60.96" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="GND"/>
<wire x1="127" y1="53.34" x2="142.24" y2="53.34" width="0.1524" layer="91"/>
<label x="137.16" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="GND"/>
<wire x1="127" y1="-2.54" x2="142.24" y2="-2.54" width="0.1524" layer="91"/>
<label x="137.16" y="-2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ECHOSOUNDER" gate="1" pin="1"/>
<wire x1="25.4" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
<label x="12.7" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ECHOSOUNDER" gate="1" pin="2"/>
<wire x1="25.4" y1="12.7" x2="12.7" y2="12.7" width="0.1524" layer="91"/>
<label x="12.7" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPS" gate="-4" pin="S"/>
<wire x1="111.76" y1="-60.96" x2="99.06" y2="-60.96" width="0.1524" layer="91"/>
<label x="99.06" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU" gate="-3" pin="S"/>
<wire x1="111.76" y1="-81.28" x2="99.06" y2="-81.28" width="0.1524" layer="91"/>
<label x="99.06" y="-81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM1" gate="A" pin="1"/>
<wire x1="50.8" y1="-63.5" x2="38.1" y2="-63.5" width="0.1524" layer="91"/>
<label x="38.1" y="-63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM2" gate="A" pin="1"/>
<wire x1="50.8" y1="-99.06" x2="38.1" y2="-99.06" width="0.1524" layer="91"/>
<label x="38.1" y="-99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM3" gate="A" pin="1"/>
<wire x1="50.8" y1="-132.08" x2="38.1" y2="-132.08" width="0.1524" layer="91"/>
<label x="38.1" y="-132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM4" gate="A" pin="1"/>
<wire x1="50.8" y1="-167.64" x2="38.1" y2="-167.64" width="0.1524" layer="91"/>
<label x="38.1" y="-167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART2" gate="-3" pin="S"/>
<wire x1="-27.94" y1="27.94" x2="-40.64" y2="27.94" width="0.1524" layer="91"/>
<label x="-40.64" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART4" gate="-3" pin="S"/>
<wire x1="-27.94" y1="7.62" x2="-40.64" y2="7.62" width="0.1524" layer="91"/>
<label x="-40.64" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="-3" pin="S"/>
<wire x1="-40.64" y1="-15.24" x2="-27.94" y2="-15.24" width="0.1524" layer="91"/>
<label x="-40.64" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C3" gate="-3" pin="S"/>
<wire x1="-40.64" y1="-35.56" x2="-27.94" y2="-35.56" width="0.1524" layer="91"/>
<label x="-40.64" y="-35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI5" gate="-4" pin="S"/>
<wire x1="-40.64" y1="-58.42" x2="-27.94" y2="-58.42" width="0.1524" layer="91"/>
<label x="-40.64" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-53.34" y1="55.88" x2="-53.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="8"/>
<wire x1="-40.64" y1="58.42" x2="-53.34" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LIDAR" gate="-4" pin="S"/>
<wire x1="111.76" y1="-38.1" x2="96.52" y2="-38.1" width="0.1524" layer="91"/>
<label x="96.52" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XBEE" gate="-1" pin="S"/>
<wire x1="111.76" y1="-101.6" x2="99.06" y2="-101.6" width="0.1524" layer="91"/>
<label x="99.06" y="-101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="IN+"/>
<wire x1="15.24" y1="-33.02" x2="0" y2="-33.02" width="0.1524" layer="91"/>
<label x="0" y="-33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="ON"/>
<wire x1="43.18" y1="63.5" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<label x="33.02" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_A" class="0">
<segment>
<pinref part="U1" gate="A" pin="A"/>
<wire x1="91.44" y1="63.5" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
<label x="71.12" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="-40.64" y1="71.12" x2="-50.8" y2="71.12" width="0.1524" layer="91"/>
<label x="-53.34" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_B" class="0">
<segment>
<pinref part="U1" gate="A" pin="B"/>
<wire x1="91.44" y1="58.42" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<label x="71.12" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="-40.64" y1="68.58" x2="-50.8" y2="68.58" width="0.1524" layer="91"/>
<label x="-53.34" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART7_RX" class="0">
<segment>
<pinref part="LIDAR" gate="-2" pin="S"/>
<wire x1="111.76" y1="-33.02" x2="96.52" y2="-33.02" width="0.1524" layer="91"/>
<label x="96.52" y="-33.02" size="1.778" layer="95"/>
<label x="96.52" y="-33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART7_TX" class="0">
<segment>
<pinref part="LIDAR" gate="-3" pin="S"/>
<wire x1="111.76" y1="-35.56" x2="96.52" y2="-35.56" width="0.1524" layer="91"/>
<label x="96.52" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOTOCTL" class="0">
<segment>
<pinref part="LIDAR" gate="-5" pin="S"/>
<wire x1="111.76" y1="-40.64" x2="96.52" y2="-40.64" width="0.1524" layer="91"/>
<label x="96.52" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS485_EN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DE"/>
<wire x1="91.44" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="!RE"/>
<wire x1="81.28" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
<wire x1="91.44" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="73.66" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<junction x="81.28" y="71.12"/>
<label x="66.04" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="DI"/>
<wire x1="91.44" y1="68.58" x2="71.12" y2="68.58" width="0.1524" layer="91"/>
<label x="71.12" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U1" gate="A" pin="VCC"/>
<wire x1="127" y1="78.74" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<label x="137.16" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VCC"/>
<wire x1="127" y1="22.86" x2="142.24" y2="22.86" width="0.1524" layer="91"/>
<label x="137.16" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPS" gate="-5" pin="S"/>
<wire x1="111.76" y1="-63.5" x2="99.06" y2="-63.5" width="0.1524" layer="91"/>
<label x="99.06" y="-63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-81.28" x2="20.32" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="20.32" y1="-78.74" x2="20.32" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-78.74" x2="20.32" y2="-78.74" width="0.1524" layer="91"/>
<junction x="20.32" y="-78.74"/>
<wire x1="20.32" y1="-73.66" x2="10.16" y2="-73.66" width="0.1524" layer="91"/>
<label x="10.16" y="-73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-116.84" x2="20.32" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="20.32" y1="-114.3" x2="20.32" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-114.3" x2="20.32" y2="-114.3" width="0.1524" layer="91"/>
<junction x="20.32" y="-114.3"/>
<wire x1="20.32" y1="-109.22" x2="10.16" y2="-109.22" width="0.1524" layer="91"/>
<label x="10.16" y="-109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-149.86" x2="20.32" y2="-147.32" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="G"/>
<wire x1="20.32" y1="-147.32" x2="20.32" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-147.32" x2="20.32" y2="-147.32" width="0.1524" layer="91"/>
<junction x="20.32" y="-147.32"/>
<wire x1="20.32" y1="-142.24" x2="10.16" y2="-142.24" width="0.1524" layer="91"/>
<label x="10.16" y="-142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="20.32" y1="-185.42" x2="20.32" y2="-182.88" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="G"/>
<wire x1="20.32" y1="-182.88" x2="20.32" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-182.88" x2="20.32" y2="-182.88" width="0.1524" layer="91"/>
<junction x="20.32" y="-182.88"/>
<wire x1="20.32" y1="-177.8" x2="10.16" y2="-177.8" width="0.1524" layer="91"/>
<label x="10.16" y="-177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART2" gate="-4" pin="S"/>
<wire x1="-27.94" y1="25.4" x2="-40.64" y2="25.4" width="0.1524" layer="91"/>
<label x="-40.64" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART4" gate="-4" pin="S"/>
<wire x1="-40.64" y1="5.08" x2="-27.94" y2="5.08" width="0.1524" layer="91"/>
<label x="-40.64" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="-4" pin="S"/>
<wire x1="-40.64" y1="-17.78" x2="-27.94" y2="-17.78" width="0.1524" layer="91"/>
<label x="-40.64" y="-17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C3" gate="-4" pin="S"/>
<wire x1="-40.64" y1="-38.1" x2="-27.94" y2="-38.1" width="0.1524" layer="91"/>
<label x="-40.64" y="-38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI5" gate="-5" pin="S"/>
<wire x1="-40.64" y1="-60.96" x2="-27.94" y2="-60.96" width="0.1524" layer="91"/>
<label x="-40.64" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU" gate="-5" pin="S"/>
<wire x1="111.76" y1="-86.36" x2="99.06" y2="-86.36" width="0.1524" layer="91"/>
<label x="99.06" y="-86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="XBEE" gate="-4" pin="S"/>
<wire x1="111.76" y1="-109.22" x2="99.06" y2="-109.22" width="0.1524" layer="91"/>
<label x="99.06" y="-109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="RO"/>
<wire x1="127" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<label x="132.08" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="ECHOSOUNDER_EN" class="0">
<segment>
<pinref part="U2" gate="A" pin="DE"/>
<wire x1="91.44" y1="15.24" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="!RE"/>
<wire x1="83.82" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<wire x1="91.44" y1="17.78" x2="83.82" y2="17.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="17.78" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<junction x="83.82" y="15.24"/>
<label x="58.42" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART8_TX" class="0">
<segment>
<pinref part="U2" gate="A" pin="DI"/>
<wire x1="91.44" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<label x="73.66" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="ECHOSOUNDER_A" class="0">
<segment>
<pinref part="U2" gate="A" pin="A"/>
<wire x1="91.44" y1="7.62" x2="73.66" y2="7.62" width="0.1524" layer="91"/>
<label x="66.04" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ECHOSOUNDER" gate="1" pin="3"/>
<wire x1="25.4" y1="10.16" x2="12.7" y2="10.16" width="0.1524" layer="91"/>
<label x="2.54" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="ECHOSOUNDER_B" class="0">
<segment>
<pinref part="U2" gate="A" pin="B"/>
<wire x1="91.44" y1="2.54" x2="73.66" y2="2.54" width="0.1524" layer="91"/>
<label x="66.04" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ECHOSOUNDER" gate="1" pin="4"/>
<wire x1="25.4" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<label x="2.54" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART8_RX" class="0">
<segment>
<pinref part="U2" gate="A" pin="RO"/>
<wire x1="127" y1="17.78" x2="142.24" y2="17.78" width="0.1524" layer="91"/>
<label x="129.54" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPS_PPS" class="0">
<segment>
<pinref part="GPS" gate="-1" pin="S"/>
<wire x1="111.76" y1="-53.34" x2="99.06" y2="-53.34" width="0.1524" layer="91"/>
<label x="99.06" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_TX" class="0">
<segment>
<pinref part="GPS" gate="-2" pin="S"/>
<wire x1="111.76" y1="-55.88" x2="99.06" y2="-55.88" width="0.1524" layer="91"/>
<label x="99.06" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_RX" class="0">
<segment>
<pinref part="GPS" gate="-3" pin="S"/>
<wire x1="111.76" y1="-58.42" x2="99.06" y2="-58.42" width="0.1524" layer="91"/>
<label x="99.06" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="IMU" gate="-2" pin="S"/>
<wire x1="111.76" y1="-78.74" x2="99.06" y2="-78.74" width="0.1524" layer="91"/>
<label x="99.06" y="-78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="IMU" gate="-1" pin="S"/>
<wire x1="111.76" y1="-76.2" x2="99.06" y2="-76.2" width="0.1524" layer="91"/>
<label x="99.06" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_RX" class="0">
<segment>
<pinref part="UART4" gate="-1" pin="S"/>
<wire x1="-40.64" y1="12.7" x2="-27.94" y2="12.7" width="0.1524" layer="91"/>
<label x="-40.64" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<pinref part="UART4" gate="-2" pin="S"/>
<wire x1="-27.94" y1="10.16" x2="-40.64" y2="10.16" width="0.1524" layer="91"/>
<label x="-40.64" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="12V_RJ45" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="IN"/>
<wire x1="43.18" y1="68.58" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<label x="30.48" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="76.2" x2="-50.8" y2="76.2" width="0.1524" layer="91"/>
<label x="-53.34" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="OFF"/>
<wire x1="43.18" y1="73.66" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-71.12" x2="30.48" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-68.58" x2="27.94" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="PWM1" gate="A" pin="3"/>
<wire x1="50.8" y1="-68.58" x2="30.48" y2="-68.58" width="0.1524" layer="91"/>
<junction x="30.48" y="-68.58"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="30.48" y1="-81.28" x2="30.48" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-93.98" x2="20.32" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-93.98" x2="10.16" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-91.44" x2="20.32" y2="-93.98" width="0.1524" layer="91"/>
<junction x="20.32" y="-93.98"/>
<label x="10.16" y="-93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-106.68" x2="30.48" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-104.14" x2="27.94" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="PWM2" gate="A" pin="3"/>
<wire x1="50.8" y1="-104.14" x2="30.48" y2="-104.14" width="0.1524" layer="91"/>
<junction x="30.48" y="-104.14"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="D"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-139.7" x2="30.48" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-137.16" x2="27.94" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="PWM3" gate="A" pin="3"/>
<wire x1="50.8" y1="-137.16" x2="30.48" y2="-137.16" width="0.1524" layer="91"/>
<junction x="30.48" y="-137.16"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="D"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="30.48" y1="-175.26" x2="30.48" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-172.72" x2="27.94" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="PWM4" gate="A" pin="3"/>
<wire x1="50.8" y1="-172.72" x2="30.48" y2="-172.72" width="0.1524" layer="91"/>
<junction x="30.48" y="-172.72"/>
</segment>
</net>
<net name="UART2_RX" class="0">
<segment>
<pinref part="UART2" gate="-1" pin="S"/>
<wire x1="-27.94" y1="33.02" x2="-40.64" y2="33.02" width="0.1524" layer="91"/>
<label x="-40.64" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_TX" class="0">
<segment>
<pinref part="UART2" gate="-2" pin="S"/>
<wire x1="-40.64" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<label x="-40.64" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SDA" class="0">
<segment>
<pinref part="I2C2" gate="-1" pin="S"/>
<wire x1="-40.64" y1="-10.16" x2="-27.94" y2="-10.16" width="0.1524" layer="91"/>
<label x="-40.64" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SCL" class="0">
<segment>
<pinref part="I2C2" gate="-2" pin="S"/>
<wire x1="-40.64" y1="-12.7" x2="-27.94" y2="-12.7" width="0.1524" layer="91"/>
<label x="-40.64" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SDA" class="0">
<segment>
<pinref part="I2C3" gate="-1" pin="S"/>
<wire x1="-40.64" y1="-30.48" x2="-27.94" y2="-30.48" width="0.1524" layer="91"/>
<label x="-40.64" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SCL" class="0">
<segment>
<pinref part="I2C3" gate="-2" pin="S"/>
<wire x1="-40.64" y1="-33.02" x2="-27.94" y2="-33.02" width="0.1524" layer="91"/>
<label x="-40.64" y="-33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_MISO" class="0">
<segment>
<pinref part="SPI5" gate="-1" pin="S"/>
<wire x1="-40.64" y1="-50.8" x2="-27.94" y2="-50.8" width="0.1524" layer="91"/>
<label x="-40.64" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_MOSI" class="0">
<segment>
<pinref part="SPI5" gate="-2" pin="S"/>
<wire x1="-40.64" y1="-53.34" x2="-27.94" y2="-53.34" width="0.1524" layer="91"/>
<label x="-40.64" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI5_SCK" class="0">
<segment>
<pinref part="SPI5" gate="-3" pin="S"/>
<wire x1="-40.64" y1="-55.88" x2="-27.94" y2="-55.88" width="0.1524" layer="91"/>
<label x="-40.64" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="30.48" y1="-116.84" x2="30.48" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-129.54" x2="20.32" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-129.54" x2="10.16" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-127" x2="20.32" y2="-129.54" width="0.1524" layer="91"/>
<junction x="20.32" y="-129.54"/>
<label x="10.16" y="-129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="S"/>
<wire x1="30.48" y1="-149.86" x2="30.48" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-162.56" x2="20.32" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-162.56" x2="10.16" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-160.02" x2="20.32" y2="-162.56" width="0.1524" layer="91"/>
<junction x="20.32" y="-162.56"/>
<label x="10.16" y="-162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="S"/>
<wire x1="30.48" y1="-185.42" x2="30.48" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="30.48" y1="-198.12" x2="20.32" y2="-198.12" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-198.12" x2="10.16" y2="-198.12" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-195.58" x2="20.32" y2="-198.12" width="0.1524" layer="91"/>
<junction x="20.32" y="-198.12"/>
<label x="10.16" y="-198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_RX" class="0">
<segment>
<pinref part="XBEE" gate="-3" pin="S"/>
<wire x1="111.76" y1="-106.68" x2="99.06" y2="-106.68" width="0.1524" layer="91"/>
<label x="99.06" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="NC" class="0">
<segment>
<pinref part="IMU" gate="-4" pin="S"/>
<wire x1="111.76" y1="-83.82" x2="99.06" y2="-83.82" width="0.1524" layer="91"/>
<label x="99.06" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_TX" class="0">
<segment>
<pinref part="XBEE" gate="-2" pin="S"/>
<wire x1="111.76" y1="-104.14" x2="99.06" y2="-104.14" width="0.1524" layer="91"/>
<label x="99.06" y="-104.14" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="106,2,43.18,73.66,N$3,,,,,"/>
<approved hash="106,2,111.76,-83.82,NC,,,,,"/>
<approved hash="113,2,51.0371,-64.5439,PWM1,,,,,"/>
<approved hash="113,2,51.0371,-100.104,PWM2,,,,,"/>
<approved hash="113,2,51.0371,-133.124,PWM3,,,,,"/>
<approved hash="113,2,51.0371,-168.684,PWM4,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
