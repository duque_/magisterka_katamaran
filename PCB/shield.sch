<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Discovery_shield">
<packages>
<package name="STM32F407VG">
<description>STM32F4DISCOVERY STM32F407VG Create by Jairo Reyes Email: jairo.314@hotmail.com</description>
<wire x1="-1.93" y1="-1.03" x2="-0.03" y2="-1.03" width="0.127" layer="21"/>
<wire x1="2.57" y1="-1.03" x2="4.47" y2="-1.03" width="0.127" layer="21"/>
<wire x1="2.57" y1="-1.03" x2="2.54" y2="-1.02" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.02" x2="-0.06" y2="-1.02" width="0.127" layer="21"/>
<wire x1="-0.06" y1="-1.02" x2="-0.03" y2="-1.03" width="0.127" layer="21"/>
<pad name="GND5" x="0" y="0" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="GND6" x="2.54" y="0" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD15" x="0" y="2.54" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="NC" x="2.54" y="2.54" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD13" x="0" y="5.08" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD14" x="2.54" y="5.08" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD11" x="0" y="7.62" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD12" x="2.54" y="7.62" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD9" x="0" y="10.16" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD10" x="2.54" y="10.16" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB15" x="0" y="12.7" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD8" x="2.54" y="12.7" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB13" x="0" y="15.24" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB14" x="2.54" y="15.24" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB11" x="0" y="17.78" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB12" x="2.54" y="17.78" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE15" x="0" y="20.32" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB10" x="2.54" y="20.32" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE13" x="0" y="22.86" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE14" x="2.54" y="22.86" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE11" x="0" y="25.4" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE12" x="2.54" y="25.4" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE9" x="0" y="27.94" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE10" x="2.54" y="27.94" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE7" x="0" y="30.48" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE8" x="2.54" y="30.48" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="GND4" x="0" y="33.02" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB2" x="2.54" y="33.02" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB1" x="0" y="35.56" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB0" x="2.54" y="35.56" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC5" x="0" y="38.1" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC4" x="2.54" y="38.1" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA7" x="0" y="40.64" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA6" x="2.54" y="40.64" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA5" x="0" y="43.18" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA4" x="2.54" y="43.18" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA3" x="0" y="45.72" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA2" x="2.54" y="45.72" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA1" x="0" y="48.26" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA0" x="2.54" y="48.26" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC3" x="0" y="50.8" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC2" x="2.54" y="50.8" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC1" x="0" y="53.34" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC0" x="2.54" y="53.34" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="GND3" x="0" y="55.88" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="NRST" x="2.54" y="55.88" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="VDD" x="0" y="58.42" drill="0.8" diameter="1.27" shape="offset" rot="R180" stop="no" thermals="no"/>
<pad name="VDD2" x="2.54" y="58.42" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="GND" x="0" y="60.96" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="GND2" x="2.54" y="60.96" drill="0.8" diameter="1.27" shape="offset"/>
<text x="-1.415" y="63.51" size="1.27" layer="21" font="vector" ratio="10" rot="R270">1</text>
<text x="2.715" y="63.42" size="1.27" layer="21" font="vector" ratio="10" rot="R270">2</text>
<wire x1="4.37" y1="61.98" x2="4.37" y2="-0.98" width="0.127" layer="21"/>
<wire x1="-1.93" y1="-1.03" x2="-1.93" y2="61.98" width="0.127" layer="21"/>
<wire x1="4.37" y1="61.98" x2="-1.93" y2="61.98" width="0.127" layer="21"/>
<wire x1="55.21" y1="-1.03" x2="53.31" y2="-1.03" width="0.127" layer="22"/>
<wire x1="50.71" y1="-1.03" x2="48.81" y2="-1.03" width="0.127" layer="22"/>
<wire x1="50.71" y1="-1.03" x2="50.68" y2="-1.02" width="0.127" layer="22"/>
<wire x1="50.68" y1="-1.02" x2="53.28" y2="-1.02" width="0.127" layer="22"/>
<wire x1="53.28" y1="-1.02" x2="53.31" y2="-1.03" width="0.127" layer="22"/>
<pad name="GND10" x="53.28" y="0" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="GND9" x="50.74" y="0" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC7" x="53.28" y="2.54" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC6" x="50.74" y="2.54" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC9" x="53.28" y="5.08" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC8" x="50.74" y="5.08" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA9" x="53.28" y="7.62" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA8" x="50.74" y="7.62" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA13" x="53.28" y="10.16" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA10" x="50.74" y="10.16" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PA15" x="53.28" y="12.7" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PA14" x="50.74" y="12.7" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC11" x="53.28" y="15.24" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC10" x="50.74" y="15.24" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD0" x="53.28" y="17.78" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC12" x="50.74" y="17.78" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD2" x="53.28" y="20.32" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD1" x="50.74" y="20.32" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD4" x="53.28" y="22.86" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD3" x="50.74" y="22.86" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PD6" x="53.28" y="25.4" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD5" x="50.74" y="25.4" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB3" x="53.28" y="27.94" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PD7" x="50.74" y="27.94" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB5" x="53.28" y="30.48" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB4" x="50.74" y="30.48" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB7" x="53.28" y="33.02" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB6" x="50.74" y="33.02" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="VDD3" x="53.28" y="35.56" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="BOOT" x="50.74" y="35.56" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PB9" x="53.28" y="38.1" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PB8" x="50.74" y="38.1" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE1" x="53.28" y="40.64" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE0" x="50.74" y="40.64" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE3" x="53.28" y="43.18" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE2" x="50.74" y="43.18" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PE5" x="53.28" y="45.72" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE4" x="50.74" y="45.72" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC13" x="53.28" y="48.26" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PE6" x="50.74" y="48.26" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PC15" x="53.28" y="50.8" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PC14" x="50.74" y="50.8" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="PH1" x="53.28" y="53.34" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="PH0" x="50.74" y="53.34" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="3V2" x="53.28" y="55.88" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="3V" x="50.74" y="55.88" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="5V2" x="53.28" y="58.42" drill="0.8" diameter="1.27" shape="offset" stop="no" thermals="no"/>
<pad name="5V" x="50.74" y="58.42" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<pad name="GND8" x="53.28" y="60.96" drill="0.8" diameter="1.27" shape="offset"/>
<pad name="GND7" x="50.74" y="60.96" drill="0.8" diameter="1.27" shape="offset" rot="R180"/>
<text x="53.165" y="63.87" size="1.27" layer="21" font="vector" ratio="10" rot="R270">1</text>
<text x="49.405" y="63.63" size="1.27" layer="21" font="vector" ratio="10" rot="R270">2</text>
<wire x1="48.91" y1="61.98" x2="48.91" y2="-0.98" width="0.127" layer="22"/>
<wire x1="55.21" y1="-1.03" x2="55.21" y2="61.98" width="0.127" layer="22"/>
<wire x1="48.91" y1="61.98" x2="55.21" y2="61.98" width="0.127" layer="22"/>
<wire x1="-6.33" y1="-2.22" x2="59.27" y2="-2.22" width="0.127" layer="25"/>
<wire x1="59.27" y1="-2.22" x2="59.57" y2="-2.22" width="0.127" layer="21"/>
<wire x1="59.27" y1="-2.22" x2="59.27" y2="94.78" width="0.127" layer="25"/>
<wire x1="59.27" y1="94.78" x2="-6.33" y2="94.78" width="0.127" layer="25"/>
<wire x1="-6.33" y1="94.78" x2="-6.33" y2="-2.22" width="0.127" layer="25"/>
<text x="14" y="62" size="1.9304" layer="25">STM32F4 DISCOVERY</text>
</package>
</packages>
<symbols>
<symbol name="STM32F407VG">
<description>STM32F4 DISCOVERY
Create by Jairo Reyes</description>
<wire x1="0" y1="30.48" x2="20.32" y2="30.48" width="0.254" layer="94"/>
<wire x1="20.32" y1="30.48" x2="20.32" y2="-35.56" width="0.254" layer="94"/>
<wire x1="0" y1="-35.56" x2="0" y2="30.48" width="0.254" layer="94"/>
<pin name="GND" x="-5.08" y="27.94" length="middle"/>
<pin name="VDD" x="-5.08" y="25.4" length="middle"/>
<pin name="GND3" x="-5.08" y="22.86" length="middle"/>
<pin name="PC1" x="-5.08" y="20.32" length="middle"/>
<pin name="PC3" x="-5.08" y="17.78" length="middle"/>
<pin name="PA1" x="-5.08" y="15.24" length="middle"/>
<pin name="PA3" x="-5.08" y="12.7" length="middle"/>
<pin name="PA5" x="-5.08" y="10.16" length="middle"/>
<pin name="PA7" x="-5.08" y="7.62" length="middle"/>
<pin name="PC5" x="-5.08" y="5.08" length="middle"/>
<pin name="PB1" x="-5.08" y="2.54" length="middle"/>
<pin name="GND4" x="-5.08" y="0" length="middle"/>
<pin name="PE7" x="-5.08" y="-2.54" length="middle"/>
<pin name="PE9" x="-5.08" y="-5.08" length="middle"/>
<pin name="PE11" x="-5.08" y="-7.62" length="middle"/>
<pin name="PE13" x="-5.08" y="-10.16" length="middle"/>
<pin name="PE15" x="-5.08" y="-12.7" length="middle"/>
<pin name="PB11" x="-5.08" y="-15.24" length="middle"/>
<pin name="PB13" x="-5.08" y="-17.78" length="middle"/>
<pin name="PB15" x="-5.08" y="-20.32" length="middle"/>
<pin name="PD9" x="-5.08" y="-22.86" length="middle"/>
<pin name="PD11" x="-5.08" y="-25.4" length="middle"/>
<pin name="PD13" x="-5.08" y="-27.94" length="middle"/>
<pin name="PD15" x="-5.08" y="-30.48" length="middle"/>
<wire x1="0" y1="-35.56" x2="20.32" y2="-35.56" width="0.254" layer="94"/>
<pin name="GND5" x="-5.08" y="-33.02" length="middle"/>
<pin name="GND2" x="25.4" y="27.94" length="middle" rot="R180"/>
<pin name="VDD2" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="NRST" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="PC0" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PC2" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PA0" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PA2" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PA4" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PA6" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PC4" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="PB0" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PB2" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PE8" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PE12" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PE10" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PE14" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PB10" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PB12" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PB14" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="PD8" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="PD10" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="PD12" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PD14" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="NC" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="GND6" x="25.4" y="-33.02" length="middle" rot="R180"/>
<wire x1="58.42" y1="30.48" x2="78.74" y2="30.48" width="0.254" layer="94"/>
<wire x1="78.74" y1="30.48" x2="78.74" y2="-35.56" width="0.254" layer="94"/>
<wire x1="58.42" y1="-35.56" x2="58.42" y2="30.48" width="0.254" layer="94"/>
<pin name="GND7" x="53.34" y="27.94" length="middle"/>
<pin name="5V" x="53.34" y="25.4" length="middle"/>
<pin name="3V" x="53.34" y="22.86" length="middle"/>
<pin name="PH0" x="53.34" y="20.32" length="middle"/>
<pin name="PC14" x="53.34" y="17.78" length="middle"/>
<pin name="PE6" x="53.34" y="15.24" length="middle"/>
<pin name="PE4" x="53.34" y="12.7" length="middle"/>
<pin name="PE2" x="53.34" y="10.16" length="middle"/>
<pin name="PE0" x="53.34" y="7.62" length="middle"/>
<pin name="PB8" x="53.34" y="5.08" length="middle"/>
<pin name="BOOT" x="53.34" y="2.54" length="middle"/>
<pin name="PB6" x="53.34" y="0" length="middle"/>
<pin name="PB4" x="53.34" y="-2.54" length="middle"/>
<pin name="PD7" x="53.34" y="-5.08" length="middle"/>
<pin name="PD5" x="53.34" y="-7.62" length="middle"/>
<pin name="PD3" x="53.34" y="-10.16" length="middle"/>
<pin name="PD1" x="53.34" y="-12.7" length="middle"/>
<pin name="PC12" x="53.34" y="-15.24" length="middle"/>
<pin name="PC10" x="53.34" y="-17.78" length="middle"/>
<pin name="PA14" x="53.34" y="-20.32" length="middle"/>
<pin name="PA10" x="53.34" y="-22.86" length="middle"/>
<pin name="PA8" x="53.34" y="-25.4" length="middle"/>
<pin name="PC8" x="53.34" y="-27.94" length="middle"/>
<pin name="PC6" x="53.34" y="-30.48" length="middle"/>
<wire x1="58.42" y1="-35.56" x2="78.74" y2="-35.56" width="0.254" layer="94"/>
<pin name="GND9" x="53.34" y="-33.02" length="middle"/>
<pin name="GND8" x="83.82" y="27.94" length="middle" rot="R180"/>
<pin name="5V_1" x="83.82" y="25.4" length="middle" rot="R180"/>
<pin name="3V_1" x="83.82" y="22.86" length="middle" rot="R180"/>
<pin name="PH1" x="83.82" y="20.32" length="middle" rot="R180"/>
<pin name="PC15" x="83.82" y="17.78" length="middle" rot="R180"/>
<pin name="PC13" x="83.82" y="15.24" length="middle" rot="R180"/>
<pin name="PE5" x="83.82" y="12.7" length="middle" rot="R180"/>
<pin name="PE3" x="83.82" y="10.16" length="middle" rot="R180"/>
<pin name="PE1" x="83.82" y="7.62" length="middle" rot="R180"/>
<pin name="PB9" x="83.82" y="5.08" length="middle" rot="R180"/>
<pin name="VDD3" x="83.82" y="2.54" length="middle" rot="R180"/>
<pin name="PB7" x="83.82" y="0" length="middle" rot="R180"/>
<pin name="PB5" x="83.82" y="-2.54" length="middle" rot="R180"/>
<pin name="PD6" x="83.82" y="-7.62" length="middle" rot="R180"/>
<pin name="PB3" x="83.82" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4" x="83.82" y="-10.16" length="middle" rot="R180"/>
<pin name="PD2" x="83.82" y="-12.7" length="middle" rot="R180"/>
<pin name="PD0" x="83.82" y="-15.24" length="middle" rot="R180"/>
<pin name="PC11" x="83.82" y="-17.78" length="middle" rot="R180"/>
<pin name="PA15" x="83.82" y="-20.32" length="middle" rot="R180"/>
<pin name="PA13" x="83.82" y="-22.86" length="middle" rot="R180"/>
<pin name="PA9" x="83.82" y="-25.4" length="middle" rot="R180"/>
<pin name="PC9" x="83.82" y="-27.94" length="middle" rot="R180"/>
<pin name="PC7" x="83.82" y="-30.48" length="middle" rot="R180"/>
<pin name="GND10" x="83.82" y="-33.02" length="middle" rot="R180"/>
<text x="73.66" y="-38.1" size="1.27" layer="95">RIGHT</text>
<text x="15.24" y="-38.1" size="1.27" layer="95">LEFT</text>
<text x="2.54" y="33.02" size="1.27" layer="94">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F4-DISCOVERY" prefix="BD" uservalue="yes">
<description>STM32F4DISCOVERY		

STM32F407VG

Create by Jairo Reyes
Email: jairo.314@hotmail.com</description>
<gates>
<gate name="G$1" symbol="STM32F407VG" x="-40.64" y="0"/>
</gates>
<devices>
<device name="" package="STM32F407VG">
<connects>
<connect gate="G$1" pin="3V" pad="3V"/>
<connect gate="G$1" pin="3V_1" pad="3V2"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="5V_1" pad="5V2"/>
<connect gate="G$1" pin="BOOT" pad="BOOT"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND10" pad="GND10"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="GND3" pad="GND3"/>
<connect gate="G$1" pin="GND4" pad="GND4"/>
<connect gate="G$1" pin="GND5" pad="GND5"/>
<connect gate="G$1" pin="GND6" pad="GND6"/>
<connect gate="G$1" pin="GND7" pad="GND7"/>
<connect gate="G$1" pin="GND8" pad="GND8"/>
<connect gate="G$1" pin="GND9" pad="GND9"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NRST" pad="NRST"/>
<connect gate="G$1" pin="PA0" pad="PA0"/>
<connect gate="G$1" pin="PA1" pad="PA1"/>
<connect gate="G$1" pin="PA10" pad="PA10"/>
<connect gate="G$1" pin="PA13" pad="PA13"/>
<connect gate="G$1" pin="PA14" pad="PA14"/>
<connect gate="G$1" pin="PA15" pad="PA15"/>
<connect gate="G$1" pin="PA2" pad="PA2"/>
<connect gate="G$1" pin="PA3" pad="PA3"/>
<connect gate="G$1" pin="PA4" pad="PA4"/>
<connect gate="G$1" pin="PA5" pad="PA5"/>
<connect gate="G$1" pin="PA6" pad="PA6"/>
<connect gate="G$1" pin="PA7" pad="PA7"/>
<connect gate="G$1" pin="PA8" pad="PA8"/>
<connect gate="G$1" pin="PA9" pad="PA9"/>
<connect gate="G$1" pin="PB0" pad="PB0"/>
<connect gate="G$1" pin="PB1" pad="PB1"/>
<connect gate="G$1" pin="PB10" pad="PB10"/>
<connect gate="G$1" pin="PB11" pad="PB11"/>
<connect gate="G$1" pin="PB12" pad="PB12"/>
<connect gate="G$1" pin="PB13" pad="PB13"/>
<connect gate="G$1" pin="PB14" pad="PB14"/>
<connect gate="G$1" pin="PB15" pad="PB15"/>
<connect gate="G$1" pin="PB2" pad="PB2"/>
<connect gate="G$1" pin="PB3" pad="PB3"/>
<connect gate="G$1" pin="PB4" pad="PB4"/>
<connect gate="G$1" pin="PB5" pad="PB5"/>
<connect gate="G$1" pin="PB6" pad="PB6"/>
<connect gate="G$1" pin="PB7" pad="PB7"/>
<connect gate="G$1" pin="PB8" pad="PB8"/>
<connect gate="G$1" pin="PB9" pad="PB9"/>
<connect gate="G$1" pin="PC0" pad="PC0"/>
<connect gate="G$1" pin="PC1" pad="PC1"/>
<connect gate="G$1" pin="PC10" pad="PC10"/>
<connect gate="G$1" pin="PC11" pad="PC11"/>
<connect gate="G$1" pin="PC12" pad="PC12"/>
<connect gate="G$1" pin="PC13" pad="PC13"/>
<connect gate="G$1" pin="PC14" pad="PC14"/>
<connect gate="G$1" pin="PC15" pad="PC15"/>
<connect gate="G$1" pin="PC2" pad="PC2"/>
<connect gate="G$1" pin="PC3" pad="PC3"/>
<connect gate="G$1" pin="PC4" pad="PC4"/>
<connect gate="G$1" pin="PC5" pad="PC5"/>
<connect gate="G$1" pin="PC6" pad="PC6"/>
<connect gate="G$1" pin="PC7" pad="PC7"/>
<connect gate="G$1" pin="PC8" pad="PC8"/>
<connect gate="G$1" pin="PC9" pad="PC9"/>
<connect gate="G$1" pin="PD0" pad="PD0"/>
<connect gate="G$1" pin="PD1" pad="PD1"/>
<connect gate="G$1" pin="PD10" pad="PD10"/>
<connect gate="G$1" pin="PD11" pad="PD11"/>
<connect gate="G$1" pin="PD12" pad="PD12"/>
<connect gate="G$1" pin="PD13" pad="PD13"/>
<connect gate="G$1" pin="PD14" pad="PD14"/>
<connect gate="G$1" pin="PD15" pad="PD15"/>
<connect gate="G$1" pin="PD2" pad="PD2"/>
<connect gate="G$1" pin="PD3" pad="PD3"/>
<connect gate="G$1" pin="PD4" pad="PD4"/>
<connect gate="G$1" pin="PD5" pad="PD5"/>
<connect gate="G$1" pin="PD6" pad="PD6"/>
<connect gate="G$1" pin="PD7" pad="PD7"/>
<connect gate="G$1" pin="PD8" pad="PD8"/>
<connect gate="G$1" pin="PD9" pad="PD9"/>
<connect gate="G$1" pin="PE0" pad="PE0"/>
<connect gate="G$1" pin="PE1" pad="PE1"/>
<connect gate="G$1" pin="PE10" pad="PE10"/>
<connect gate="G$1" pin="PE11" pad="PE11"/>
<connect gate="G$1" pin="PE12" pad="PE12"/>
<connect gate="G$1" pin="PE13" pad="PE13"/>
<connect gate="G$1" pin="PE14" pad="PE14"/>
<connect gate="G$1" pin="PE15" pad="PE15"/>
<connect gate="G$1" pin="PE2" pad="PE2"/>
<connect gate="G$1" pin="PE3" pad="PE3"/>
<connect gate="G$1" pin="PE4" pad="PE4"/>
<connect gate="G$1" pin="PE5" pad="PE5"/>
<connect gate="G$1" pin="PE6" pad="PE6"/>
<connect gate="G$1" pin="PE7" pad="PE7"/>
<connect gate="G$1" pin="PE8" pad="PE8"/>
<connect gate="G$1" pin="PE9" pad="PE9"/>
<connect gate="G$1" pin="PH0" pad="PH0"/>
<connect gate="G$1" pin="PH1" pad="PH1"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
<connect gate="G$1" pin="VDD2" pad="VDD2"/>
<connect gate="G$1" pin="VDD3" pad="VDD3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Rembrandt Electronics - JST XH Connectors v1-0">
<packages>
<package name="JST-XH-04-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;
Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="6.2" y1="-2.3575" x2="6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="6.2" y1="3.3925" x2="-6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-6.2" y1="3.3925" x2="-6.2" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-2.3575" x2="6.2" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.655" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.0025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.4675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="-1.8" width="0.254" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-04-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="6.2" y1="-2.3575" x2="6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="6.2" y1="3.3925" x2="-6.2" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-6.2" y1="3.3925" x2="-6.2" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-2.3575" x2="6.2" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-3.81" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" rot="R90"/>
<text x="-6.655" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.0025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.4675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-3.8" y1="-2.3" x2="-3.8" y2="-1.8" width="0.254" layer="21"/>
<wire x1="3.9" y1="-2.3" x2="3.9" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-05-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="7.45" y1="-2.3575" x2="7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="7.45" y1="3.3925" x2="-7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-7.45" y1="3.3925" x2="-7.45" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-7.45" y1="-2.3575" x2="7.45" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-8.055" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.2025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.6675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-5.1" y1="-2.3" x2="-5.1" y2="-1.8" width="0.254" layer="21"/>
<wire x1="5.1" y1="-2.3" x2="5.1" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-05-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="7.45" y1="-2.3575" x2="7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="7.45" y1="3.3925" x2="-7.45" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-7.45" y1="3.3925" x2="-7.45" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-7.45" y1="-2.3575" x2="7.45" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" rot="R90"/>
<text x="-8.055" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.2025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.6675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-5.1" y1="-2.3" x2="-5.1" y2="-1.8" width="0.254" layer="21"/>
<wire x1="5.1" y1="-2.3" x2="5.1" y2="-1.8" width="0.254" layer="21"/>
</package>
<package name="JST-XH-02-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;2&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="3.7" y1="-2.3575" x2="3.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="3.7" y1="3.3925" x2="-3.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-3.7" y1="3.3925" x2="-3.7" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-3.7" y1="-2.3575" x2="3.7" y2="-2.3575" width="0.254" layer="21"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-4.255" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.3025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.8675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-1.3" y1="-2.3" x2="-1.3" y2="-1.8" width="0.2" layer="21"/>
<wire x1="1.3" y1="-2.3" x2="1.3" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-1.3" y1="-1.8" x2="1.3" y2="-1.8" width="0.2" layer="21"/>
</package>
<package name="JST-XH-02-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;2&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="3.7" y1="-2.3575" x2="3.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="3.7" y1="3.3925" x2="-3.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-3.7" y1="3.3925" x2="-3.7" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-3.7" y1="-2.3575" x2="3.7" y2="-2.3575" width="0.254" layer="21"/>
<pad name="2" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-1.27" y="0" drill="1.016" rot="R90"/>
<text x="-4.255" y="-2.04" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.3025" y="3.8925" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.8675" y="-1.4875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-1.3" y1="-2.3" x2="-1.3" y2="-1.8" width="0.2" layer="21"/>
<wire x1="1.3" y1="-2.3" x2="1.3" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-1.3" y1="-1.8" x2="1.3" y2="-1.8" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST-XH-04-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;4&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-04-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-04-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-XH-05-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;5&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="2.54" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-05-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-05-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-XH-02-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;2&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-2" symbol="M" x="2.54" y="15.24" addlevel="always" swaplevel="1"/>
<gate name="-1" symbol="MV" x="2.54" y="17.78" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-02-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-02-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="diy-modules">
<description>&lt;b&gt;DIY Modules for Arduino, Raspberry Pi, CubieBoard etc.&lt;/b&gt;
&lt;br&gt;&lt;br&gt;
The library contains a list of symbols and footprints for popular, cheap and easy-to-use electronic modules.&lt;br&gt;
The modules are intend to work with microprocessor-based platforms such as &lt;a href="http://arduino.cc"&gt;Arduino&lt;/a&gt;, &lt;a href="http://raspberrypi.org/"&gt;Raspberry Pi&lt;/a&gt;, &lt;a href="http://cubieboard.org/"&gt;CubieBoard&lt;/a&gt;, &lt;a href="http://beagleboard.org/"&gt;BeagleBone&lt;/a&gt; and many others. There are many manufacturers of the modules in the world. Almost all of them can be bought on &lt;a href="ebay.com"&gt;ebay.com&lt;/a&gt;.&lt;br&gt;
&lt;br&gt;
By using this library, you can design a PCB for devices created with usage of modules. Even if you do not need to create PCB design, you can also use the library to quickly document your work by drawing schematics of devices built by you.&lt;br&gt;
&lt;br&gt;
The latest version, examples, photos and much more can be found at: &lt;b&gt;&lt;a href="http://diymodules.org/eagle"&gt;diymodules.org/eagle&lt;/a&gt;&lt;/b&gt;&lt;br&gt;&lt;br&gt;
Comments, suggestions and bug reports please send to: &lt;b&gt;&lt;a href="mailto:eagle@diymodules.org"&gt;eagle@diymodules.org&lt;/b&gt;&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Version: 1.8.0 (2017-Jul-02)&lt;/i&gt;&lt;br&gt;
&lt;i&gt;Created by: Miroslaw Brudnowski&lt;/i&gt;&lt;br&gt;&lt;br&gt;
&lt;i&gt;Released under the Creative Commons Attribution 4.0 International License: &lt;a href="http://creativecommons.org/licenses/by/4.0"&gt;http://creativecommons.org/licenses/by/4.0&lt;/a&gt;&lt;/i&gt;
&lt;br&gt;&lt;br&gt;
&lt;center&gt;
&lt;a href="http://diymodules.org/eagle"&gt;&lt;img src="http://www.diymodules.org/img/diymodules-lbr-image.php?v=1.8.0" alt="DIYmodules.org"&gt;&lt;/a&gt;
&lt;/center&gt;</description>
<packages>
<package name="LEVEL-SHIFTER-4CH">
<description>&lt;b&gt;4-Channel Bi-Directional  Logic Level Converter&lt;/b&gt; based on &lt;b&gt;BSS128&lt;/b&gt; transistors</description>
<wire x1="-5.715" y1="7.62" x2="-4.445" y2="7.62" width="0.127" layer="21"/>
<wire x1="4.445" y1="7.62" x2="5.715" y2="7.62" width="0.127" layer="21"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="5.715" y1="-7.62" x2="4.445" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-7.62" x2="-5.715" y2="-7.62" width="0.127" layer="21"/>
<pad name="J2.1" x="5.08" y="6.35" drill="1" shape="square"/>
<pad name="J2.2" x="5.08" y="3.81" drill="1"/>
<pad name="J2.3" x="5.08" y="1.27" drill="1"/>
<text x="0" y="8.89" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.89" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="J2.4" x="5.08" y="-1.27" drill="1"/>
<pad name="J2.5" x="5.08" y="-3.81" drill="1"/>
<text x="3.175" y="6.35" size="1.016" layer="21" align="center-right">HV1</text>
<text x="3.175" y="3.81" size="1.016" layer="21" align="center-right">HV2</text>
<text x="3.175" y="1.27" size="1.27" layer="21" align="center-right">HV</text>
<pad name="J2.6" x="5.08" y="-6.35" drill="1"/>
<text x="0" y="-1.27" size="1.27" layer="21" align="center">GND</text>
<text x="3.175" y="-3.81" size="1.016" layer="21" align="center-right">HV3</text>
<text x="3.175" y="-6.35" size="1.016" layer="21" align="center-right">HV4</text>
<pad name="J1.1" x="-5.08" y="6.35" drill="1" shape="square"/>
<pad name="J1.2" x="-5.08" y="3.81" drill="1"/>
<pad name="J1.3" x="-5.08" y="1.27" drill="1"/>
<pad name="J1.4" x="-5.08" y="-1.27" drill="1"/>
<pad name="J1.5" x="-5.08" y="-3.81" drill="1"/>
<pad name="J1.6" x="-5.08" y="-6.35" drill="1"/>
<wire x1="6.35" y1="6.985" x2="5.715" y2="7.62" width="0.127" layer="21"/>
<wire x1="4.445" y1="7.62" x2="3.81" y2="6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="5.715" width="0.127" layer="21"/>
<wire x1="3.81" y1="5.715" x2="4.445" y2="5.08" width="0.127" layer="21"/>
<wire x1="4.445" y1="5.08" x2="3.81" y2="4.445" width="0.127" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.445" y1="2.54" x2="3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="0" width="0.127" layer="21"/>
<wire x1="4.445" y1="0" x2="3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.127" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.81" y2="-4.445" width="0.127" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="4.445" y2="-5.08" width="0.127" layer="21"/>
<wire x1="4.445" y1="-5.08" x2="3.81" y2="-5.715" width="0.127" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="4.445" y2="-7.62" width="0.127" layer="21"/>
<wire x1="5.715" y1="-7.62" x2="6.35" y2="-6.985" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.715" x2="5.715" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.715" y1="5.08" x2="6.35" y2="4.445" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.175" x2="5.715" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="6.35" y1="0.635" x2="5.715" y2="0" width="0.127" layer="21"/>
<wire x1="5.715" y1="0" x2="6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="-4.445" x2="5.715" y2="-5.08" width="0.127" layer="21"/>
<wire x1="5.715" y1="-5.08" x2="6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-4.445" y1="7.62" x2="-3.81" y2="6.985" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="5.715" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.715" x2="-4.445" y2="5.08" width="0.127" layer="21"/>
<wire x1="-4.445" y1="5.08" x2="-3.81" y2="4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.175" x2="-4.445" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-4.445" y2="0" width="0.127" layer="21"/>
<wire x1="-4.445" y1="0" x2="-3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-4.445" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-5.08" x2="-3.81" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-5.715" x2="-3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-6.985" x2="-4.445" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-7.62" x2="-6.35" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-5.715" y2="7.62" width="0.127" layer="21"/>
<wire x1="-6.35" y1="5.715" x2="-5.715" y2="5.08" width="0.127" layer="21"/>
<wire x1="-5.715" y1="5.08" x2="-6.35" y2="4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="0" width="0.127" layer="21"/>
<wire x1="-5.715" y1="0" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-5.715" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.08" x2="-6.35" y2="-5.715" width="0.127" layer="21"/>
<text x="-3.175" y="6.35" size="1.016" layer="21" align="center-left">LV1</text>
<text x="-3.175" y="3.81" size="1.016" layer="21" align="center-left">LV2</text>
<text x="-3.175" y="1.27" size="1.27" layer="21" align="center-left">LV</text>
<text x="-3.175" y="-3.81" size="1.016" layer="21" align="center-left">LV3</text>
<text x="-3.175" y="-6.35" size="1.016" layer="21" align="center-left">LV4</text>
<wire x1="2.159" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.604" y1="7.874" x2="6.604" y2="7.874" width="0.127" layer="21"/>
<wire x1="6.604" y1="7.874" x2="6.604" y2="-7.874" width="0.127" layer="21"/>
<wire x1="6.604" y1="-7.874" x2="-6.604" y2="-7.874" width="0.127" layer="21"/>
<wire x1="-6.604" y1="-7.874" x2="-6.604" y2="7.874" width="0.127" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="4.445" x2="-6.35" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="0.635" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.35" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="-6.985" width="0.127" layer="21"/>
<wire x1="6.35" y1="-6.985" x2="6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.715" x2="6.35" y2="6.985" width="0.127" layer="21"/>
</package>
<package name="RS485-TTL-CONVERTER">
<description>&lt;b&gt;RS485 to TTL converter&lt;/b&gt; based on &lt;b&gt;MAX485&lt;/b&gt; chip</description>
<wire x1="-21.971" y1="7.112" x2="21.971" y2="7.112" width="0.127" layer="21"/>
<wire x1="21.971" y1="7.112" x2="21.971" y2="-7.112" width="0.127" layer="21"/>
<wire x1="21.971" y1="-7.112" x2="-21.971" y2="-7.112" width="0.127" layer="21"/>
<wire x1="-21.971" y1="-7.112" x2="-21.971" y2="7.112" width="0.127" layer="21"/>
<wire x1="15.875" y1="-5.08" x2="9.525" y2="-5.08" width="0.127" layer="21"/>
<wire x1="9.525" y1="-5.08" x2="8.255" y2="-5.08" width="0.127" layer="21"/>
<wire x1="8.255" y1="-5.08" x2="8.255" y2="0" width="0.127" layer="21"/>
<wire x1="8.255" y1="0" x2="8.255" y2="5.08" width="0.127" layer="21"/>
<wire x1="8.255" y1="5.08" x2="9.525" y2="5.08" width="0.127" layer="21"/>
<wire x1="9.525" y1="5.08" x2="15.875" y2="5.08" width="0.127" layer="21"/>
<wire x1="15.875" y1="5.08" x2="15.875" y2="0" width="0.127" layer="21"/>
<wire x1="15.875" y1="0" x2="15.875" y2="-5.08" width="0.127" layer="21"/>
<wire x1="8.255" y1="0" x2="15.875" y2="0" width="0.127" layer="21"/>
<circle x="12.7" y="-2.54" radius="1.27" width="0.127" layer="21"/>
<circle x="12.7" y="2.54" radius="1.27" width="0.127" layer="21"/>
<circle x="8.89" y="-2.54" radius="0.381" width="0.127" layer="21"/>
<circle x="8.89" y="2.54" radius="0.381" width="0.127" layer="21"/>
<wire x1="9.525" y1="-5.08" x2="9.525" y2="5.08" width="0.127" layer="21"/>
<wire x1="12.065" y1="-3.175" x2="13.335" y2="-1.905" width="0.127" layer="21"/>
<wire x1="12.065" y1="1.905" x2="13.335" y2="3.175" width="0.127" layer="21"/>
<text x="0" y="8.255" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.255" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="J1.1" x="-20.32" y="-3.81" drill="1" shape="square"/>
<pad name="J1.2" x="-20.32" y="-1.27" drill="1"/>
<pad name="J1.3" x="-20.32" y="1.27" drill="1"/>
<pad name="J1.4" x="-20.32" y="3.81" drill="1"/>
<wire x1="-21.59" y1="4.445" x2="-20.955" y2="5.08" width="0.127" layer="21"/>
<wire x1="-20.955" y1="5.08" x2="-19.685" y2="5.08" width="0.127" layer="21"/>
<wire x1="-19.685" y1="5.08" x2="-19.05" y2="4.445" width="0.127" layer="21"/>
<wire x1="-19.05" y1="4.445" x2="-19.05" y2="3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="3.175" x2="-19.685" y2="2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-19.05" y2="1.905" width="0.127" layer="21"/>
<wire x1="-19.05" y1="1.905" x2="-19.05" y2="0.635" width="0.127" layer="21"/>
<wire x1="-19.05" y1="0.635" x2="-19.685" y2="0" width="0.127" layer="21"/>
<wire x1="-19.685" y1="0" x2="-19.05" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-0.635" x2="-19.05" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-1.905" x2="-19.685" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-19.05" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-3.175" x2="-19.05" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-4.445" x2="-19.685" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-19.685" y1="-5.08" x2="-20.955" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-20.955" y1="-5.08" x2="-21.59" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-4.445" x2="-21.59" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-3.175" x2="-20.955" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-21.59" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-1.905" x2="-21.59" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-0.635" x2="-20.955" y2="0" width="0.127" layer="21"/>
<wire x1="-20.955" y1="0" x2="-21.59" y2="0.635" width="0.127" layer="21"/>
<wire x1="-21.59" y1="0.635" x2="-21.59" y2="1.905" width="0.127" layer="21"/>
<wire x1="-21.59" y1="1.905" x2="-20.955" y2="2.54" width="0.127" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-21.59" y2="3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="3.175" x2="-21.59" y2="4.445" width="0.127" layer="21"/>
<pad name="J2.1" x="20.32" y="3.81" drill="1" shape="square" rot="R180"/>
<pad name="J2.2" x="20.32" y="1.27" drill="1" rot="R180"/>
<pad name="J2.3" x="20.32" y="-1.27" drill="1" rot="R180"/>
<pad name="J2.4" x="20.32" y="-3.81" drill="1" rot="R180"/>
<wire x1="21.59" y1="-4.445" x2="20.955" y2="-5.08" width="0.127" layer="21"/>
<wire x1="20.955" y1="-5.08" x2="19.685" y2="-5.08" width="0.127" layer="21"/>
<wire x1="19.685" y1="-5.08" x2="19.05" y2="-4.445" width="0.127" layer="21"/>
<wire x1="19.05" y1="-4.445" x2="19.05" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.05" y1="-3.175" x2="19.685" y2="-2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="19.05" y2="-1.905" width="0.127" layer="21"/>
<wire x1="19.05" y1="-1.905" x2="19.05" y2="-0.635" width="0.127" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="0" width="0.127" layer="21"/>
<wire x1="19.685" y1="0" x2="19.05" y2="0.635" width="0.127" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="1.905" width="0.127" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.685" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="2.54" x2="19.05" y2="3.175" width="0.127" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.05" y2="4.445" width="0.127" layer="21"/>
<wire x1="19.05" y1="4.445" x2="19.685" y2="5.08" width="0.127" layer="21"/>
<wire x1="19.685" y1="5.08" x2="20.955" y2="5.08" width="0.127" layer="21"/>
<wire x1="20.955" y1="5.08" x2="21.59" y2="4.445" width="0.127" layer="21"/>
<wire x1="21.59" y1="4.445" x2="21.59" y2="3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="3.175" x2="20.955" y2="2.54" width="0.127" layer="21"/>
<wire x1="20.955" y1="2.54" x2="21.59" y2="1.905" width="0.127" layer="21"/>
<wire x1="21.59" y1="1.905" x2="21.59" y2="0.635" width="0.127" layer="21"/>
<wire x1="21.59" y1="0.635" x2="20.955" y2="0" width="0.127" layer="21"/>
<wire x1="20.955" y1="0" x2="21.59" y2="-0.635" width="0.127" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="21.59" y2="-1.905" width="0.127" layer="21"/>
<wire x1="21.59" y1="-1.905" x2="20.955" y2="-2.54" width="0.127" layer="21"/>
<wire x1="20.955" y1="-2.54" x2="21.59" y2="-3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="-3.175" x2="21.59" y2="-4.445" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LEVEL-SHIFTER-4CH">
<description>&lt;b&gt;4-Channel Bi-Directional  Logic Level Converter&lt;/b&gt; based on &lt;b&gt;BSS128&lt;/b&gt; transistors</description>
<pin name="LV3" x="-15.24" y="-5.08" length="middle"/>
<pin name="GND.1" x="-15.24" y="-2.54" length="middle" direction="pwr"/>
<pin name="LV" x="-15.24" y="0" length="middle" direction="pwr"/>
<pin name="LV2" x="-15.24" y="2.54" length="middle"/>
<pin name="LV1" x="-15.24" y="5.08" length="middle"/>
<pin name="HV4" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="HV3" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="GND.2" x="15.24" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="HV" x="15.24" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="HV2" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="HV1" x="15.24" y="5.08" length="middle" rot="R180"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="LV4" x="-15.24" y="-7.62" length="middle"/>
</symbol>
<symbol name="RS485-TTL-CONVERTER">
<description>&lt;b&gt;RS485 to TTL converter&lt;/b&gt; based on &lt;b&gt;MAX485&lt;/b&gt; chip</description>
<wire x1="-17.78" y1="7.62" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<pin name="B" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="A" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="22.86" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<wire x1="-17.78" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="8.89" y2="-6.35" width="0.254" layer="94"/>
<wire x1="8.89" y1="-6.35" x2="8.89" y2="-1.27" width="0.254" layer="94"/>
<wire x1="8.89" y1="-1.27" x2="8.89" y2="3.81" width="0.254" layer="94"/>
<wire x1="8.89" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="8.89" y2="-1.27" width="0.254" layer="94"/>
<circle x="5.08" y="1.27" radius="1.27" width="0.254" layer="94"/>
<circle x="5.08" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-17.78" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="22.86" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="RO" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="RE" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="DE" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="DI" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LEVEL-SHIFTER-4CH">
<description>&lt;b&gt;4-Channel Bi-Directional  Logic Level Converter&lt;/b&gt; based on &lt;b&gt;BSS128&lt;/b&gt; transistors
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/i2c+logic+converter+module"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=LEVEL-SHIFTER-4CH"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LEVEL-SHIFTER-4CH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LEVEL-SHIFTER-4CH">
<connects>
<connect gate="G$1" pin="GND.1" pad="J1.4"/>
<connect gate="G$1" pin="GND.2" pad="J2.4"/>
<connect gate="G$1" pin="HV" pad="J2.3"/>
<connect gate="G$1" pin="HV1" pad="J2.1"/>
<connect gate="G$1" pin="HV2" pad="J2.2"/>
<connect gate="G$1" pin="HV3" pad="J2.5"/>
<connect gate="G$1" pin="HV4" pad="J2.6"/>
<connect gate="G$1" pin="LV" pad="J1.3"/>
<connect gate="G$1" pin="LV1" pad="J1.1"/>
<connect gate="G$1" pin="LV2" pad="J1.2"/>
<connect gate="G$1" pin="LV3" pad="J1.5"/>
<connect gate="G$1" pin="LV4" pad="J1.6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RS485-TTL-CONVERTER">
<description>&lt;b&gt;RS485 to TTL converter&lt;/b&gt; based on &lt;b&gt;MAX485&lt;/b&gt; chip
&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=RS485-TTL-CONVERTER"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RS485-TTL-CONVERTER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RS485-TTL-CONVERTER">
<connects>
<connect gate="G$1" pin="A" pad="J2.3"/>
<connect gate="G$1" pin="B" pad="J2.2"/>
<connect gate="G$1" pin="DE" pad="J1.2"/>
<connect gate="G$1" pin="DI" pad="J1.1"/>
<connect gate="G$1" pin="GND" pad="J2.4"/>
<connect gate="G$1" pin="RE" pad="J1.3"/>
<connect gate="G$1" pin="RO" pad="J1.4"/>
<connect gate="G$1" pin="VCC" pad="J2.1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-1" urn="urn:adsk.eagle:footprint:8285/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.334" y="-0.635" size="1.27" layer="21" ratio="10">4</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA04-1" urn="urn:adsk.eagle:package:8337/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA04-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA04-1" urn="urn:adsk.eagle:symbol:8284/1" library_version="1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-1" urn="urn:adsk.eagle:component:8375/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8337/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-kycon" urn="urn:adsk.eagle:library:157">
<description>&lt;b&gt;Connector from KYCON, Inc&lt;/b&gt;&lt;p&gt;
1810 Little Orchard Street,&lt;br&gt;
San Jose,&lt;br&gt;
CA 95125 (408)494-0330&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/autor&gt;</description>
<packages>
<package name="GLX-S-88M" urn="urn:adsk.eagle:footprint:7673/1" library_version="1">
<description>&lt;b&gt;Mod. Jack, Right Angle, 8 posiotion, 8 contatcs&lt;/b&gt; RJ45&lt;p&gt;
Source: GLX-S-88M.pdf</description>
<wire x1="-8.532" y1="4.52" x2="8.532" y2="4.52" width="0" layer="20"/>
<wire x1="8.25" y1="6.525" x2="8.25" y2="-3.302" width="0.2032" layer="22"/>
<wire x1="8.25" y1="-6.35" x2="8.25" y2="-7.875" width="0.2032" layer="22"/>
<wire x1="8.25" y1="-7.875" x2="-8.25" y2="-7.875" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-7.875" x2="-8.25" y2="-6.35" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-3.302" x2="-8.25" y2="6.525" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="6.525" x2="8.25" y2="6.525" width="0.2032" layer="22"/>
<wire x1="-8.25" y1="-6.351" x2="-8.25" y2="-3.381" width="0.2032" layer="51"/>
<wire x1="8.25" y1="-3.381" x2="8.25" y2="-6.351" width="0.2032" layer="51"/>
<pad name="1" x="-3.57" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="2" x="-2.55" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="3" x="-1.53" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="4" x="-0.51" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="5" x="0.51" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="6" x="1.53" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="7" x="2.55" y="-2.3" drill="0.9" diameter="1.4224"/>
<pad name="8" x="3.57" y="-4.84" drill="0.9" diameter="1.4224"/>
<pad name="S1" x="-8.125" y="-4.84" drill="1.6" diameter="2.1844"/>
<pad name="S2" x="8.125" y="-4.84" drill="1.6" diameter="2.1844"/>
<text x="-8.128" y="-9.652" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-7.44" y="0" drill="2.55"/>
<hole x="7.44" y="0" drill="2.55"/>
</package>
</packages>
<packages3d>
<package3d name="GLX-S-88M" urn="urn:adsk.eagle:package:7680/1" type="box" library_version="1">
<description>Mod. Jack, Right Angle, 8 posiotion, 8 contatcs RJ45
Source: GLX-S-88M.pdf</description>
<packageinstances>
<packageinstance name="GLX-S-88M"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="JACK8SH" urn="urn:adsk.eagle:symbol:7672/1" library_version="1">
<wire x1="1.524" y1="10.668" x2="0" y2="10.668" width="0.254" layer="94"/>
<wire x1="0" y1="10.668" x2="0" y2="9.652" width="0.254" layer="94"/>
<wire x1="0" y1="9.652" x2="1.524" y2="9.652" width="0.254" layer="94"/>
<wire x1="1.524" y1="8.128" x2="0" y2="8.128" width="0.254" layer="94"/>
<wire x1="0" y1="8.128" x2="0" y2="7.112" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="1.524" y2="7.112" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.588" x2="0" y2="5.588" width="0.254" layer="94"/>
<wire x1="0" y1="5.588" x2="0" y2="4.572" width="0.254" layer="94"/>
<wire x1="0" y1="4.572" x2="1.524" y2="4.572" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.048" x2="0" y2="3.048" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="0" y2="2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="1.524" y2="2.032" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.572" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0" y1="-5.588" x2="1.524" y2="-5.588" width="0.254" layer="94"/>
<wire x1="1.524" y1="-7.112" x2="0" y2="-7.112" width="0.254" layer="94"/>
<wire x1="0" y1="-7.112" x2="0" y2="-8.128" width="0.254" layer="94"/>
<wire x1="0" y1="-8.128" x2="1.524" y2="-8.128" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-10.16" x2="0.254" y2="-10.16" width="0.127" layer="94"/>
<wire x1="1.016" y1="-10.16" x2="1.524" y2="-10.16" width="0.127" layer="94"/>
<wire x1="2.286" y1="-10.16" x2="2.794" y2="-10.16" width="0.127" layer="94"/>
<wire x1="3.048" y1="-10.16" x2="3.302" y2="-10.16" width="0.127" layer="94"/>
<wire x1="3.302" y1="-10.16" x2="3.302" y2="-9.652" width="0.127" layer="94"/>
<wire x1="3.302" y1="9.906" x2="3.302" y2="10.414" width="0.127" layer="94"/>
<wire x1="3.302" y1="10.922" x2="3.302" y2="11.43" width="0.127" layer="94"/>
<wire x1="3.302" y1="11.43" x2="2.794" y2="11.43" width="0.127" layer="94"/>
<wire x1="2.286" y1="11.43" x2="1.778" y2="11.43" width="0.127" layer="94"/>
<wire x1="1.27" y1="11.43" x2="0.762" y2="11.43" width="0.127" layer="94"/>
<wire x1="0.254" y1="11.43" x2="-0.381" y2="11.43" width="0.127" layer="94"/>
<wire x1="-0.381" y1="11.43" x2="-0.381" y2="10.668" width="0.127" layer="94"/>
<wire x1="-0.381" y1="9.652" x2="-0.381" y2="8.128" width="0.127" layer="94"/>
<wire x1="-0.381" y1="7.112" x2="-0.381" y2="5.588" width="0.127" layer="94"/>
<wire x1="-0.381" y1="4.572" x2="-0.381" y2="3.048" width="0.127" layer="94"/>
<wire x1="-0.381" y1="2.032" x2="-0.381" y2="0.508" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="-2.032" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-3.048" x2="-0.381" y2="-4.572" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-5.588" x2="-0.381" y2="-7.112" width="0.127" layer="94"/>
<wire x1="-0.381" y1="-8.128" x2="-0.381" y2="-10.16" width="0.127" layer="94"/>
<wire x1="4.826" y1="4.064" x2="4.826" y2="3.048" width="0.1998" layer="94"/>
<wire x1="4.826" y1="3.048" x2="4.826" y2="2.54" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.54" x2="4.826" y2="2.032" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.032" x2="4.826" y2="1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.524" x2="4.826" y2="1.016" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.016" x2="4.826" y2="0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0.508" x2="4.826" y2="0" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0" x2="4.826" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-0.508" x2="4.826" y2="-1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-1.524" x2="7.366" y2="-1.524" width="0.1998" layer="94"/>
<wire x1="7.366" y1="-1.524" x2="7.366" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="7.366" y1="-0.254" x2="8.89" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="8.89" y1="-0.254" x2="8.89" y2="2.794" width="0.1998" layer="94"/>
<wire x1="8.89" y1="2.794" x2="7.366" y2="2.794" width="0.1998" layer="94"/>
<wire x1="7.366" y1="2.794" x2="7.366" y2="4.064" width="0.1998" layer="94"/>
<wire x1="7.366" y1="4.064" x2="4.826" y2="4.064" width="0.1998" layer="94"/>
<wire x1="4.826" y1="3.048" x2="5.588" y2="3.048" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.54" x2="5.588" y2="2.54" width="0.1998" layer="94"/>
<wire x1="4.826" y1="2.032" x2="5.588" y2="2.032" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.524" x2="5.588" y2="1.524" width="0.1998" layer="94"/>
<wire x1="4.826" y1="1.016" x2="5.588" y2="1.016" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0.508" x2="5.588" y2="0.508" width="0.1998" layer="94"/>
<wire x1="4.826" y1="0" x2="5.588" y2="0" width="0.1998" layer="94"/>
<wire x1="4.826" y1="-0.508" x2="5.588" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.302" y1="8.636" x2="3.302" y2="9.144" width="0.127" layer="94"/>
<wire x1="3.302" y1="7.366" x2="3.302" y2="7.874" width="0.127" layer="94"/>
<wire x1="3.302" y1="6.096" x2="3.302" y2="6.604" width="0.127" layer="94"/>
<wire x1="3.302" y1="4.826" x2="3.302" y2="5.334" width="0.127" layer="94"/>
<wire x1="3.302" y1="3.556" x2="3.302" y2="4.064" width="0.127" layer="94"/>
<wire x1="3.302" y1="2.286" x2="3.302" y2="2.794" width="0.127" layer="94"/>
<wire x1="3.302" y1="1.016" x2="3.302" y2="1.524" width="0.127" layer="94"/>
<wire x1="3.302" y1="-0.254" x2="3.302" y2="0.254" width="0.127" layer="94"/>
<wire x1="3.302" y1="-1.524" x2="3.302" y2="-1.016" width="0.127" layer="94"/>
<wire x1="3.302" y1="-2.794" x2="3.302" y2="-2.286" width="0.127" layer="94"/>
<wire x1="3.302" y1="-4.064" x2="3.302" y2="-3.556" width="0.127" layer="94"/>
<wire x1="3.302" y1="-5.334" x2="3.302" y2="-4.826" width="0.127" layer="94"/>
<wire x1="3.302" y1="-6.604" x2="3.302" y2="-6.096" width="0.127" layer="94"/>
<wire x1="3.302" y1="-7.874" x2="3.302" y2="-7.366" width="0.127" layer="94"/>
<wire x1="3.302" y1="-9.144" x2="3.302" y2="-8.636" width="0.127" layer="94"/>
<text x="3.81" y="10.668" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="-10.922" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="S2" x="2.54" y="-12.7" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S1" x="0" y="-12.7" visible="off" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GLX-S-88M" urn="urn:adsk.eagle:component:7684/1" prefix="X" library_version="1">
<description>&lt;b&gt;Mod. Jack, Right Angle, 8 posiotion, 8 contatcs&lt;/b&gt; RJ45&lt;p&gt;
Source: GLX-S-88M.pdf</description>
<gates>
<gate name="G$1" symbol="JACK8SH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GLX-S-88M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7680/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BD1" library="Discovery_shield" deviceset="STM32F4-DISCOVERY" device=""/>
<part name="X1" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="GPS"/>
<part name="X2" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="I2C1"/>
<part name="X3" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="I2C3"/>
<part name="X4" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="DEBUG"/>
<part name="X5" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-04-PIN" device="-LONG-PAD" value="XBee"/>
<part name="U$1" library="diy-modules" deviceset="LEVEL-SHIFTER-4CH" device="" value="LEVEL-SHIFTER"/>
<part name="SV1" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA04-1" device="" package3d_urn="urn:adsk.eagle:package:8337/1" value="Garmin"/>
<part name="U$2" library="diy-modules" deviceset="RS485-TTL-CONVERTER" device=""/>
<part name="X7" library="con-kycon" library_urn="urn:adsk.eagle:library:157" deviceset="GLX-S-88M" device="" package3d_urn="urn:adsk.eagle:package:7680/1"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="X8" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-05-PIN" device="-LONG-PAD" value="SPI1"/>
<part name="X6" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-02-PIN" device="-LONG-PAD" value="TIM2"/>
<part name="X9" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-02-PIN" device="-LONG-PAD" value="TIM3"/>
<part name="X10" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-02-PIN" device="-LONG-PAD" value="TIM9"/>
<part name="X11" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-02-PIN" device="-LONG-PAD" value="TIM12"/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="BD1" gate="G$1" x="26.67" y="13.97" smashed="yes">
<attribute name="NAME" x="31.75" y="46.99" size="1.27" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="UART4_RX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA1"/>
<wire x1="21.59" y1="29.21" x2="13.97" y2="29.21" width="0.1524" layer="91"/>
<label x="6.35" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PC10"/>
<wire x1="80.01" y1="-3.81" x2="72.39" y2="-3.81" width="0.1524" layer="91"/>
<label x="64.77" y="-6.35" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_TX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA2"/>
<wire x1="52.07" y1="26.67" x2="58.42" y2="26.67" width="0.1524" layer="91"/>
<label x="54.61" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_RX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA3"/>
<wire x1="21.59" y1="26.67" x2="13.97" y2="26.67" width="0.1524" layer="91"/>
<label x="6.35" y="26.67" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_TX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB10"/>
<wire x1="52.07" y1="1.27" x2="58.42" y2="1.27" width="0.1524" layer="91"/>
<label x="55.88" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_RX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB11"/>
<wire x1="21.59" y1="-1.27" x2="15.24" y2="-1.27" width="0.1524" layer="91"/>
<label x="5.08" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_RX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PD2"/>
<wire x1="110.49" y1="1.27" x2="118.11" y2="1.27" width="0.1524" layer="91"/>
<label x="113.03" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_TX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PC12"/>
<wire x1="80.01" y1="-1.27" x2="72.39" y2="-1.27" width="0.1524" layer="91"/>
<label x="64.77" y="-1.27" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_TX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PC6"/>
<wire x1="80.01" y1="-16.51" x2="73.66" y2="-16.51" width="0.1524" layer="91"/>
<label x="64.77" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_RX" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PC7"/>
<wire x1="110.49" y1="-16.51" x2="119.38" y2="-16.51" width="0.1524" layer="91"/>
<label x="115.57" y="-16.51" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB6"/>
<wire x1="80.01" y1="13.97" x2="72.39" y2="13.97" width="0.1524" layer="91"/>
<label x="66.04" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB9"/>
<wire x1="110.49" y1="19.05" x2="116.84" y2="19.05" width="0.1524" layer="91"/>
<label x="113.03" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA5"/>
<wire x1="21.59" y1="24.13" x2="13.97" y2="24.13" width="0.1524" layer="91"/>
<label x="6.35" y="24.13" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MISO" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA6"/>
<wire x1="52.07" y1="21.59" x2="58.42" y2="21.59" width="0.1524" layer="91"/>
<wire x1="58.42" y1="21.59" x2="58.42" y2="20.32" width="0.1524" layer="91"/>
<label x="54.61" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA7"/>
<wire x1="21.59" y1="21.59" x2="13.97" y2="21.59" width="0.1524" layer="91"/>
<label x="6.35" y="21.59" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SCL" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA8"/>
<wire x1="80.01" y1="-11.43" x2="74.93" y2="-11.43" width="0.1524" layer="91"/>
<label x="64.77" y="-11.43" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SDA" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PC9"/>
<wire x1="110.49" y1="-13.97" x2="118.11" y2="-13.97" width="0.1524" layer="91"/>
<label x="115.57" y="-13.97" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM2" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PA15"/>
<wire x1="110.49" y1="-6.35" x2="118.11" y2="-6.35" width="0.1524" layer="91"/>
<label x="115.57" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM3" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB4"/>
<wire x1="80.01" y1="11.43" x2="72.39" y2="11.43" width="0.1524" layer="91"/>
<label x="66.04" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM9" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PE5"/>
<wire x1="110.49" y1="26.67" x2="116.84" y2="26.67" width="0.1524" layer="91"/>
<label x="114.3" y="26.67" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM12" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PB14"/>
<wire x1="52.07" y1="-3.81" x2="58.42" y2="-3.81" width="0.1524" layer="91"/>
<label x="54.61" y="-3.81" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="GND"/>
<wire x1="21.59" y1="41.91" x2="15.24" y2="41.91" width="0.1524" layer="91"/>
<label x="12.7" y="41.91" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND2"/>
<wire x1="52.07" y1="41.91" x2="57.15" y2="41.91" width="0.1524" layer="91"/>
<label x="57.15" y="41.91" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND6"/>
<wire x1="52.07" y1="-19.05" x2="57.15" y2="-19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND5"/>
<wire x1="21.59" y1="-19.05" x2="15.24" y2="-19.05" width="0.1524" layer="91"/>
<label x="10.16" y="-19.05" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND10"/>
<wire x1="110.49" y1="-19.05" x2="118.11" y2="-19.05" width="0.1524" layer="91"/>
<label x="119.38" y="-19.05" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND9"/>
<wire x1="80.01" y1="-19.05" x2="73.66" y2="-19.05" width="0.1524" layer="91"/>
<label x="67.31" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND7"/>
<wire x1="80.01" y1="41.91" x2="72.39" y2="41.91" width="0.1524" layer="91"/>
<label x="67.31" y="41.91" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="GND8"/>
<wire x1="110.49" y1="41.91" x2="116.84" y2="41.91" width="0.1524" layer="91"/>
<label x="116.84" y="41.91" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="5V"/>
<wire x1="80.01" y1="39.37" x2="73.66" y2="39.37" width="0.1524" layer="91"/>
<label x="69.85" y="39.37" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="5V_1"/>
<wire x1="110.49" y1="39.37" x2="116.84" y2="39.37" width="0.1524" layer="91"/>
<label x="118.11" y="39.37" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="VDD"/>
<wire x1="21.59" y1="39.37" x2="15.24" y2="39.37" width="0.1524" layer="91"/>
<label x="11.43" y="39.37" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BD1" gate="G$1" pin="VDD2"/>
<wire x1="52.07" y1="39.37" x2="57.15" y2="39.37" width="0.1524" layer="91"/>
<label x="57.15" y="39.37" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENABLE" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="PH0"/>
<wire x1="80.01" y1="34.29" x2="73.66" y2="34.29" width="0.1524" layer="91"/>
<wire x1="73.66" y1="34.29" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
<label x="68.58" y="34.29" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD1" class="0">
<segment>
<pinref part="BD1" gate="G$1" pin="VDD3"/>
<wire x1="110.49" y1="16.51" x2="116.84" y2="16.51" width="0.1524" layer="91"/>
<label x="116.84" y="16.51" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="-1" x="55.88" y="-20.32" smashed="yes">
<attribute name="NAME" x="58.42" y="-21.082" size="1.524" layer="95"/>
<attribute name="VALUE" x="55.118" y="-18.923" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-2" x="55.88" y="-22.86" smashed="yes">
<attribute name="NAME" x="58.42" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-3" x="55.88" y="-25.4" smashed="yes">
<attribute name="NAME" x="58.42" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-4" x="55.88" y="-27.94" smashed="yes">
<attribute name="NAME" x="58.42" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-1" x="86.36" y="-20.32" smashed="yes">
<attribute name="NAME" x="88.9" y="-21.082" size="1.524" layer="95"/>
<attribute name="VALUE" x="85.598" y="-18.923" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="-2" x="86.36" y="-22.86" smashed="yes">
<attribute name="NAME" x="88.9" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-3" x="86.36" y="-25.4" smashed="yes">
<attribute name="NAME" x="88.9" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X2" gate="-4" x="86.36" y="-27.94" smashed="yes">
<attribute name="NAME" x="88.9" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="X3" gate="-1" x="116.84" y="-20.32" smashed="yes">
<attribute name="NAME" x="119.38" y="-21.082" size="1.524" layer="95"/>
<attribute name="VALUE" x="116.078" y="-18.923" size="1.778" layer="96"/>
</instance>
<instance part="X3" gate="-2" x="116.84" y="-22.86" smashed="yes">
<attribute name="NAME" x="119.38" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X3" gate="-3" x="116.84" y="-25.4" smashed="yes">
<attribute name="NAME" x="119.38" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X3" gate="-4" x="116.84" y="-27.94" smashed="yes">
<attribute name="NAME" x="119.38" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="X4" gate="-1" x="147.32" y="-20.32" smashed="yes">
<attribute name="NAME" x="149.86" y="-21.082" size="1.524" layer="95"/>
<attribute name="VALUE" x="146.558" y="-18.923" size="1.778" layer="96"/>
</instance>
<instance part="X4" gate="-2" x="147.32" y="-22.86" smashed="yes">
<attribute name="NAME" x="149.86" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X4" gate="-3" x="147.32" y="-25.4" smashed="yes">
<attribute name="NAME" x="149.86" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X4" gate="-4" x="147.32" y="-27.94" smashed="yes">
<attribute name="NAME" x="149.86" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="X5" gate="-1" x="25.4" y="-20.32" smashed="yes">
<attribute name="NAME" x="27.94" y="-21.082" size="1.524" layer="95"/>
<attribute name="VALUE" x="24.638" y="-18.923" size="1.778" layer="96"/>
</instance>
<instance part="X5" gate="-2" x="25.4" y="-22.86" smashed="yes">
<attribute name="NAME" x="27.94" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X5" gate="-3" x="25.4" y="-25.4" smashed="yes">
<attribute name="NAME" x="27.94" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X5" gate="-4" x="25.4" y="-27.94" smashed="yes">
<attribute name="NAME" x="27.94" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="U$1" gate="G$1" x="35.56" y="-67.31" smashed="yes">
<attribute name="NAME" x="25.4" y="-54.61" size="1.778" layer="95"/>
<attribute name="VALUE" x="25.4" y="-57.15" size="1.778" layer="96"/>
</instance>
<instance part="SV1" gate="1" x="80.01" y="-62.23" smashed="yes" rot="R180">
<attribute name="VALUE" x="81.28" y="-52.07" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="81.28" y="-68.072" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="U$2" gate="G$1" x="107.95" y="-74.93" smashed="yes">
<attribute name="VALUE" x="90.17" y="-64.77" size="1.778" layer="96"/>
</instance>
<instance part="X7" gate="G$1" x="157.48" y="-74.93" smashed="yes">
<attribute name="NAME" x="161.29" y="-64.262" size="1.778" layer="95"/>
<attribute name="VALUE" x="161.29" y="-85.852" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="40.64" y="-5.08" smashed="yes">
<attribute name="VALUE" x="38.1" y="-7.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="15.24" y="-38.1" smashed="yes">
<attribute name="VALUE" x="13.335" y="-41.275" size="1.778" layer="96"/>
</instance>
<instance part="X8" gate="-1" x="170.18" y="-17.78" smashed="yes">
<attribute name="NAME" x="172.72" y="-18.542" size="1.524" layer="95"/>
<attribute name="VALUE" x="169.418" y="-16.383" size="1.778" layer="96"/>
</instance>
<instance part="X8" gate="-2" x="170.18" y="-20.32" smashed="yes">
<attribute name="NAME" x="172.72" y="-21.082" size="1.524" layer="95"/>
</instance>
<instance part="X8" gate="-3" x="170.18" y="-22.86" smashed="yes">
<attribute name="NAME" x="172.72" y="-23.622" size="1.524" layer="95"/>
</instance>
<instance part="X8" gate="-4" x="170.18" y="-25.4" smashed="yes">
<attribute name="NAME" x="172.72" y="-26.162" size="1.524" layer="95"/>
</instance>
<instance part="X8" gate="-5" x="170.18" y="-27.94" smashed="yes">
<attribute name="NAME" x="172.72" y="-28.702" size="1.524" layer="95"/>
</instance>
<instance part="X6" gate="-2" x="17.78" y="22.86" smashed="yes">
<attribute name="NAME" x="20.32" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="X6" gate="-1" x="17.78" y="25.4" smashed="yes">
<attribute name="NAME" x="20.32" y="24.638" size="1.524" layer="95"/>
<attribute name="VALUE" x="17.018" y="26.797" size="1.778" layer="96"/>
</instance>
<instance part="X9" gate="-2" x="55.88" y="22.86" smashed="yes">
<attribute name="NAME" x="58.42" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="X9" gate="-1" x="55.88" y="25.4" smashed="yes">
<attribute name="NAME" x="58.42" y="24.638" size="1.524" layer="95"/>
<attribute name="VALUE" x="55.118" y="26.797" size="1.778" layer="96"/>
</instance>
<instance part="X10" gate="-2" x="88.9" y="22.86" smashed="yes">
<attribute name="NAME" x="91.44" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="X10" gate="-1" x="88.9" y="25.4" smashed="yes">
<attribute name="NAME" x="91.44" y="24.638" size="1.524" layer="95"/>
<attribute name="VALUE" x="88.138" y="26.797" size="1.778" layer="96"/>
</instance>
<instance part="X11" gate="-2" x="127" y="22.86" smashed="yes">
<attribute name="NAME" x="129.54" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="X11" gate="-1" x="127" y="25.4" smashed="yes">
<attribute name="NAME" x="129.54" y="24.638" size="1.524" layer="95"/>
<attribute name="VALUE" x="126.238" y="26.797" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="15.24" y="12.7" smashed="yes">
<attribute name="VALUE" x="13.335" y="9.525" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="SV1" gate="1" pin="2"/>
<wire x1="72.39" y1="-59.69" x2="66.04" y2="-59.69" width="0.1524" layer="91"/>
<label x="67.31" y="-59.69" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND.1"/>
<wire x1="20.32" y1="-69.85" x2="12.7" y2="-69.85" width="0.1524" layer="91"/>
<label x="12.7" y="-69.85" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND.2"/>
<wire x1="50.8" y1="-69.85" x2="58.42" y2="-69.85" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-69.85" x2="58.42" y2="-71.12" width="0.1524" layer="91"/>
<label x="54.61" y="-69.85" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X5" gate="-4" pin="S"/>
<wire x1="22.86" y1="-27.94" x2="15.24" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="15.24" y1="-35.56" x2="15.24" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-33.02" x2="15.24" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-33.02" x2="45.72" y2="-33.02" width="0.1524" layer="91"/>
<junction x="15.24" y="-33.02"/>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="53.34" y1="-27.94" x2="45.72" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-33.02" x2="45.72" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-33.02" x2="76.2" y2="-33.02" width="0.1524" layer="91"/>
<junction x="45.72" y="-33.02"/>
<wire x1="76.2" y1="-33.02" x2="106.68" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-33.02" x2="137.16" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-27.94" x2="137.16" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="X4" gate="-4" pin="S"/>
<wire x1="137.16" y1="-33.02" x2="137.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-27.94" x2="106.68" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="X3" gate="-4" pin="S"/>
<wire x1="106.68" y1="-33.02" x2="106.68" y2="-27.94" width="0.1524" layer="91"/>
<junction x="106.68" y="-33.02"/>
<wire x1="83.82" y1="-27.94" x2="76.2" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="X2" gate="-4" pin="S"/>
<wire x1="76.2" y1="-33.02" x2="76.2" y2="-27.94" width="0.1524" layer="91"/>
<junction x="76.2" y="-33.02"/>
<wire x1="137.16" y1="-33.02" x2="167.64" y2="-33.02" width="0.1524" layer="91"/>
<junction x="137.16" y="-33.02"/>
<wire x1="167.64" y1="-33.02" x2="167.64" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="X8" gate="-5" pin="S"/>
</segment>
<segment>
<pinref part="X6" gate="-2" pin="S"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="17.78" width="0.1524" layer="91"/>
<wire x1="15.24" y1="17.78" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
<wire x1="15.24" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<junction x="15.24" y="17.78"/>
<pinref part="X11" gate="-2" pin="S"/>
<wire x1="53.34" y1="17.78" x2="86.36" y2="17.78" width="0.1524" layer="91"/>
<wire x1="86.36" y1="17.78" x2="124.46" y2="17.78" width="0.1524" layer="91"/>
<wire x1="124.46" y1="17.78" x2="124.46" y2="22.86" width="0.1524" layer="91"/>
<pinref part="X10" gate="-2" pin="S"/>
<wire x1="86.36" y1="22.86" x2="86.36" y2="17.78" width="0.1524" layer="91"/>
<junction x="86.36" y="17.78"/>
<pinref part="X9" gate="-2" pin="S"/>
<wire x1="53.34" y1="22.86" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<junction x="53.34" y="17.78"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="130.81" y1="-80.01" x2="138.43" y2="-80.01" width="0.1524" layer="91"/>
<label x="134.62" y="-80.01" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS422_RX" class="0">
<segment>
<pinref part="SV1" gate="1" pin="3"/>
<pinref part="U$1" gate="G$1" pin="HV1"/>
<wire x1="72.39" y1="-62.23" x2="50.8" y2="-62.23" width="0.1524" layer="91"/>
<label x="59.69" y="-62.23" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS422_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="HV2"/>
<pinref part="SV1" gate="1" pin="4"/>
<wire x1="50.8" y1="-64.77" x2="72.39" y2="-64.77" width="0.1524" layer="91"/>
<label x="59.69" y="-64.77" size="1.778" layer="95"/>
</segment>
</net>
<net name="RS422_VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="HV"/>
<wire x1="50.8" y1="-67.31" x2="59.69" y2="-67.31" width="0.1524" layer="91"/>
<label x="52.07" y="-67.31" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="1"/>
<wire x1="72.39" y1="-57.15" x2="66.04" y2="-57.15" width="0.1524" layer="91"/>
<label x="58.42" y="-57.15" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LV"/>
<wire x1="20.32" y1="-67.31" x2="12.7" y2="-67.31" width="0.1524" layer="91"/>
<label x="12.7" y="-67.31" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="40.64" y1="-7.62" x2="40.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-12.7" x2="45.72" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="45.72" y1="-12.7" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-12.7" x2="106.68" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-12.7" x2="137.16" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-20.32" x2="45.72" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-20.32" x2="45.72" y2="-12.7" width="0.1524" layer="91"/>
<junction x="45.72" y="-12.7"/>
<wire x1="144.78" y1="-20.32" x2="137.16" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="X4" gate="-1" pin="S"/>
<wire x1="137.16" y1="-12.7" x2="137.16" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-20.32" x2="106.68" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="X3" gate="-1" pin="S"/>
<wire x1="106.68" y1="-20.32" x2="106.68" y2="-12.7" width="0.1524" layer="91"/>
<junction x="106.68" y="-12.7"/>
<wire x1="83.82" y1="-20.32" x2="76.2" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="X2" gate="-1" pin="S"/>
<wire x1="76.2" y1="-20.32" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="-12.7"/>
<wire x1="167.64" y1="-12.7" x2="137.16" y2="-12.7" width="0.1524" layer="91"/>
<junction x="137.16" y="-12.7"/>
<pinref part="X8" gate="-1" pin="S"/>
<wire x1="167.64" y1="-12.7" x2="167.64" y2="-17.78" width="0.1524" layer="91"/>
<label x="41.91" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="130.81" y1="-72.39" x2="138.43" y2="-72.39" width="0.1524" layer="91"/>
<label x="135.89" y="-72.39" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENABLE" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RE"/>
<wire x1="85.09" y1="-74.93" x2="77.47" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="DE"/>
<wire x1="85.09" y1="-77.47" x2="77.47" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="77.47" y1="-74.93" x2="77.47" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="77.47" y1="-77.47" x2="73.66" y2="-77.47" width="0.1524" layer="91"/>
<junction x="77.47" y="-77.47"/>
<label x="67.31" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LV1"/>
<wire x1="20.32" y1="-62.23" x2="8.89" y2="-62.23" width="0.1524" layer="91"/>
<label x="6.35" y="-62.23" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART6_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LV2"/>
<wire x1="20.32" y1="-64.77" x2="8.89" y2="-64.77" width="0.1524" layer="91"/>
<label x="6.35" y="-64.77" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="B"/>
<wire x1="130.81" y1="-74.93" x2="143.51" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-74.93" x2="143.51" y2="-72.39" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-72.39" x2="144.78" y2="-72.39" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-72.39" x2="144.78" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-74.93" x2="146.05" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-74.93" x2="146.05" y2="-72.39" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-72.39" x2="147.32" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-71.12" x2="147.32" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="3"/>
<wire x1="147.32" y1="-76.2" x2="153.67" y2="-69.85" width="0.1524" layer="91"/>
<wire x1="153.67" y1="-69.85" x2="154.94" y2="-69.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="A"/>
<wire x1="130.81" y1="-77.47" x2="143.51" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-77.47" x2="143.51" y2="-87.63" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-87.63" x2="142.24" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="143.51" y1="-87.63" x2="146.05" y2="-85.09" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-85.09" x2="146.05" y2="-80.01" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-77.47" x2="146.05" y2="-80.01" width="0.1524" layer="91"/>
<wire x1="146.05" y1="-80.01" x2="147.32" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-81.28" x2="149.86" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-78.74" x2="149.86" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-77.47" x2="151.13" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="6"/>
<wire x1="151.13" y1="-78.74" x2="152.4" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-77.47" x2="154.94" y2="-77.47" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART4_RX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LV3"/>
<wire x1="20.32" y1="-72.39" x2="8.89" y2="-72.39" width="0.1524" layer="91"/>
<label x="6.35" y="-72.39" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LV4"/>
<wire x1="20.32" y1="-74.93" x2="8.89" y2="-74.93" width="0.1524" layer="91"/>
<label x="6.35" y="-74.93" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="HV3"/>
<pinref part="U$2" gate="G$1" pin="RO"/>
<wire x1="50.8" y1="-72.39" x2="85.09" y2="-72.39" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="HV4"/>
<wire x1="50.8" y1="-74.93" x2="59.69" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="59.69" y1="-74.93" x2="59.69" y2="-80.01" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="DI"/>
<wire x1="59.69" y1="-80.01" x2="85.09" y2="-80.01" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART5_TX" class="0">
<segment>
<pinref part="X5" gate="-2" pin="S"/>
<wire x1="22.86" y1="-22.86" x2="15.24" y2="-22.86" width="0.1524" layer="91"/>
<label x="10.16" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART5_RX" class="0">
<segment>
<pinref part="X5" gate="-3" pin="S"/>
<wire x1="22.86" y1="-25.4" x2="15.24" y2="-25.4" width="0.1524" layer="91"/>
<label x="10.16" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_TX" class="0">
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="53.34" y1="-22.86" x2="45.72" y2="-22.86" width="0.1524" layer="91"/>
<label x="40.64" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART3_RX" class="0">
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="53.34" y1="-25.4" x2="45.72" y2="-25.4" width="0.1524" layer="91"/>
<label x="40.64" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<wire x1="83.82" y1="-22.86" x2="76.2" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="X2" gate="-2" pin="S"/>
<label x="71.12" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<wire x1="83.82" y1="-25.4" x2="76.2" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="X2" gate="-3" pin="S"/>
<label x="71.12" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SDA" class="0">
<segment>
<wire x1="114.3" y1="-22.86" x2="106.68" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="X3" gate="-2" pin="S"/>
<label x="101.6" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C3_SCL" class="0">
<segment>
<wire x1="114.3" y1="-25.4" x2="106.68" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="X3" gate="-3" pin="S"/>
<label x="101.6" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_TX" class="0">
<segment>
<wire x1="144.78" y1="-22.86" x2="137.16" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="X4" gate="-2" pin="S"/>
<label x="132.08" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_RX" class="0">
<segment>
<wire x1="144.78" y1="-25.4" x2="137.16" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="X4" gate="-3" pin="S"/>
<label x="132.08" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MISO" class="0">
<segment>
<pinref part="X8" gate="-2" pin="S"/>
<wire x1="167.64" y1="-20.32" x2="160.02" y2="-20.32" width="0.1524" layer="91"/>
<label x="157.48" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="X8" gate="-3" pin="S"/>
<wire x1="167.64" y1="-22.86" x2="160.02" y2="-22.86" width="0.1524" layer="91"/>
<label x="157.48" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="X8" gate="-4" pin="S"/>
<wire x1="167.64" y1="-25.4" x2="160.02" y2="-25.4" width="0.1524" layer="91"/>
<label x="157.48" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM2" class="0">
<segment>
<pinref part="X6" gate="-1" pin="S"/>
<wire x1="15.24" y1="25.4" x2="5.08" y2="25.4" width="0.1524" layer="91"/>
<label x="5.08" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM3" class="0">
<segment>
<pinref part="X9" gate="-1" pin="S"/>
<wire x1="53.34" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<label x="43.18" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM9" class="0">
<segment>
<pinref part="X10" gate="-1" pin="S"/>
<wire x1="86.36" y1="25.4" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<label x="76.2" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="TIM12" class="0">
<segment>
<pinref part="X11" gate="-1" pin="S"/>
<wire x1="124.46" y1="25.4" x2="111.76" y2="25.4" width="0.1524" layer="91"/>
<label x="111.76" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD1" class="0">
<segment>
<pinref part="X5" gate="-1" pin="S"/>
<wire x1="22.86" y1="-20.32" x2="15.24" y2="-20.32" width="0.1524" layer="91"/>
<label x="11.43" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
